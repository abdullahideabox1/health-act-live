$(document).ready(function () {
    var $consultation_profile = $('#consultation_profile');
    $(".giftchatopen").click(function () {
        var item_name = $(this).data('tab-name');
        $('#gifted_items').val(item_name);
        console.log(item_name);
        $(".giftchat").show(1500);
        $('.closure').click(function () {
            $(".giftchat").hide(1500);
        })
    });


    if (screen.width < 767) {
        var $navbar_click = $('#navbarCollapse > ul > li > a');
        $navbar_click.on('click', function () {
            var button_check = $('button.navbar-toggler');
            if (button_check.not('.collapsed')) {
                button_check.trigger('click');
            }

        });
    }
    consultation_profile();
});

function consultation_profile() {
    $(document).delegate('#consultation_profile', 'click', function () {
        var consiltation_name = $(this).children('div').children('h6').text();
        var consultation_id = $(this).data('consultant-id');
        $.ajax({
            url: '/site/ConsultantJSON',
            type: "POST",
            data: {'consultation_id': consultation_id},
            success: function (data) {
                $("#Consultant_data").html(data);
                $("#Consultant_data").show(1500).animate("slow");
                $('.closurecons').click(function () {
                    $("#Consultant_data").hide(1500);
                })
            }
        });


    });
}
