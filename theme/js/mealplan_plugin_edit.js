$(document).ready(function () {
    var $Main = null;
    /*Delete Item*/
    $(document).delegate('.delete_item', 'click', function () {
        var item = $(this).parent();
        var request = confirm("Are you sure?");
        if (request == true) {
            item.remove()
        }
        priceCalculate();
    });

    $(document).delegate('.dragdrop_item', 'drag', function () {
        $(this).remove();
    });
    $(document).delegate('#saveModalDetail', 'click', function () {
        var item_id = $('#item_id').val();
        var checkDiv = $('#' + item_id);
        var quantity = $('#quantity').val();

        if (quantity > 0) {
            var product_quantity = checkDiv.children('#product_quantity').val(quantity);
            var get_option_value = $('#get_option_value').val();
            var get_option_id = $('#get_option_value').find(':selected').data('option-id');
            var option_value_id = checkDiv.children('#option_value_id').val(get_option_id);


            var get_delivery_time = $('#get_delivery_time').val();
            var delivery_time = checkDiv.children('#delivery_time').val(get_delivery_time);
            var option_value = checkDiv.children('#option_value').val(get_option_value);

            priceCalculate();

            $('[data-dismiss="modal"]').trigger('click');
        } else {
            alert("Quantity must be greater than 1");
        }
    });

    edit_item($Main);
    ProductModal();
    mealPlanCalendar();


    $('#showCalendar').trigger('click');


});


function mealPlanCalendar() {

    $(document).delegate('#showCalendar', 'click', function () {
        var startDate = $('#startDate').val();
        var endDate = $('#endDate').val();
        var getStartDate = new Date(startDate);
        var getEndDate = new Date(endDate);
        var findedStartedDate = ('0' + (getStartDate.getDate() + 6)).slice(-2);
        var findedEndedDate = getEndDate.getDate();
        var findedStartedMonth = ('0' + (getStartDate.getMonth() + 1)).slice(-2);
        var findedStartedYear = getStartDate.getFullYear();
        var finalDateFormat = findedStartedYear + '-' + findedStartedMonth + '-' + findedStartedDate;
        var totalDaySelected = parseInt(findedEndedDate) - parseInt(findedStartedDate);
        var createWeeks = totalDaySelected / 7;
        var weeksDivided = Math.ceil(createWeeks);
        var mealPlanID = $('#mealPlanID').val();

        $('#start_date').val(startDate);
        $('#end_date').val(endDate);

        if (startDate == '') {
            alert('Please Select Start Date!');
        } else if (endDate == '') {
            $('#endDate').val(finalDateFormat);

        }
        else {
            $.ajax({
                url: '/site/MealPlanCalenderJSONEdit',
                type: "POST",
                data: {
                    'startDate': startDate,
                    'endDate': endDate,
                    'findedStartedDate': findedStartedDate,
                    'findedStartedMonth': findedStartedMonth,
                    'findedStartedYear': findedStartedYear,
                    'weeksDivided': weeksDivided,
                    'createWeeks': createWeeks,
                    'findedEndedDate': findedEndedDate,
                    'mealPlanID': mealPlanID
                },
                success: function (data) {
                    $(".calender-table").html(data);
                    priceCalculate();
                }
            });
        }


    });
}
/**/

/**/

function edit_item($Main) {
    $(document).delegate('.edit_item', 'click', function () {
        $Main = $(this);
        var modelOpen = $('#ShowProductDetailModal');
        var item_id = $(this).parent().attr('id');
        var delivery_time = $(this).parent().children("input#delivery_time").val();
        var product_quantity = $(this).parent().children("input#product_quantity").val();
        var calender_type = $(this).parent().children("input#calender_type").val();
        var option_value_id = $(this).parent().children("input#option_value_id").val();
        var $product_id = $(this).data('product-id');

        modelOpen.trigger('click');
        $.ajax({
            url: '/site/MealPlanProductsWithOptionsJSON',
            type: "POST",
            data: {
                'product_id': $product_id,
                'item_id': item_id,
                'delivery_time': delivery_time,
                'product_quantity': product_quantity,
                'calender_type': calender_type,
                'option_value_id': option_value_id
            },
            success: function (data) {
                $(".ProductDetailModal .modal-content").html(data);
            }
        });
    });
}


function ProductModal() {
    $('[data-target="#ProductModal"]').click(function () {
        var $product_id = $(this).data('product-id');
        $.ajax({
            url: '/site/MealPlanProductsJSON',
            type: "POST",
            data: {'product_id': $product_id},
            success: function (data) {
                $(".ProductModal .modal-content").html(data);
            }
        });
    });
}
/*Drag & Drop*/
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    var product_id = ev.target.getAttribute("data-product-id");
    var product_price = ev.target.getAttribute("data-product-price");
    if (product_id == null)
        product_id = $(ev.target.querySelector('#product_id')).attr('value');


    if (product_price == null)
        product_price = $(ev.target.querySelector('#product_price')).attr('value');

    product_quantity = $(ev.target.querySelector('#product_quantity')).attr('value');
    console.log(product_quantity);
    delivery_time = $(ev.target.querySelector('#delivery_time')).attr('value');
    option_value = $(ev.target.querySelector('#option_value')).attr('value');
    option_value_id = $(ev.target.querySelector('#option_value_id')).attr('value');

    ev.dataTransfer.setData("productID", product_id);
    ev.dataTransfer.setData("productPrice", product_price);
    ev.dataTransfer.setData("Text", ev.target.querySelector('span').innerHTML);

    ev.dataTransfer.setData("productQuantity", product_quantity);
    ev.dataTransfer.setData("deliveryTime", delivery_time);
    ev.dataTransfer.setData("optionValue", option_value);
    ev.dataTransfer.setData("optionValueId", option_value_id);
}

var cn = 0;
var gcn = 0;
var copydata1;

function dropcopy(ev, id) {
    cn += 1;
    gcn += 1;
    var ab;

    var data = ev.dataTransfer.getData("Text");
    var productID = ev.dataTransfer.getData("productID");
    var productPrice = ev.dataTransfer.getData("productPrice");
    var copydata = document.createElement("p");

    /*Find Calender Detail*/
    var calender_date = ev.target.getAttribute('data-calender-date');
    var calender_day = ev.target.getAttribute('data-calender-day');
    var calender_type = ev.target.getAttribute('data-calender-type');
    /*Find Calender Detail*/

    var productQuantity = ev.dataTransfer.getData("productQuantity");
    var deliveryTime = ev.dataTransfer.getData("deliveryTime");
    var optionValue = ev.dataTransfer.getData("optionValue");
    var optionValueId = ev.dataTransfer.getData("optionValueId");


    if (productQuantity === "undefined")
        productQuantity = 1;

    if (deliveryTime === "undefined")
        deliveryTime = "0";

    if (optionValue === "undefined")
        optionValue = "0";

    if (optionValueId === "undefined")
        optionValueId = "0";

    $('#' + id).append(copydata);
    $('<span>' + data + '</span>').appendTo(copydata);
    /*Variables*/

    var addDeleteBtn = '<span class="delete_item ml-1 btn-outline-green"><u>X</u></span>';
    var editBtn = '<span class="edit_item ml-1 btn-outline-green" data-product-id="' + productID + '"><i class="far fa-edit"></i></span>';
    var itemName = '<input type="hidden" name="UserMenuDetail[product_name][]" id="product_name" value="' + data + '"/>';
    var itemID = '<input type="hidden" name="UserMenuDetail[product_id][]" id="product_id" value="' + productID + '"/>';
    var itemPrice = '<input type="hidden" name="UserMenuDetail[product_price][]" id="product_price" value="' + productPrice + '"/>';
    var calender_date_input = '<input type="hidden" name="UserMenuDetail[calender_date][]" id="calender_date" value="' + calender_date + '"/>';
    var calender_day_input = '<input type="hidden" name="UserMenuDetail[calender_day][]" id="calender_day" value="' + calender_day + '"/>';
    var calender_type_input = '<input type="hidden" name="UserMenuDetail[calender_type][]" id="calender_type" value="' + calender_type + '"/>';
    var product_quantity = '<input type="hidden" name="UserMenuDetail[product_quantity][]" id="product_quantity" value="' + productQuantity + '"/>';
    var delivery_time = '<input type="hidden" name="UserMenuDetail[delivery_time][]" id="delivery_time" value="' + deliveryTime + '"/>';
    var option_value = '<input type="hidden" name="UserMenuDetail[option_value][]" id="option_value" value="' + optionValue + '"/>';
    var option_value_id = '<input type="hidden" name="UserMenuDetail[option_value_id][]" id="option_value_id" value="' + optionValueId + '"/>';
    /*Adding Dragable Option in New Item*/
    $(copydata).attr('draggable', true);
    $(copydata).attr('ondragstart', 'drag(event)');
    $(copydata).attr('id', +cn + '');
    $(copydata).attr('data-id', +cn + '');
    $(copydata).attr('class', 'dragdrop_item');
    $(copydata).attr('data-product-id', productID);
    /*Append Buttons*/
    $(addDeleteBtn).appendTo(copydata);
    $(editBtn).appendTo(copydata);

    /*Adding Input Field Item Name*/
    $(itemName).appendTo(copydata);
    $(itemID).appendTo(copydata);
    $(itemPrice).appendTo(copydata);
    $(calender_date_input).appendTo(copydata);
    $(calender_day_input).appendTo(copydata);
    $(calender_type_input).appendTo(copydata);
    $(product_quantity).appendTo(copydata);
    $(delivery_time).appendTo(copydata);
    $(option_value).appendTo(copydata);
    $(option_value_id).appendTo(copydata);

    if (calender_date === null) {
        $(copydata).remove();
    }

    /*Total Amount Calculation*/

    priceCalculate();
    /* var sum = $('#total_price').val();
     var total_sum = parseInt(sum) + parseInt(productPrice);
     var total = $('#total_price').val(total_sum);
     $('#total_amount').text(total_sum);*/


    /*Trigger Modal*/
    //$('.edit_item').trigger('click');

}

function priceCalculate() {
    //console.log('------------------');
    var product_id = "";
    var product_qty = "";
    $('.dragdrop_item').each(function () {
        product_id += $(this).data('product-id') + ",";

        product_qty += $(this).children("input#product_quantity").val() + ",";

        //console.log(product_price);

    });

    $.ajax({
        url: '/site/ProductPriceJSON',
        type: "POST",
        data: {'product_id': product_id, 'product_qty': product_qty},
        success: function (data) {
            $("#total_amount").html(data);
            $("#total_price").attr('value', data);
            if (data > 0) {
                $('#checkout_btn').html('<button type="submit" class="btn btn-primary-white float-right ml-5">CHECK OUT</button>');
            } else {
                $('#checkout_btn').html('<button type="submit" class="btn btn-primary-white float-right ml-5" disabled>CHECK OUT</button>');
            }

        }
    });


    calaoriesCalculate();


    //  console.log('------------------');
}


function calaoriesCalculate() {
    //console.log('------------------');
    var product_id = "";
    var product_qty = "";
    var option_value_id = "";
    var calender_date = "";

    $('.dragdrop_item').each(function () {
        product_id += $(this).data('product-id') + ",";
        option_value_id += $(this).children("input#option_value_id").val() + ",";
        product_qty += $(this).children("input#product_quantity").val() + ",";
        calender_date += $(this).children("input#calender_date").val() + ",";

        //console.log(product_price);

    });

    $.ajax({
        url: '/site/ProductCalaoriesJSON',
        type: "POST",
        data: {
            'product_id': product_id,
            'product_qty': product_qty,
            'option_value_id': option_value_id,
            'calender_date': calender_date
        },
        success: function (data) {
            var myJSON = JSON.parse(data);

            $(".calaries").html('');

            $.each(myJSON, function (index, value) {

                string = "<p class='mb-0'>Calories " + value.calory + " </p><p class='mb-0'> Carbs " + value.carbo + " </p><p class='mb-0'> Protein " + value.protein + " </p><p class='mb-0'> Fats " + value.fats + "</p>";

                $("#" + index + "calorie").html(string);


            });


            //$("#calaries").html(data);
        }
    });


    //  console.log('------------------');
}