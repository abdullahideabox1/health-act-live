$(document).ready(function () {
    $('#RegisterUser').on('click', function () {
        $.validate({
            lang: 'en'
        });
    });
    setTimeout(function () {
        $("#success_message").hide('blind', {}, 500)
    }, 1000);
    mealPlans_tray();
});

function mealPlans_tray() {
    $(document).delegate('#mealPlans_tray', 'click', function () {
        $("#mealPlans_tray_data").show(1500).animate("slow");
        var id = $(this).data('id');
        $.ajax({
            url: '/site/MealPlansJSON',
            type: "POST",
            data: {'id': id},
            success: function (data) {
                $("#mealPlans_tray_data").html(data);
                $('.closurecons').click(function () {
                    $("#mealPlans_tray_data").hide(1500);
                })
            }
        });


    });
}