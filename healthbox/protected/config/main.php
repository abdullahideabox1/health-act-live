<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Web Application',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

    ),

    // application components
    'preload' => array('log', 'input'),
    'components' => array(
        'email' => array(
            'class' => 'application.extensions.email.Email',
            'delivery' => 'php', //Will use the php mailing function.
            //May also be set to 'debug' to instead dump the contents of the email into the view
        ),

        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),

        'input' => array(
            'class' => 'CmsInput',
            'cleanPost' => false,
            'cleanGet' => false,
            'cleanMethod' => 'stripClean',
        ),

        'clientScript' => array(

            // disable default yii scripts
            'scriptMap' => array(
                'jquery.js' => false,
                //  'jquery.min.js' => false,

            )),

        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => false,
            'rules' => array(
                //'/career/*' => array('/site/career/'),
               // '<action>' => '/site/<action>',
              //  '<action>/<id:\d+>' => '/site/<action>/<id:\d+>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'artist/<slug:[a-zA-Z0-9_ -]+>' => '/site/artist',
                'album/<slug:[a-zA-Z0-9_ -]+>' => '/site/album',
            ),
        ),


        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),

    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'owaismeer17@gmail.com',
        'genre' => array(
            'Romantic' => 'Romantic',
            'Sad/Ballad' => 'Sad/Ballad',
            'Comedy/Mirth' => 'Comedy/Mirth',
            'Patriotic' => 'Patriotic',
            'Evergreen' => 'Evergreen',
            'Classical' => 'Classical',
            'Qawwali' => 'Qawwali',
            'Sufi' => 'Sufi',
            'Religious' => 'Religious',
            'Instrumental' => 'Instrumental',
            'Pop (Mainstream)' => 'Pop (Mainstream)',
            'Children' => 'Children',
            'Inspirational' => 'Inspirational',
            'Festive' => 'Festive',
            'Literary' => 'Literary',
            'Ghazal/Geet' => 'Ghazal/Geet',
            'Folk' => 'Folk',
        ),
    ),
);
