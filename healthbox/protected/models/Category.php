<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $category_name
 * @property string $category_image
 * @property integer $is_active
 */
class Category extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_name, is_active', 'required'),
            array('is_active', 'numerical', 'integerOnly' => true),
            array('category_name, category_image', 'length', 'max' => 255),
            array('is_sort','length', 'max' => 11),
            array('category_image', 'length', 'max' => 255),
           // array('category_image', 'file', 'types' => 'jpg, gif, png', 'on' => 'add'), //
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, category_name, category_image, is_active, is_sort', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'item' => array(self::HAS_MANY, 'Item', 'category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_image' => 'Category Image',
            'is_active' => 'Active / Inactive',
            'is_sort' => 'is_sort',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_name', $this->category_name, true);
        $criteria->compare('category_image', $this->category_image, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('is_sort', $this->is_sort);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function scopes() {
        return array(
            'bysort' => array('order' => 'is_sort ASC'),
        );
    }
}
