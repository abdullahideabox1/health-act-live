<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property integer $user_type
 * @property integer $is_menu
 * @property integer $is_schedule
 * @property integer $is_venue
 * @property integer $is_order
 * @property integer $is_customer
 * @property integer $is_active
 * @property string $date_added
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $confirm_password;

    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('first_name, email, date_added', 'required'),
            array('password,confirm_password', 'required'),
            array('user_type, is_menu, is_schedule, is_venue, is_order, is_customer,is_delivery,is_kitchen, is_active', 'numerical', 'integerOnly' => true),
            array('first_name, last_name, email', 'length', 'max' => 50),
            array('password', 'length', 'max' => 255),
            array('password', 'compare', 'compareAttribute' => 'confirm_password', 'message' => 'Password must be same.'),
            array('email', 'email'),
            array('email', 'unique', 'message' => 'It is already been registered'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, first_name, last_name, email, password, user_type, is_menu, is_schedule, is_venue, is_order, is_customer,is_delivery,is_kitchen, is_active, date_added', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'user_type' => 'User Type',
            'is_menu' => 'Can Add Menu?',
            'is_schedule' => 'Can Add Time?',
            'is_venue' => 'Can Add Areas?',
            'is_order' => 'Can View Order?',
            'is_customer' => 'Can View Customer?',
            'is_delivery' => 'Can View Deliverables?',
            'is_kitchen' => 'Can View Kitchen?',
            'is_active' => 'Active / Inactive',
            'date_added' => 'Date Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('is_menu', $this->is_menu);
        $criteria->compare('is_schedule', $this->is_schedule);
        $criteria->compare('is_venue', $this->is_venue);
        $criteria->compare('is_order', $this->is_order);
        $criteria->compare('is_customer', $this->is_customer);
        $criteria->compare('is_delivery', $this->is_delivery);
        $criteria->compare('is_kitchen', $this->is_kitchen);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
