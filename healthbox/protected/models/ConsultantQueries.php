<?php

/**
 * This is the model class for table "consultant_queries".
 *
 * The followings are the available columns in table 'consultant_queries':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $consultant_name
 * @property string $appointment_time
 * @property string $date
 * @property string $comments
 * @property string $date_added
 */
class ConsultantQueries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'consultant_queries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone, email, consultant_name, appointment_time, date', 'required'),
			array('comments, date_added', 'safe'),
			array('name, phone, email, consultant_name, appointment_time, date', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, phone, email, consultant_name, appointment_time, date, comments, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'phone' => 'Phone',
			'email' => 'Email',
			'consultant_name' => 'Consultant Name',
			'appointment_time' => 'Appointment Time',
			'date' => 'Date',
			'comments' => 'Comments',
			'date_added' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('consultant_name',$this->consultant_name,true);
		$criteria->compare('appointment_time',$this->appointment_time,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConsultantQueries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
