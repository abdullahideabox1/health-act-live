<?php

/**
 * This is the model class for table "mealplans".
 *
 * The followings are the available columns in table 'mealplans':
 * @property integer $id
 * @property integer $tab_id
 * @property string $title
 * @property string $image
 * @property string $sub_title
 * @property string $total_price
 * @property string $free_meal
 * @property string $delivery_charges
 * @property string $price_entire_meal
 * @property string $meal_gst
 * @property string $without_saturday
 * @property string $without_saturday_gst
 * @property string $date_created
 */
class Mealplans extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mealplans';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tab_id, title, image, sub_title', 'required'),
			array('total_price, free_meal, delivery_charges, price_entire_meal, meal_gst, without_saturday, without_saturday_gst, date_created', 'safe'),
			array('tab_id', 'numerical', 'integerOnly'=>true),
			array('title, image, sub_title, total_price, free_meal, delivery_charges, price_entire_meal, meal_gst, without_saturday, without_saturday_gst', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tab_id, title, image, sub_title, total_price, free_meal, delivery_charges, price_entire_meal, meal_gst, without_saturday, without_saturday_gst, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tab_id' => 'Meal Plans Tab',
			'title' => 'Meal Plans Title',
			'image' => 'Meal Plans Tab Image',
			'sub_title' => 'Meal Plans Sub Title',
			'total_price' => 'Total Price',
			'free_meal' => 'Free Meal',
			'delivery_charges' => 'Delivery Charges',
			'price_entire_meal' => 'Total Price for Entire Meal Plan',
			'meal_gst' => 'Total Price for Entire Meal Plan + GST',
			'without_saturday' => 'Total Price for Entire Meal Plan (Without Saturday)',
			'without_saturday_gst' => 'Total Price for Entire Meal Plan (Without Saturday) + GST',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tab_id',$this->tab_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('sub_title',$this->sub_title,true);
		$criteria->compare('total_price',$this->total_price,true);
		$criteria->compare('free_meal',$this->free_meal,true);
		$criteria->compare('delivery_charges',$this->delivery_charges,true);
		$criteria->compare('price_entire_meal',$this->price_entire_meal,true);
		$criteria->compare('meal_gst',$this->meal_gst,true);
		$criteria->compare('without_saturday',$this->without_saturday,true);
		$criteria->compare('without_saturday_gst',$this->without_saturday_gst,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mealplans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
