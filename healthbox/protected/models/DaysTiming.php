<?php

/**
 * This is the model class for table "days_timing".
 *
 * The followings are the available columns in table 'days_timing':
 * @property integer $id
 * @property integer $day_id
 * @property string $timing_from
 * @property string $timing_to
 * @property integer $is_active
 */
class DaysTiming extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'days_timing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('day_id, timing_from, timing_to, is_active', 'required'),
            array('day_id, is_active', 'numerical', 'integerOnly' => true),
            array('timing_from, timing_to', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, day_id, timing_from, timing_to, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'days' => array(self::BELONGS_TO, 'Weekdays', 'day_id'),
        );
    }

    public function scopes()
    {
        return array(
            'bystatus' => array('order' => 'day_id ASC'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'day_id' => 'Day',
            'timing_from' => 'Timing From',
            'timing_to' => 'Timing To',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('day_id', $this->day_id);
        $criteria->compare('timing_from', $this->timing_from, true);
        $criteria->compare('timing_to', $this->timing_to, true);
        $criteria->compare('is_active', $this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DaysTiming the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
