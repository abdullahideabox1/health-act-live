<?php

/**
 * This is the model class for table "user_menu_detail".
 *
 * The followings are the available columns in table 'user_menu_detail':
 * @property integer $id
 * @property integer $user_menu_id
 * @property string $product_name
 * @property integer $product_id
 * @property string $product_price
 * @property string $calender_date
 * @property string $calender_day
 * @property string $calender_type
 * @property string $product_quantity
 * @property string $delivery_time
 * @property string $option_value
 * @property string $option_value_id
 */
class UserMenuDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_menu_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_name, product_id', 'required'),
			array('user_menu_id, product_id', 'numerical', 'integerOnly'=>true),
			array('product_name, product_price, calender_date, calender_day, calender_type, product_quantity, delivery_time, option_value, option_value_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_menu_id, product_name, product_id, product_price, calender_date, calender_day, calender_type, product_quantity, delivery_time, option_value, option_value_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_menu_id' => 'User Menu',
			'product_name' => 'Product Name',
			'product_id' => 'Product',
			'product_price' => 'Product Price',
			'calender_date' => 'Calender Date',
			'calender_day' => 'Calender Day',
			'calender_type' => 'Calender Type',
			'product_quantity' => 'Product Quantity',
			'delivery_time' => 'Delivery Time',
			'option_value' => 'Option Value',
			'option_value_id' => 'Option Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_menu_id',$this->user_menu_id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('product_price',$this->product_price,true);
		$criteria->compare('calender_date',$this->calender_date,true);
		$criteria->compare('calender_day',$this->calender_day,true);
		$criteria->compare('calender_type',$this->calender_type,true);
		$criteria->compare('product_quantity',$this->product_quantity,true);
		$criteria->compare('delivery_time',$this->delivery_time,true);
		$criteria->compare('option_value',$this->option_value,true);
		$criteria->compare('option_value_id',$this->option_value_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserMenuDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
