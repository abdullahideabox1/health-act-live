<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.food-ordering.co.uk/demo-epson/Menu.asp?id_r=2 by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Apr 2016 09:08:02 GMT -->
<?php include './protected/views/layouts/head.php'; ?>
<?php
$session = new CHttpSession;               //add this line
$session->open();
?>
<style>
    #navbar {
        background-color: white;
        min-height: 89px;
    }

    #mainmenu {
        margin-top: 10px;
    }

    #pricecolumn {
        margin-top: 15px;
    }

    #myNavbar {
        margin-top: 38px;
        background-color: white;
        border-bottom: 1px solid whitesmoke;
    }

    #basket {
        margin-top: 13px;
    }

    @media screen and (max-width: 320px) {
        #mainmenu {
            margin-top: 10px;
        }

    }

    @media screen and (max-width: 390px) {
        #addItembtn {
            position: relative;
            top: -38px;
            left: -13px;
        }
    }

    #addItembtn {
        position: relative;
        top: -38px;
        left: -8px;
    }

    .main-menu.large, .logo.large {
        height: 80px;
    }

    .main-menu, .logo {
        height: 50px;
        display: table-cell;
        vertical-align: middle;
    }

    @font-face {
        font-family: "openSans Regular";
        src: url(<?php echo Yii::app()->baseUrl;?>/fonts/OpenSans-Regular-webfont.ttf);
    }

    h3 {
        font-family: "openSans Regular";
    }

    .myfonthead {
        font-size: 36px;
        font-family: 'OpenSans Regular';
        color: #60B49D;
    }

    .myfonts {
        font-weight: lighter;
        font-size: 15px;
        font-family: 'OpenSans Regular';
        color: #60B49D;
    }

    .myfontsDetail {
        font-family: 'OpenSans Regular';
        color: #717172;
    }


</style>
<body style="padding-top: 0px">
<?php include 'healthActheader.php'; ?>

<div class="container" id="wholepage" style="padding-bottom:100px;">

    <?php echo $content ?>

</div>


<script type="text/javascript">


    $(document).ready(function () {

        var hour = 16;
        if (hour < 10) hour = '0' + hour;
        $("select[name=p_hour]").find('option[value=' + hour + ']').attr("selected", true);


        jQuery.validator.setDefaults({
            errorPlacement: function (error, element) {
                // if the input has a prepend or append element, put the validation msg after the parent div
                if (element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            errorElement: "small", // contain the error msg in a small tag
            wrapper: "div", // wrap the error message and small tag in a div
            highlight: function (element) {
                $(element).closest('.control-group').addClass('error'); // add the Bootstrap error class to the control group
            },
            success: function (element) {
                $(element).closest('.control-group').removeClass('error'); // remove the Boostrap error class from the control group
            }
        });

        $("form").removeAttr("novalidate");
        // $("form").validate();
        $("form").validate({
            rules: {
                Email: {
                    required: true,
                    email: true
                }
            }
        });

    });


</script>

<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">


</nav>


</body>
</html>
