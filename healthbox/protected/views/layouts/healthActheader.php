<style>
    @media screen and (max-width: 380px) {
        #logoImage {
            margin-left: 0px;

        }
    }

    @media screen and (min-width: 700px) {
        #logoImage {
            margin-left: 0;
        }
    }
</style>
<nav id="navbar" class="navbar navbar-default">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" style="display: none" data-toggle="collapse"
                    data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="logoa" class="navbar-brand" href="http://www.thehealthact.com"><img id="logoImage"
                                                                                       src="<?php echo Yii::app()->baseUrl; ?>/img/logo.png"
                                                                                       title="logo"
                                                                                       alt="The Health Act Logo"
                                                                                       srcset="<?php echo Yii::app()->baseUrl; ?>/img/logo2.png "></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="http://thehealthact.com/">HOME</a></li>
                <li><a href="http://thehealthact.com/about">ABOUT</a></li>
                <li><a href="http://thehealthact.com/consultation">CONSULTATION</a></li>
                <li><a href="http://thehealthact.com/gift">GIFTS</a></li>
                <li><a href="http://thehealthact.com/healthbox/">MENU</a></li>
                <li><a href="http://thehealthact.com/MealPlans">MEAL PLANS</a></li>
                <li><a href="http://thehealthact.com/cold">JUCIES</a></li>
                <li><a href="http://thehealthact.com/FoodService">FOOD SERVICES</a></li>
                <li><a href="http://thehealthact.com/contact">CONTACT</a></li>
            </ul>
        </div>

</nav>