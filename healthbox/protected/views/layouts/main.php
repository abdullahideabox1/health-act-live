<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.food-ordering.co.uk/demo-epson/Menu.asp?id_r=2 by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Apr 2016 09:08:02 GMT -->
<?php include './protected/views/layouts/head.php'; ?>
<?php
$session = new CHttpSession;               //add this line
$session->open();
?>
<style>
    #navbar {
        background-color: white;
        min-height: 89px;
    }

    #mainmenu {

    }

    #pricecolumn {
        margin-top: 15px;
    }

    #myNavbar {
        margin-top: 38px;
        background-color: white;
        border-bottom: 1px solid whitesmoke;
    }

    #basket {
        margin-top: 13px;
    }

    @media screen and (max-width: 320px) {
        #mainmenu {
            margin-top: 10px;
        }
     
    }
    @media screen and (max-width: 390px) {
        #addItembtn{
            position: relative;
            top: -38px;
            left: -13px;
        }
    }
    #addItembtn{
        position: relative;
        top: -38px;
        left: -8px;
    }
    .main-menu.large, .logo.large {
        height: 80px;
    }

    .main-menu, .logo {
        height: 50px;
        display: table-cell;
        vertical-align: middle;
    }

    @font-face {
        font-family: "openSans Regular";
        src: url(<?php echo Yii::app()->baseUrl;?>/fonts/OpenSans-Regular-webfont.ttf);
    }

    .myfonthead {
        font-size: 32px;
        font-family: 'OpenSans Regular';
        color: #60B49D;
    }

    .myfonts {
        font-weight: lighter;
        font-size: 15px;
        font-family: 'OpenSans Regular';
        color: #60B49D;
    }

    .myfontsDetail {
        font-family: 'OpenSans Regular';
        color: #717172;
    }

</style>
<body style="padding-top: 0px">



<?php include 'healthActheader.php';?>
<div class="container" id="wholepage" style="padding-bottom:100px;">

    <div id="undefined-sticky-wrapper" class="main-menu-wrapper sticky" style="height: 81px;">

        <?php include './protected/views/layouts/mobilecategory.php'; ?>
        <div class="row clearfix">

            <?php include './protected/views/layouts/category.php'; ?>

            <?php include './protected/views/layouts/menu.php'; ?>


            <div class="col-md-3 col-sm-12 col-xs-12 column" id="pricecolumn">

                <div class="panel panel-default" id="beforeorder" style="border-color: #60B49D;">
                    <div class="panel-heading" style="background-color: #60B49D; border-color: #60B49D; color: white;">
                        <h3 class="panel-title">Before your order</h3>
                    </div>
                    <div class="panel-body" style="padding:10px;">


                        <div class="delblock">
                            <div><i class="fa fa-truck fa-2x"></i></div>

                            <div class="order-action-type">


                               <label>
                                   <input type="radio" name="orderTypePicker" value="d"/> Delivery
                                   <span class="outside"><span class="inside"></span></span>
                               </label>

                            </div>

                        </div>
                        <div class="delblock">

                            <div><i class="fa fa-user fa-2x"></i></div>


                            <div class="order-action-type">
                                <label>
                                    <input type="radio" name="orderTypePicker" value="c"/>
                                    Pick-up
                                    <span class="outside"><span class="inside"></span></span>
                                </label>
                            </div>
                        </div>

                        <div id="DeliveryTime" class="form-group" style=" text-align:center;display: none">
                            <label for="control-label" style="margin-top: 10px;"></label>
                            <div class="clearfix" style="margin-top: 14px;"></div>
                            <div class="col-md-12 col-xs-12 input-group" style="">
                                <?php $areas = Areas::model()->findAll("is_active = :is_active", array("is_active" => 1)) ?>
                                <select class=" form-control" style="margin-bottom: 10px; "name="areas"
                                        onchange="areas(this.value)">
                                    <option>Select Area</option>
                                    <?php if ($areas) {
                                        foreach ($areas as $area) { ?>
                                            <option
                                                value="<?php echo $area['id'] ?>"><?php echo $area['area'] ?></option>
                                        <?php }
                                    } ?>
                                </select>
                                <div class="row">
                                    <div class="col-md-4 col-xs-4" data-toggle="tooltip" data-placement="bottom"
                                         title="Delivery Time">
                                        <i class="glyphicon glyphicon-time"> </i>
                                        <span id="del_time" style="font-size: 11px"> 0 min</span>
                                    </div>
                                    <div class="col-md-4 col-xs-4" data-toggle="tooltip" data-placement="bottom"
                                         title="Delivery Fee">
                                        <i><img src="<?php echo Yii::app()->baseUrl; ?>/img/bike.png" width="15"
                                                height="15"> </i>

                                        <span id="del_fee" style="font-size: 11px"> Rs. 0</span>
                                    </div>
                                    <!--<div class="col-md-4 col-xs-4" data-toggle="tooltip" data-placement="bottom"
                                         title="Minimum Order">
                                        <i ><img src="<?php /*echo Yii::app()->baseUrl; */?>/img/doller.png" width="15"
                                                                                 height="15"></i>

                                        <span id="min_order" style="font-size: 11px"> Rs. 0</span>
                                    </div>-->
                                </div>
                                <script>
                                    function areas(val) {
                                        $.ajax({
                                                url: "<?php echo Yii::app()->baseUrl . '/site/areasselection'?>",
                                                data: {
                                                    id: val
                                                },
                                                type: "POST",
                                                dataType: "json",
                                            })
                                            .done(function (json) {
                                                $("#del_time").html(" " + json.delivery_time + " mins");
                                                $("#delivery_time").html(json.delivery_time + " mins");
                                                if(json.delivery_fee > 0 ){
                                                    $("#del_fee").html(" Rs." + json.delivery_fee);
                                                }else{
                                                    $("#del_fee").html("Free");
                                                }

                                                $("#min_order").html(" Rs." + json.min_order);
                                                $('input[name=deliveryDistance]').val(json.delivery_time);
                                                $('input[name=min_order]').val(json.min_order);
                                            })
                                    }
                                </script>
                            </div>


                            <div class="visible-md"><br><br>
                            </div>

                        </div>
                        <div class="clear-both">
                        </div>


                    </div>
                </div>


                <div id="rightaffix" data-spy="affix" data-offset-top="300" data-offset-bottom="200">
                    <div class="panel panel-primary" id="basket">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon glyphicon-shopping-cart"></span>
                                Your
                                order</h3>
                        </div>
                        <div class="panel-body" id="footerbasket">

                            <?php //include './protected/views/layouts/cart.php';
                            echo $content; ?>


                        </div>
                    </div>

                    <?php include './protected/views/layouts/timing.php'; ?>

                    <!--
                    <img src="Images/nochex.png" class="img-responsive"  id="nochexlogo"><br>
                    <a href="foodalergies.html"  id="foodalergieslink">Food Alergies</a>
                    -->
                </div>


            </div>
        </div>

    </div>


    <?php include './protected/views/layouts/js.php'; ?>

    <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">


        <div class="navbar-header" style="float:right;">


            <div class="navbar-brand"  style="padding: 10px 15px;"><span class="label label-success" id="addtobasket"
                                            style="float:left;margin-right:10px;">Added</span>

                <button type="button" id="butbasket" class="btn btn-success btn-sm"><span
                        class="glyphicon glyphicon-shopping-cart"></span> <b>BASKET</b> <span
                        id="shoppingcart2"></span>
                </button>

            </div>
        </div>


    </nav>


    <div id="ClosedModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        Closed</h3>
                </div>
                <div class="modal-body">
                    <p>
                        Sorry, <b>
                            Taste Good</b> is closed at the moment.<br/>
                        However, you can place an order now for delivery at a later time.<br/>
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="DeliveryModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        Delivery not possible</h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="OptionModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        You forgot to make a selection!</h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="AnnouncementModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        Announcement</h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="timeslotModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        Warning</h3>
                </div>
                <div class="modal-body">
                    Delivery is not available during your selected timeslot. Please check our opening times.
                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="timeslotModal2" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3 style="color: red">
                        Warning</h3>
                </div>
                <div class="modal-body">
                    Collection is not available during your selected timeslot. Please check our opening times.
                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn btn-primary">Ok!</a>
                </div>
            </div>
        </div>
    </div>


    <script>

        var nua = navigator.userAgent;
        var is_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));
        if (is_android) {
            $('#bs-example-navbar-collapse-1').removeClass("scrollable-menu");


        }

    </script>


</body>
</html>
