<script type="text/javascript">

    $(function () {
        $("input.noSubmit").keypress(function (e) {
            var k = e.keyCode || e.which;
            if (k == 13) {
                e.preventDefault();
            }
        });
    });

    var jsDate = {
        1: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '1',
            delivery: 'y',
            collection: 'y'
        },
        2: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '2',
            delivery: 'y',
            collection: 'y'
        },
        3: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '3',
            delivery: 'y',
            collection: 'y'
        },
        4: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '4',
            delivery: 'y',
            collection: 'y'
        },
        5: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '5',
            delivery: 'y',
            collection: 'y'
        },
        6: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '6',
            delivery: 'y',
            collection: 'y'
        },
        7: {
            min: Date.parse('01/01/2011 16:30'),
            max: Date.parse('01/01/2011 23:59'),
            d: '7',
            delivery: 'y',
            collection: 'y'
        }
    };
    var myDays = ["Monday", "Tuesday", "Wednesday",
        "Thursday", "Friday", "Saturday", "Sunday"]

    function ReloadShop() {

        //$("#shoppingcart").load("ShoppingCartb7ea.html?id_r=2");

    }

    $("form").submit(function (event) {

        event.preventDefault();
        var item_id = $(this).find($('input[name=item]')).val();
        if($('#option_value_id_'+item_id).val() == 0){
            $("#OptionModal div.modal-body").html('Please specify how you would like your meal');
            $("#OptionModal").modal();
            return false;
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: $("form").attr("action"),
            data: $(this).serialize(), // serializes the form's elements.
            success: function (data) {
                $('#shoppingcart table').prepend('<tr><td>' + data.item_name + '<br/>(x ' + data.qty + ')</td><td>Rs.' + data.price + '</td>' +
                    '<td ><button class="mybtn" onclick="Deleteitem(' + data.item_id + ')">X</button></td></tr>');
                $('#subtotal').html(data.total_price);
                $('#total_price').show();
                // alert(data.qty); // show response from the php script.
                // $('#shoppingcart').remove();

                var html = $.ajax({
                    url: "<?php echo $this->createUrl('site/cartReload'); ?>",
                    success: function (data) {
                        $('#shoppingcart').html(data);
                    }
                });


                //($('#shoppingcart').load();
            }
        });

    });


    function Deleteitem(elem, count) {

        $.ajax({
            type: "POST",
            url: '<?php echo Yii::app()->baseUrl."/site/deleteitem"?>',
            data: 'item_id=' + elem + '&item_count=' + count, // serializes the form's elements.
            success: function (data) {
                var html = $.ajax({
                    url: "<?php echo $this->createUrl('site/cartReload'); ?>",
                    success: function (data) {
                        $('#shoppingcart').html(data);
                    }
                });
            }
        });
    }


    function Showdishproperties(itemtoshow) {

        $("#" + itemtoshow).slideToggle();

    }


    function CheckOrder() {

        try {
            var delivery_type = $('input[name=orderTypePicker]:checked').val();

            if (!delivery_type) {

                $('#beforeorder').css('border-color', 'red');
                $('#beforeorder').css('border-width', '4px');
                scrollTo("beforeorder");
                //$("#BeforeYouOrder").modal();
                return false;
            }

            var form = $("#cartform");
            $('input[name=deliveryType]', form).val(delivery_type);
            if (delivery_type == 'd') {
                var distance = $('input[name=deliveryDistance]').val();
                if (!distance) {
                    $('#beforeorder').css('border-color', 'red');
                    $('#beforeorder').css('border-width', '4px');
                    $("#DeliveryModal div.modal-body").html('Please select area.');
                    $("#DeliveryModal").modal();
                    return false;
                }
                var min_amount = $('input[name=min_order]').val();
                if (!min_amount) {
                    min_amount = 0;
                }

                var total = parseFloat($("#SubTotal").val());

                if (min_amount > 0 && total < min_amount) {

                    $("#DeliveryModal div.modal-body").html('Sorry. Minimum Order for Delivery is Rs. ' + min_amount);
                    $("#DeliveryModal").modal();
                    return false;
                }
            }


            // check to see if current time is greater than delivery or collection time
            var dt = $("#DeliveryTime");
            var _sTime = distance;

            var timing_from = '<?php echo strtotime(Yii::app()->session['timing_from']) ?>';
            var timing_to = '<?php echo strtotime(Yii::app()->session['timing_to']) ?>';
            var cur_time = '<?php echo strtotime(date("h:i:sa"))?>';

            if (cur_time < timing_to && cur_time > timing_from) {
                $('#beforeorder').css('border-color', '#E9EAEB');

                window.location.href = "<?php echo Yii::app()->baseUrl.'/site/proceed/type/'?>" + delivery_type;
                return true;

            } else {
                $("#timeslotModal div.modal-body").html('Sorry. We are close now.');
                $("#timeslotModal").modal();
                return false;
            }
        }
        catch (ex) {
            return false;
        }


    }


    $(function () {

        $("input[name=orderTypePicker]").click(function () {
            if ($(this).val() == 'd') {
                $("#DeliveryTime").show();
                $("#DeliveryTime label").text("");
            }
            else {
                $("#DeliveryTime").hide();
                $("#DeliveryTime label").text("Collection Time *");
            }
        });

        jQuery.validator.setDefaults({
            errorPlacement: function (error, element) {
                if (element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            errorElement: "small",
            wrapper: "div",
            highlight: function (element) {
                $(element).closest('.control-group').addClass('error');
            },
            success: function (element) {
                $(element).closest('.control-group').removeClass('error');
            }
        });

        $("form").removeAttr("novalidate");
        $("form").validate();

        $.ajaxSetup ({
            cache: false
        });

        //  ReloadShop();


        //  $("#ClosedModal").modal();


    });
</script>