<div class="panel panel-danger" style="border-color: #60B49D;margin-bottom: 100px;">
    <div class="panel-heading"  style="border-color: #60B49D; background-color: #60B49D; color: white">
        <h3 class="panel-title">Our opening hours</h3>
    </div>
    <div class="panel-body">
        <?php $timing = DaysTiming::model()->findAll("is_active = :is_active", array("is_active" => 1)) ?>
        <table border="0" width="100%">
            <?php if ($timing) {
                foreach ($timing as $days) { ?>
                    <tr>
                        <td style="width: 30px">
                            <?php echo $days['days']['day_added'] ?>
                        </td>
                        <td>
                            <div align="right"> <?php echo $days['timing_from'] ?>
                                - <?php echo $days['timing_to'] ?>
                            </div>
                        </td>
                        <script type="text/javascript">
                            $(function () {
                                $("[data-toggle=tooltip]").tooltip();
                            });
                        </script>

                    </tr>
                <?php }
            } ?>
        </table>
    </div>
</div>