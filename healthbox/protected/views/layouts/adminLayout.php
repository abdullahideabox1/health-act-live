<?php
$page = "Admin Dashbaord";
$title = "Admin Dashboard";

$userType = "Admin";
?>

<?php
// set resource path for linking resources in subdirectories
$respath = Yii::app()->request->baseUrl . '/';
?>

<!DOCTYPE html>
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]-->

<head>
    <!-- Jquery -->
    <script src="<?php echo $respath ?>js/admin/jquery.js"></script>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title> Food Ordering - <?php if (isset($title)) echo $title; ?></title>
    <meta name="description" content="Food Ordering">


    <?php if (Yii::app()->controller->action->id == 'kitchen') { ?>
        <meta http-equiv="refresh" content="30;URL='<?php echo Yii::app()->baseUrl . '/admin/kitchen' ?>'">
    <?php } else if (Yii::app()->controller->action->id == 'pending') { ?>
        <meta http-equiv="refresh" content="30;URL='<?php echo Yii::app()->baseUrl . '/admin/pending' ?>'">
    <?php } ?>
    <meta name="author" content="Ansonika">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->

    <link href="<?php echo $respath ?>css/admin/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/bootstrap-responsive.css" rel="stylesheet">

    <link href="<?php echo $respath ?>css/admin/megamenu.css" rel="stylesheet">

    <link href="<?php echo $respath ?>css/admin/style.css" rel="stylesheet">

    <link href="<?php echo $respath ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/admincss.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <!-- Support media queries for IE8 -->
    <script src="<?php echo $respath ?>js/admin/respond.min.js"></script>

    <!-- HTML5 and CSS3-in older browsers-->
    <script src="<?php echo $respath ?>js/admin/modernizr.custom.17475.js"></script>

    <!-- jquery validdte plugin -->

    <script src="<?php echo $respath ?>js/admin/jquery.validate.js"></script>


    <!--[if IE 7]>
    <link rel="stylesheet" href="../font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

</head>

<style>
    table tr td {
        text-align: center !important;
    }

    table tr th {
        text-align: center !important;
    }
</style>


<body>
<div class="page-wrap">
    <!--[if !IE]><!-->
    <script>if (/*@cc_on!@*/false) {
            document.documentElement.className += ' ie10';
        }</script>
    <!--<![endif]--> <!-- Border radius fixed IE10-->


    <header>
        <div class="container">
            <div class="row">
                <div class="span4" id="logo"><a href="<?php echo Yii::app()->baseUrl . '/admin' ?>"><img
                            src="<?php echo Yii::app()->baseUrl . '/images/ideabox png-01.png' ?>" alt="Logo"
                            width="120" height="100"></a></div>
                <div class="span8">

                    <div id="menu-top">
                        <ul>
                            <li><i class="icon-gears icon-2x text-success"></i><a
                                    href="<?php echo Yii::app()->baseUrl . '/admin/' ?>"
                                    title="Admin Dashbaord">Admin Area</a>
                                &nbsp;
                                &nbsp;
                            </li>
                            <?php if (Yii::app()->session['userType'] != 1) { ?>
                                <li><i class="icon-gears icon-2x text-success"></i><a
                                        href="<?php echo Yii::app()->baseUrl . '/admin/changepassword/' . Yii::app()->session['userId'] ?>"
                                        title="Admin Dashbaord">Change Profile</a>
                                    &nbsp;
                                    &nbsp;
                                </li>
                            <?php } ?>
                            <li><i class="icon-ban-circle icon-2x text-error"></i><a
                                    href="<?php echo Yii::app()->baseUrl . '/site/logout' ?>"
                                    title="logout">Logout</a></li>

                        </ul>


                    </div>

                </div>
            </div>
        </div>
    </header>


    <nav>
        &nbsp;
    </nav>

    <!-- header.php end -->


    <div class="container">
        <div class="row">

            <!-- =========================Start Col left section ============================= -->
            <aside class="span3">
                <div class="col-left">
                    <?php $userRight = $this->userRights(); ?>
                    <div id="student_control">
                        <h3>Menu</h3>
                        <?php if (Yii::app()->session['userType'] == 1) { ?>
                            <a class="accrodian-trigger" href="#">User</a>

                            <div style="display: none;" id="userAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/users' ?>">Add User </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/userlisting' ?>">
                                            Listing</a></li>

                                </ul>

                            </div>
                        <?php } ?>



                        <?php if ($userRight['is_menu']) { ?>
                            <a class="accrodian-trigger" href="#">Menu</a>

                            <div style="display: none;" id="menuAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/categorylisting' ?>">Categories
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/itemlisting' ?>">Items
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/subitemlisting' ?>">Sub-Selections
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/subitemoptionlisting' ?>">Selection
                                            Options
                                        </a></li>

                                </ul>

                            </div>
                        <?php } ?>

                        <?php if ($userRight['is_venue']) { ?>
                            <a class="accrodian-trigger" href="#">Area</a>

                            <div style="display: none;" id="areaAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/arealisting' ?>">Areas
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if ($userRight['is_schedule']) { ?>
                            <a class="accrodian-trigger" href="#">Days</a>

                            <div style="display: none;" id="daysAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/dayslisting' ?>">Days/Time
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if ($userRight['is_schedule']) { ?>
                            <a class="accrodian-trigger" href="#">Promotions</a>

                            <div style="display: none;" id="daysAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/promotionlisting' ?>">Promotion
                                            Code Listing
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>


                        <?php if ($userRight['is_delivery']) { ?>
                            <a class="accrodian-trigger" href="#">Delivery</a>

                            <div style="display: none;" id="deliveryAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/pending' ?>">Pending
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/prepared' ?>">Prepared
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/sent' ?>">Sent
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/return' ?>">Return
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>


                        <?php if ($userRight['is_kitchen']) { ?>
                            <a class="accrodian-trigger" href="#">Kitchen</a>

                            <div style="display: none;" id="kitchenAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/kitchen/1' ?>">Kitchen
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if ($userRight['is_customer']) { ?>
                            <a class="accrodian-trigger" href="#">Customer</a>

                            <div style="display: none;" id="customerAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/customerlisting' ?>">Customer
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>


                        <a class="accrodian-trigger" href="#">Report</a>

                        <div style="display: none;" id="customerAccordian" class="accrodian-data">

                            <ul class="list_2">
                                <li><a href="<?php echo Yii::app()->baseUrl . '/admin/saleslisting' ?>">Sales Reports
                                    </a></li>
                                <li><a href="<?php echo Yii::app()->baseUrl . '/admin/kitchenreportlisting' ?>">Kitchen
                                        Reports
                                    </a></li>
                                <li><a href="<?php echo Yii::app()->baseUrl . '/admin/reportlisting' ?>">Item Reports
                                    </a></li>
                            </ul>

                        </div>
                        <?php if ($userRight['is_meal_plan']) { ?>
                            <a class="accrodian-trigger" href="#">Meal Plans</a>

                            <div style="display: none;" id="mealPlanAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplanlisting' ?>">Meal
                                            Plans Listings
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>
                        <?php if ($userRight['is_frontend']) { ?>
                            <a class="accrodian-trigger" href="#">Front End</a>

                            <div style="display: none;" id="FrontEndAccordian" class="accrodian-data">

                                <ul class="list_2">
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/imageslisting' ?>">Front End
                                            Images Listings
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/tabslisting' ?>">Tabs Listings
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/consultantlisting' ?>">Consultant
                                            Listings
                                        </a></li>
                                    <li>
                                        <a href="<?php echo Yii::app()->baseUrl . '/admin/consultantschedulelisting' ?>">Consultant
                                            Schedule Listings
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/consultantquerieslisting' ?>">Consultant
                                            Queries Listings
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplanslisting' ?>">Meal
                                            Plans Listings
                                        </a></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplansitemslisting' ?>">Meal
                                            Plans Items Listings
                                        </a></li>
                                </ul>

                            </div>
                        <?php } ?>
                    </div>
                    <!--Admin control-->

                </div>
                <!--col-left-->

            </aside>


            <!-- =========================Start Col right section ============================= -->


            <section class="span9">
                <?php echo $content ?>
                <!-- end col right-->
            </section>

        </div>
        <!-- end row-->
    </div>
    <!--end container-->


    <div id="toTop">Back to Top</div>


</div>
<!--page wrap sticky footer fix -->

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="span12 text-center">
                <span class="text-info">&#169; Copyright <?php echo date('Y'); ?> </span>
            </div>
        </div>
    </div>

</footer>


<!-- End footer-->


<!-- MEGAMENU -->
<script src="<?php echo $respath ?>js/admin/jquery.easing.js"></script>
<script src="<?php echo $respath ?>js/admin/megamenu.js"></script>

<!-- OTHER JS -->
<script src="<?php echo $respath ?>js/admin/bootstrap.js"></script>
<script src="<?php echo $respath ?>js/admin/functions.js"></script>
<script src="<?php echo $respath ?>js/admin/validate.js"></script>

<script type="text/javascript">
    function play_sound() {
        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', 'http://thehealthact.com/healthbox//upload/martian-gun.mp3');
        audioElement.setAttribute('autoplay', 'autoplay');
        audioElement.load();
        audioElement.play();
    }
    <?php if(Yii::app()->session['repeat_max_order_id'] != Yii::app()->session['max_order_id']){ ?>
    play_sound();
    <?php }?>

</script>


<script>
    function show_confirm() {
        return confirm("Are you sure you want to delete this item?");
    }
</script>

</body>
</html>