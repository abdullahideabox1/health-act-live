<div class="col-md-2" id="categories">

    <div data-spy="affix" data-offset-top="60" data-offset-bottom="200">
        <div style="width:165px; height : 450px; overflow : auto; " class="hidden-xs">
            <ul id="cat" class="nav nav-stacked nav-pills navdesktop"
                style="; width:155px;overflow : auto;    height: 80vh;">
                <div style="margin-bottom: 10px;color: #9BCB3B">
                    <h4><b>Categories</b></h4>
                </div>


                <?php $categories = Category::model()->with('item')->findAll('t.is_active = :is_active AND item.is_active = 1 AND t.id != 6', array(':is_active' => 1)) ?>

                <?php if ($categories) {
                    foreach ($categories as $cat) { ?>
                        <li><a href="#p<?php echo $cat['id'] ?>" class="catlink asdf" data-option="opt-<?php echo $cat['id'] ?>" >
                                <?php echo $cat['category_name'] ?></a></li>
                    <?php }
                } ?>

            </ul>
        </div>
    </div>
</div>
<script>

    $('[data-option^="opt-"]').click(function () {
        var id = $(this).data('option').split('-');
        slidesmooth($('#p' + id[1]));
    });
    function slidesmooth(ele) {

        var body = $("html, body");
        body.animate({
            scrollTop:ele.offset().top,
            scrollLeft:ele.offset().left
        },1000)
        //   $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
    }
</script>