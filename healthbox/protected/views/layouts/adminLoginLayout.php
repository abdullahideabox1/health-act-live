<?php
$page = "Admin Dashbaord";
$title = "Admin Dashboard";

$userType = "Admin";
?>

<?php
// set resource path for linking resources in subdirectories
$respath = Yii::app()->request->baseUrl . '/';
?>

<!DOCTYPE html>
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]-->

<head>
    <!-- Jquery -->
    <script src="<?php echo $respath ?>js/admin/jquery.js"></script>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title> Food Ordering - <?php if (isset($title)) echo $title; ?></title>
    <meta name="description" content="Food Ordering">
    <meta name="author" content="Ansonika">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->

    <link href="<?php echo $respath ?>css/admin/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/bootstrap-responsive.css" rel="stylesheet">

    <link href="<?php echo $respath ?>css/admin/megamenu.css" rel="stylesheet">

    <link href="<?php echo $respath ?>css/admin/style.css" rel="stylesheet">

    <link href="<?php echo $respath ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/admincss.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $respath ?>js/admin/fancybox/source/jquery.fancybox63b9.css?v=2.1.4">


    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if IE 7]>
    <link rel="stylesheet" href="../font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

</head>

<body>
<div class="page-wrap">
    <!--[if !IE]><!-->
    <script>if (/*@cc_on!@*/false) {
            document.documentElement.className += ' ie10';
        }</script>
    <!--<![endif]--> <!-- Border radius fixed IE10-->


    <header>
        <div class="container">
            <div class="row">
                <div class="span4" id="logo"><a href="<?php echo Yii::app()->baseUrl.'/admin'?>"><img src="<?php echo Yii::app()->baseUrl.'/images/ideabox png-01.png'?>" alt="Logo"
                                                                         width="120" height="100"></a></div>
                <div class="span8">

                    <div id="menu-top">



                    </div>

                </div>
            </div>
        </div>
    </header>


    <nav>
        &nbsp;
    </nav>

    <!-- header.php end -->


    <div class="container">
        <div class="row">




            <section class="span12">
                <?php echo $content ?>
                <!-- end col right-->
            </section>

        </div>
        <!-- end row-->
    </div>
    <!--end container-->


    <div id="toTop">Back to Top</div>


</div>
<!--page wrap sticky footer fix -->

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="span12 text-center">
                <span class="text-info">&#169; Copyright <?php echo date('Y'); ?> </span>
            </div>
        </div>
    </div>

</footer>


<!-- End footer-->





</body>
</html>