<nav class="navbar navbar-default  navmobile" role="navigation"style=" border-radius: 4px;   margin-top: 20px;" >
    <div class="container-fluid" >
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Categories</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse scrollable-menu" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $categories = Category::model()->with('item')->findAll('t.is_active = :is_active AND item.is_active = 1', array(':is_active' => 1)) ?>

                <?php if ($categories) {
                    foreach ($categories as $cat) { ?>
                        <li><a href="javascript:;" class="movedown" data="p<?php echo $cat['id'] ?>">
                                <?php echo $cat['category_name'] ?></a></li>
                    <?php }
                } ?>
                
            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>