<div class="col-md-9 col-sm-12 col-xs-12 column" id="mainmenu">
    <ul class="nav nav-stacked nav-pills">

    </ul>

    <?php //$this->pre($_SESSION);
    $categories = Category::model()->with('item')->findAll('t.is_active = :is_active AND item.is_active = 1 AND t.id != 6', array(':is_active' => 1)) ?>
    <?php if ($categories) {
        foreach ($categories as $cat) {
            ?>

            <a style="font-size: 24px; border: 1px solid transparent" id="p<?php echo $cat['id'] ?>"
               name="p<?php echo $cat['id'] ?>"></a>
            <h4 class="cat-name">
                <?php echo $cat['category_name'] ?>
            </h4>
            <div class="col-md-12 col-lg-12 col-sm-12 pl-0 pr-0 p-sm-0">
                <?php if ($cat['item']) {
                    ?>
                    <img width="100%" class="hidden"
                         src="<?php echo Yii::app()->baseUrl . '/upload/category/' . $cat['category_image'] ?>">
                    <?php
                    foreach ($cat['item'] as $item) { ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl-0 pr-45 category-items min-h-500 p-sm-0 mb-10">
                            <form method="POST" action="<?php echo Yii::app()->baseUrl . '/site/addtocart/' ?>">
                                <div class="text-center mb-10">
                                    <img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/upload/ItemImages/<?php echo $item['item_image'] ?>"
                                        width="100%"/>
                                </div>
                                <input type="hidden" value="<?php echo $item['id'] ?>" name="item">
                                <input type="hidden" value="<?php echo $item['price'] ?>" name="price">

                                <div class="row margin20 clearfix" style="margin-top: 0">

                                    <!--            <div class="product10w code"> <?php /*echo $item['id'] */ ?>.</div>-->


                                    <?php if ($item['subitem'] && $item['subitem'][0]['subitemoption']) { ?>
                                        <div class="pb-15">

                                            <span class="desc item-name">
                                                <?php echo $item['name'] ?>
                                                <i><span class="small"></span></i>
                                            </span>
                                            <span class="pull-right">
                                                <button type="submit" name="submit"
                                                        class="btn-success btnadd myBtnSuccess">
                                                    <!-- <span class="glyphicon glyphicon-plus"></span>-->
                                                   <span> +</span>
                                                </button>
                                            </span>
                                            <span class="pull-right item-price" style="margin-right: 10px">
                                                Rs. <?php echo number_format($item['price']) ?>
                                            </span>
                                            <!--<a class="col-md-1 col-xs-2 btn btn-success btn-xs  dishpropertiesbutton"
                                       onclick="Showdishproperties('item<?php /*echo $item['id'] */ ?>');"
                                       style="padding:5px;">Options</a>-->
                                        </div>
                                        <p class="item-description">
                                            <?php echo $item['description'] ?><br/>
                                        </p>
                                        <div class="row  clearfix dishproperties" id="item<?php echo $item['id'] ?>">

                                            <?php $y = 0;
                                            foreach ($item['subitem'] as $subitem) {
                                                if ($y == 0) { ?>
                                                    <input id="option_value_id_<?php echo $item['id'] ?>" type="hidden"
                                                           name="option_id" value="0">
                                                <?php }
                                                $y++; ?>

                                                <?php if ($subitem['subitemoption']) { ?>
                                                    <div class="col-md-12 col-xs-12 desc">
                                                        <h5 class="color-green addons-heading"><?php echo $subitem['heading'] ?></h5>
                                                        <?php if ($subitem['subitemoption']) {
                                                            $selectionType = $subitem['selection_type'];
                                                            foreach ($subitem['subitemoption'] as $option) {
                                                                if ($option['is_active']) {
                                                                    if ($selectionType == 1) { ?>
                                                                        <div class="addons-items">
                                                                           <label>
                                                                               <input style="margin: 5px" type="radio" class="radio-inline"
                                                                                      onchange="ajaxoption('<?php echo $item['id'] ?>','<?php echo $option['id'] ?>')"
                                                                                      name="<?php echo $subitem['heading'] ?>"
                                                                                      value="<?php echo $option['id'] ?>">
                                                                               <span class="outside"><span class="inside"></span></span>
                                                                               <?php echo $option['option_name'] ?>
                                                                           </label>
                                                                        </div>
                                                                    <?php } else { ?>
                                                                        <div>
                                                                            <input
                                                                                type="hidden"
                                                                                name="<?php echo $option['option_name'] ?>"
                                                                                value="0"/>

                                                                            <input style="margin: 5px" type="checkbox"
                                                                                   name="<?php echo $option['option_name'] ?>"
                                                                                   value="<?php echo $option['id'] ?>"><?php echo $option['option_name'] ?>

                                                                        </div>
                                                                    <?php }
                                                                } ?>
                                                            <?php }
                                                        } ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>


                                    <?php } else { ?>
                                        <span class="desc item-name">
<?php echo $item['name'] ?>

                                            <i><span class="small"></span></i>
                                        </span>


                                        <span class="pull-right">
                                           <button type="submit" name="submit"
                                                   class="btn-success btnadd myBtnSuccess">
                                               <!-- <span class="glyphicon glyphicon-plus"></span>--> +
                                           </button>
                                        </span>
                                        <span class="pull-right item-price" style="margin-right: 10px">
                                            Rs. <?php echo number_format($item['price']) ?>
                                        </span>
                                        <p>
                                            <?php echo $item['description'] ?><br/>

                                        </p>


                                    <?php } ?>
                                    <div class="color-green mt-10 addons-items-details">

                                        <?php if ($item['calory'] > 0) { ?>
                                            Calories <span
                                                id="calory<?php echo $item['id'] ?>"><?php echo (float)$item['calory'] ?></span>
                                            |
                                        <?php } ?>
                                        <?php if ($item['carbo'] > 0) { ?>
                                            Carbs <span
                                                id="carbo<?php echo $item['id'] ?>"><?php echo (float)$item['carbo'] ?></span>
                                            |
                                        <?php } ?>
                                        <?php if ($item['protein'] > 0) { ?>
                                            Protein <span
                                                id="protein<?php echo $item['id'] ?>"><?php echo (float)$item['protein'] ?></span>
                                            |
                                        <?php } ?>
                                        <?php if ($item['fats'] > 0) { ?>
                                            Fats <span
                                                id="fats<?php echo $item['id'] ?>"><?php echo (float)$item['fats'] ?></span>
                                        <?php } ?>


                                        <span>
                                    <span style="display: none"
                                          id="save_calory<?php echo $item['id'] ?>"><?php echo (float)$item['calory'] ?>
                                        <span style="display: none"
                                              id="save_carbo<?php echo $item['id'] ?>"><?php echo (float)$item['carbo'] ?>
                                            <span style="display: none"
                                                  id="save_protein<?php echo $item['id'] ?>"><?php echo (float)$item['protein'] ?></span>
                                            <span style="display: none"
                                                  id="save_fats<?php echo $item['id'] ?>"><?php echo (float)$item['fats'] ?></span>
                                </span>
                                    </div>

                                </div>

                            </form>
                        </div>
                    <?php }
                } ?>
            </div>
        <?php }

    } ?>

</div>


<script>
    function ajaxoption(item_id, option_id) {
        $("#option_value_id_" + item_id).val(1);
        $.ajax({
            url: '<?php echo Yii::app()->baseUrl . '/site/ajaxoption'?>',
            data: {option_id: option_id},
            type: 'POST',
            dataType: 'JSON',
            success: function (output) {
                $("#fats" + item_id).html(output.fats);
                $("#protein" + item_id).html(output.protein);
                $("#carbo" + item_id).html(output.carbo);
                $("#calory" + item_id).html(output.calory);
            }
        });

    }
</script>