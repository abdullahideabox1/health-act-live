<head>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
    <meta charset="utf-8">
    <title>Health Act - Online Food Ordering</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="description" content="">
    <meta name="author" content="">

    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="Scripts/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="Scripts/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">


    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.validate.min.js" type="text/javascript"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfgFHAxCIeGC_xOIqdqQhenL7V1HaWHaw&amp;sensor=false"></script>


    <script>

        $(document).ready(function () {
            $('body').css('padding-top', '0px');

        });
        function scrollTo(id) {
            // Scroll
            $('html,body').animate({scrollTop: $("#" + id).offset().top - 160}, 'slow');
        }

        $(window).load(function () {

            if ($(window).width() <= 1000) {

                $(window).off('.affix');
                $('#rightaffix').removeData('bs.affix').removeClass('affix affix-top affix-bottom');

            }

            $(window).resize(function () {

                if ($(window).width() <= 1000) {

                    $(window).off('.affix');
                    $('#rightaffix').removeData('bs.affix').removeClass('affix affix-top affix-bottom');

                }

            });


            $('.movedown').click(function (e) {
                $('.navbar-collapse').collapse('hide');
                scrollTo($(this).attr('data'), {duration: 'slow', offsetTop: '-100'});

            });
            $('.btnadd').click(function (e) {


                $('#addtobasket').fadeIn('slow', function () {
                    $('#addtobasket').delay(1000).fadeOut('slow');

                });
            });

            $("#butcontinue").click(function () {
                scrollTo("beforeorder");
            });


            $("#butbasket").click(function () {
                scrollTo("basket");
            });
            $(".catlink").hover(function () {
                if ($(this).css('background-color') == 'rgb(155,203,59)') {
                    $(this).css({'background-color': '#60B49D'}).css({'color': 'white'});

                }
            }, function () {
                if ($(this).css('background-color') == 'rgb(155,203,59)') {
                    $(this).css({'background-color': '#f3f3f3'}).css({'color': 'black'});

                }
            });
            $(".catlink").click(function () {

                $("ul#cat li a,ul#cat li.active a").css({'color': 'black'});
                $(".catlink").css({'background-color': '#f3f3f3'});
                $(this).css({'background-color': '#60B49D'}).css({'color': 'white'});
                $(this).removeClass('asdf');
                $(this).addClass('asdf');

            });

        });

        $(document).ready(function () {
            responsive();
            $(window).resize(function () {
                responsive();
            });
            $("form").keypress(function (e) {
                //Enter key
                if (e.which == 13) {
                    CheckDistance();
                    return false;
                }
            });
        });

        function responsive() {
            var winWidth = $(window).width();
            if (winWidth < 992) {
                $("#header").addClass("navbar-fixed-top");
            } else {
                $("#header").removeClass("navbar-fixed-top");
                $("body").css("padding-top", "0px");
            }
        }
    </script>

    <style>
        .nav>li>a{
            padding: 10px 15px;
            letter-spacing: 0;
            font-family: poppins, serif;
            color: rgb(82, 86, 89) !important;
            font-size: 18px;
        }
        #navbar .nav>li>a{
            padding: 2px 0.8rem;
            font-weight: 500;
            letter-spacing: 0;
        }
        nav#navbar{
            box-shadow: 0px 2px 4px #888888;
            border-radius: 0;
        }
        .nav>li>a:hover{
            color: #60B49D !important;
        }

    </style>
</head>