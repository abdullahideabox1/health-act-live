<?php

$condition = "  ";
$criteria = new CDbCriteria;
if (isset($_GET) && $_GET) {

    if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
        if ($_GET['start_date'] && $_GET['end_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $end_date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
        } elseif ($_GET['start_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= CURDATE()";
        } elseif ($_GET['end_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $end_date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
        }
    }
}
$criteria->condition = "1 = 1 $condition";
$criteria->order = "order_date ASC";

$model = CustomerOrder::model()->findAll($criteria);

?>

<h2 style="text-align: center">KITCHEN REPORT
    <h2>

        <table border="1">
            <tr>
                <th>Order Id</th>
                <th>Date</th>
                <th>Recieved Time</th>
                <th>Prepared Time</th>
                <th>Delivered Time</th>
                <th>Return Time</th>
                <th>Order Details</th>
                <th>Item Count</th>
                <th>If Returned</th>
            </tr>

            <tbody>
            <?php
            foreach ($model as $m) {
                $i = 1; ?>
                <tr>
                    <td style="text-align: center"><?php echo $m['id']; ?></td>
                    <td style="text-align: center;width: 100px"><?php echo date('d-m-Y', strtotime($m['order_date'])); ?></td>

                    <?php if (count($m['order_status']) == 1) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                        <?php } ?>
                        <td style="text-align: center"> -</td>
                        <td style="text-align: center"> -</td>
                        <td style="text-align: center"> -</td>
                    <?php }
                    if (count($m['order_status']) == 2 && $m['status'] == 5) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                        <?php } ?>
                        <td> -</td>
                        <td> -</td>
                        <td> -</td>
                    <?php }
                    if (count($m['order_status']) == 2 && $m['status'] == 2) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                            <?php $i++;} ?>
                        <td> -</td>
                        <td> -</td>
                    <?php }
                    if (count($m['order_status']) == 3) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 3 && $status['status_id'] == 3) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 3 && $status['status_id'] == 4) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                            <?php $i++;
                        } ?>
                        <td style="text-align: center"> -</td>
                    <?php } ?>
                    <?php if (count($m['order_status']) == 4) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 3 && $status['status_id'] == 3) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 4 && $status['status_id'] == 4) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                            <?php $i++;
                        } ?>
                    <?php } ?>


                    <td style="text-align: center">
                        <?php foreach ($m['order_detail'] as $order_detail) {
                            $cartitem = json_decode($order_detail['json_data']);
                            echo $cartitem->name . ',';
                        } ?></td>
                    <td style="text-align: center"><?php echo count($m['order_detail']); ?></td>
                    <td style="text-align: center"><?php if ($m['status'] == 4) {
                            echo "Returned";
                        } elseif($m['status'] == 5){
                            echo "Canceled";
                        }else {
                            " - ";
                        } ?></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>