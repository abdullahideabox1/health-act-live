<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "bSort": false,
            "bSearch": false
        });
        $("#myTable_length").remove();
        $("#myTable_filter").remove();
    });
</script>

<?php
$page = "Food Order Details";
$title = "Food Order Details";
$userType = "Admin";


?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4>Order ID # <?php echo $model['id']; ?></h4>
    <hr>


    <div class="span4">
        <strong>Customer Name: </strong>
        <?php echo ucwords($model['customer']['username']); ?>
    </div>
    <div class="span4">
        <strong>Customer Mobile:</strong>
        <?php echo $model['customer']['mobile']; ?>
    </div>
    <br/>

    <div class="span4">
        <strong>Delivery Address:</strong>
        <?php echo ucfirst($model['shipping_address']); ?>
    </div>
    <div class="span4">
        <strong>Customer Comments:</strong>
        <?php echo ucfirst($model['comments']); ?>
    </div>


    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Price</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model['order_detail'] as $m) { ?>
                <tr>
                    <td><?php echo ucwords($m['item']['name']); ?></td>
                    <td><?php echo ucwords($m['qty']); ?></td>
                    <td><?php echo ucwords($m['price']); ?></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#deliveryAccordian").show();
    });
</script>
