<?php
if ($model['id']) {
    $title = "Update Food Area";
} else {
    $title = "Add Food Area";
}
?>

<style>
    #ui-datepicker-div{
        width: 400px !important;
    }
</style>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'days-days-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'day_added') ?>
            <?php echo $form->textField($model, 'day_added', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Days")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'timing_from') ?>
            <?php $this->widget('ext.timepicker.timepicker', array(
                'model'=>$model,
                'name'=>'timing_from',
                'select'=> 'time',

                'options' => array(
                    'showOn'=>'focus',
                    'timeFormat'=>'hh:mm',
                    //'hourMin'=> (int) $hourMin,
                    //'hourMax'=> (int) $hourMax,
                    'hourGrid'=>2,
                    'minuteGrid'=>10,
                ),

            ));?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'timing_to') ?>
            <?php $this->widget('ext.timepicker.timepicker', array(
                'model'=>$model,
                'name'=>'timing_to',
                'select'=> 'time',

                'options' => array(
                    'showOn'=>'focus',
                    'timeFormat'=>'hh:mm',
                    //'hourMin'=> (int) $hourMin,
                    //'hourMax'=> (int) $hourMax,
                    'hourGrid'=>2,
                    'minuteGrid'=>10,
                ),

            ));?>
        </div>
    </div>


    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'is_active') ?>
            <?php echo $form->checkBox($model, 'is_active', array(
                'class' => 'input-block-level form-control ',
            )); ?>
        </div>
    </div>

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Update Food Area" ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Add Food Area" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#daysAccordian").show();
    });
</script>