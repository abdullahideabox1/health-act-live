<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "pageLength": 500,
            "bSort" : false
        });
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Kitchen";
$title = "Kitchen";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php if (isset($_GET['id']) && $_GET['id']) {
        $value = 'checked';
    } else {
        $value = '';
    } ?>
    <input type="checkbox" name="current_day" <?php echo $value ?> onchange="toggleCheckbox(this)"> Current Day
    <script>
        function toggleCheckbox(element) {

            // element.checked = !element.checked;
            if (element.checked == true) {
                window.location.href = "<?php echo Yii::app()->baseUrl.'/admin/kitchen/1'?>";
            } else {
                window.location.href = "<?php echo Yii::app()->baseUrl.'/admin/kitchen/'?>";
            }
        }
    </script>

   <?php /* Date: <strong style="text-decoration: underline"><?php echo date('d F Y') ?></strong>*/ ?>

    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Time</th>
                <th>Item</th>
                <th>Status</th>
                <th>Print</th>

            </tr>
            </thead>
            <tbody>
            <?php //
            foreach ($model as $m) { ?>
                <tr>

                    <td><?php echo ucwords($m['id']); ?></td>
                    <td><?php echo $this->time_elapsed_string(strtotime($m['order_time'])); ?></td>
                    <td><?php if ($m['order_detail']) {
                            foreach ($m['order_detail'] as $item) {
                                $cartitem = json_decode($item['json_data']);

                                    echo '<b style="text-decoration: underline">'.$cartitem->name . ' ( x' . $cartitem->qty . ' ) ' . '</b><br/>';

                                    if (isset($cartitem->radio) && $cartitem->radio) {
                                        foreach ($cartitem->radio as $variety => $value) {
                                            $option = SubitemOption::model()->findByPk($value); ?>
                                            <!--<b ><?php /*echo str_replace('_', " ", $option['option']['heading']); */?>:</b><br/>-->
                                            <span><?php echo $option['option_name'] ?></span><br/>

                                        <?php }
                                    }

                                    if (isset($cartitem->checkbox) && $cartitem->checkbox) {
                                        $option_name = "";
                                        foreach ($cartitem->checkbox as $variety => $value) {
                                            $option = SubitemOption::model()->findByPk($value);
                                            if ($option_name != $option['option']['heading']) { ?>
                                                <!--<b><?php /*echo $option_name = str_replace('_', " ", $option['option']['heading']); */?>
                                                    :</b><br/>-->
                                            <?php } ?>
                                            <span><?php echo $option['option_name'] ?></span><br/>

                                        <?php }
                                    } ?> <hr/><?php
                            }
                        } ?></td>

                    <td><a href="<?php echo Yii::app()->baseUrl . '/admin/orderprepared/' . $m['id']; ?>"
                           class="btn btn-mini btn-success"><?php echo 'Prepared';?></a></td>

                    <td>
                        <div>
                            <a onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"
                               href="<?php echo Yii::app()->baseUrl . '/admin/printreciept/' . $m['id']; ?>"
                               class="btn btn-mini btn-success">Reciept</a>
                        </div>
                    </td>

                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#kitchenAccordian").show();
    });
</script>
