<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "bSort":false
        });
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Food Delivery";
$title = "Food Delivery";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>
    <?php if (isset($_GET['id']) && $_GET['id']) {
        $value = 'checked';
    } else {
        $value = '';
    } ?>
    <input type="checkbox" name="current_day" <?php echo $value ?> onchange="toggleCheckbox(this)"> Current Day
    <script>
        function toggleCheckbox(element) {

            // element.checked = !element.checked;
            if (element.checked == true) {
                window.location.href = "<?php echo Yii::app()->baseUrl.'/admin/pending/1'?>";
            } else {
                window.location.href = "<?php echo Yii::app()->baseUrl.'/admin/pending/'?>";
            }
            ;
        }
    </script>


    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Customer Name</th>
                <th>Mobile#</th>
                <th>Date</th>
                <th>Time</th>
                <th>Total Amount</th>
                <th>Discount Amount</th>
                <th>Type</th>
                <th>Status</th>
                <th>Print</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>

                    <td><a target="_blank"
                           href="<?php echo Yii::app()->baseUrl . '/admin/deliverydetail/' . $m['id']; ?>"><?php echo ucwords($m['id']); ?></a></a>
                    </td>
                    <td><?php echo ucwords($m['customer']['username']); ?></td>
                    <td><?php echo ucwords($m['customer']['mobile']); ?></td>
                    <td><?php echo date('d F Y', strtotime($m['order_date'])); ?></td>
                    <td><?php echo date('h:iA', strtotime($m['order_time'])); ?></td>
                    <?php $total_price = $m['total_price']- $m['discounted_amount']?>
                    <td><?php echo number_format(($total_price) + round(($total_price * 13) / 100) + $m['delivery_charges'], 2); ?></td>
                    <td><?php echo $m['discounted_amount']; ?></td>
                    <td><?php if ($m['is_delivery'] == 1) {
                            echo "D";
                        } else {
                            echo "C";
                        }; ?></td>
                    <td>Pending</td>
                    <td>
                        <div>
                            <a onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"
                               href="<?php echo Yii::app()->baseUrl . '/admin/printreciept/' . $m['id']; ?>"
                               class="btn btn-mini btn-success">Reciept</a>
                        </div>
                    </td>
                    <td><a onclick="return confirm('Are you sure you want to cancel this order?');"
                           href="<?php echo Yii::app()->baseUrl . '/admin/ordercancel/' . $m['id']; ?>"
                           class="btn btn-mini btn-danger">Cancel</a></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#deliveryAccordian").show();
    });
</script>
