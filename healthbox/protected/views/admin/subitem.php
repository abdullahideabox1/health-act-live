<?php
if ($model['id']) {
    $title = "Update Food Sub - Selection";
} else {
    $title = "Add Food Sub - Selection";
}
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subitem-subitem-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'item_id') ?>
            <?php echo $form->dropDownList($model, 'item_id', CHtml::listData(Item::model()->findAll('is_active = :is_active', array('is_active' => 1)), 'id', 'name'), array('empty' => 'Select Item')) ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'heading') ?>
            <?php echo $form->textField($model, 'heading', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Heading")); ?>
        </div>
    </div>

   <!-- <div class="row">
        <div class="span8">
            <?php /*echo $form->label($model, 'selection_type') */?>
            <?php /*echo $form->dropDownList($model, 'selection_type', array('1' => 'Radio', '2' => "Checkbox")) */?>
        </div>
    </div>-->

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit"
                        class="btn btn-success btn-small"><?php echo "Update Food Sub - Selection" ?></button>
            <?php } else { ?>
                <button type="submit"
                        class="btn btn-success btn-small"><?php echo "Add Food Sub - Selection" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>