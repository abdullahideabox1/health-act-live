<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php
$page = "Feedback Listings";
$title = "Feedback Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>


    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Sr.no</th>
                <th>User Id</th>
                <th>User Ip</th>
                <th>Action</th>
                <th>Date</th>
                <th>File Upload</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($model as $m) { ?>
                <tr>
                    <td><?php echo ++$i ?></td>
                    <td><?php echo $m['user_id']; ?></td>
                    <td><?php echo $m['user_ip']; ?></td>
                    <td><?php echo $m['action']; ?></td>
                    <td><?php echo date('Y-m-d', strtotime($m['date_action'])) ?></td>
                    <td><?php echo $m['file_uplaod']; ?></td>

                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#feedbackAccordian").show();
    });
</script>
