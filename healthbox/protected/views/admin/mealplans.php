<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <td>DATE</td>
        <td>BREAKFAST</td>
        <td>LUNCH</td>
        <td>DINNER</td>
        <td>SNACKS</td>
        <td>JUICES</td>
    </tr>
    </thead>
    <tbody>

    <?php

    foreach ($output as $date=>$data) {
    ?>

    <tr>

        <td width="100px"><?= $date; ?></td>
        <td><?php
            if (isset($data['BREAKFAST'])) {
                foreach ($data['BREAKFAST'] as $k) {
                    echo '<p><b>';
                    echo $k['product_name'] . " ";
                    echo '</b>';
                    echo $k['product_price'];
                    echo '</p>';
                    echo "<br>";
                }
            }else{
                echo "--";
            }
             ?></td>
        <td><?php
            if (isset($data['LUNCH'])) {
                foreach ($data['LUNCH'] as $k) {
                    echo '<p><b>';
                    echo $k['product_name'] . " ";
                    echo '</b>';
                    echo $k['product_price'];
                    echo '</p>';
                    echo "<br>";
                }
            }else{
                echo "--";
            }
            ?></td>
        <td><?php
            if (isset($data['DINNER'])) {
                foreach ($data['DINNER'] as $k) {
                    echo '<p><b>';
                    echo $k['product_name'] . " ";
                    echo '</b>';
                    echo $k['product_price'];
                    echo '</p>';
                    echo "<br>";
                }
            }else{
                echo "--";
            }
            ?></td>
        <td><?php
            if (isset($data['SNACKS'])) {
                foreach ($data['SNACKS'] as $k) {
                    echo '<p><b>';
                    echo $k['product_name'] . " ";
                    echo '</b>';
                    echo $k['product_price'];
                    echo '</p>';
                    echo "<br>";
                }
            }else{
                echo "--";
            }
            ?></td>
        <td><?php
            if (isset($data['JUICES'])) {
                foreach ($data['JUICES'] as $k) {
                    echo '<p><b>';
                    echo $k['product_name'] . " ";
                    echo '</b>';
                    echo $k['product_price'];
                    echo '</p>';
                    echo "<br>";
                }
            }else{
                echo "--";
            }
            ?></td>
    </tr>

    <?php
    }
    ?>
    </tbody>
</table>

