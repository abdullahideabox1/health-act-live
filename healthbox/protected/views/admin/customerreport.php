<?php

$criteria = new CDbCriteria;
$criteria->order = "id DESC";
$model = Customer::model()->findAll($criteria);

?>

<h2 style="text-align: center">CUSTOMER REPORT
    <h2>
        <table border="1">
            <tr>
                <th>SR.No</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Address</th>
                <th>Registered User</th>
            </tr>

            <tbody>
            <?php

            $i = 0;
            foreach ($model as $m) { ?>
                <tr>
                    <td style="text-align: center"><?php echo ++$i; ?></td>
                    <td style="text-align: center"><?php echo ucwords($m['username']); ?></td>
                    <td style="text-align: center"><?php echo $m['mobile']; ?></td>
                    <td style="text-align: center"><?php echo $m['email']; ?></td>
                    <td style="text-align: center"><?php echo ucwords($m['address']); ?></td>
                    <td style="text-align: center"><?php echo ($m['is_registered']) ? "Yes" : "No" ; ?></td>
                </tr>

            <?php } ?>
            </tbody>
        </table>