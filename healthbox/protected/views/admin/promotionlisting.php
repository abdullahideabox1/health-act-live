<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Food Promotion Listing";
$title = "Food Promotion Listing";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>
    <a href="<?php echo Yii::app()->baseUrl . '/admin/promotion' ?>" style="margin-bottom: 10px"
       class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add Promotion</a>

    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Promotion Code</th>
                <th>Date From</th>
                <th>Date Start</th>
                <th>Discounted Value</th>
                <th>Status</th>
                <th>Action</th>
                <th></th>


            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>
                    <td><?php echo $m['promotion_code']; ?></td>
                    <td><?php echo date('d F Y' , strtotime($m['date_from'])); ?></td>
                    <td><?php echo date('d F Y' , strtotime($m['date_to'])); ?></td>
                    <td><?php echo $m['discounted_value']; ?></td>
                    <?php if ($m['is_active'] == 1) {
                        $status = "Active";
                    } else {
                        $status = "Inactive";
                    } ?>
                    <td><?php echo $status ?></td>



                    <td><a href="<?php echo Yii::app()->baseUrl . '/admin/promotion/' . $m['id']; ?>"
                           class="btn btn-mini btn-warning">Edit</a></td>

                    <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                           href="<?php echo Yii::app()->baseUrl . '/admin/removepromotion/' . $m['id']; ?>"
                           class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>
