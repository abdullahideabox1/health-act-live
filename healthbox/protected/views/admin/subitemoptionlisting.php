<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Food Sub - Selection Option Listings";
$title = "Food Sub - Selection Option Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>
    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Heading</th>
                <th>Item Name</th>
                <th>Category Name</th>
                <th>Status</th>
                <th>Option</th>
                <th>Action</th>
                <th></th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>
                    <td><?php echo ucwords($m['option']['heading']); ?></td>
                    <td><?php echo ucwords($m['option']['item']['name']); ?></td>
                    <td><?php echo ucwords($m['option']['item']['category']['category_name']); ?></td>
                    <td><?php echo ($m['is_active'] == 1) ? "Active" : "Inactive" ; ?></td>
                    <td><?php echo ucwords($m['option_name']); ?></td>

                    <td><a href="<?php echo Yii::app()->baseUrl . '/admin/subitemoption/id/' . $m['subitem_id'].'/id2/'.$m['id']; ?>"
                           class="btn btn-mini btn-warning">Edit</a></td>

                    <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                           href="<?php echo Yii::app()->baseUrl . '/admin/removesubitemoption/' . $m['id'];?>"
                           class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>
