<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php
$page = "Consultant Queries Listings";
$title = "Consultant Queries Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


    <div class="col-right">
        <h4><?php echo $title; ?></h4>
        <hr>


        <?php if ($model) { ?>

            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Consultant Name</th>
                    <th>Appointment Time</th>
                    <th>Date</th>
                    <th>Comments</th>
                    <th>Date Added</th>



                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($model as $m) { ?>
                    <tr>

                        <td><?php echo ucwords($m['id']); ?></td>
                        <td><?php echo ucwords($m['name']); ?></td>
                        <td><?php echo ucwords($m['phone']); ?></td>
                        <td><?php echo ucwords($m['email']); ?></td>
                        <td><?php echo ucwords($m['consultant_name']); ?></td>
                        <td><?php echo ucwords($m['appointment_time']); ?></td>
                        <td><?php echo ucwords($m['date']); ?></td>
                        <td><?php echo ucwords($m['comments']); ?></td>
                        <td><?php echo ucwords($m['date_added']); ?></td>

                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php
            // $this->widget('CLinkPager', array(
            //      'pages' => $pages,
            //  ));
        } else { ?>
            <h6>You don't have any Records</h6>
        <?php } ?>

    </div>
    <!-- end col right-->



<script>
    $(document).ready(function () {
        $("#PhotoboothAccordian").show();
    });
</script>
