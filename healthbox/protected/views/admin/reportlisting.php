<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "bSort" : false
        } );
         $("#myTable_filter").remove();
        $("#myTable_length").remove();
    });
</script>

<?php
$search = "?";

if (isset($_GET['year']) && $_GET['year']) {
    $year = $_GET['year'];
    $search .= "year=$year";
} else {
    $search .= "year=0";
}

if (isset($_GET['vessel']) && $_GET['vessel']) {
    $vessel_id = $_GET['vessel'];
    $search .= "&vessel=$vessel_id";
} else {
    $search .= "&vessel=0";
}


if (isset($_GET['start_date']) && $_GET['start_date']) {
    $start_date = $_GET['start_date'];
    $search .= "&start_date=$start_date";
} else {
    $search .= "&start_date=0";
}
if (isset($_GET['end_date']) && $_GET['end_date']) {
    $end_date = $_GET['end_date'];
    $search .= "&end_date=$end_date";
} else {
    $search .= "&end_date=0";
}

if (isset($_GET['party']) && $_GET['party']) {
    $party_id = $_GET['party'];
    $search .= "&party=$party_id";
} else {
    $search .= "&party=0";
}
if (isset($_GET['suit']) && $_GET['suit']) {
    $suit = $_GET['suit'];
    $search .= "&suit=$suit";
} else {
    $search .= "&suit=0";
}

?>


<div class="col-right">
    <h4>FOOD ORDERING REPORT</h4>
    <hr>
    <?php if ($model) { ?>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/orderreport/'.$search  ?>"
           style="margin-bottom: 10px;margin-right: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Export Data</a>
    <?php } ?>
    <form method="GET" action="<?php echo Yii::app()->baseUrl . '/admin/reportlisting' ?>">


        <label>Search By Arrival Date:<br/>

            <?php
            if (isset($_GET['start_date']) && $_GET['start_date']) {
                $start_date = date('d-m-Y', strtotime($_GET['start_date']));
            } else {
                $start_date = "";
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'start_date',
                'value' => $start_date,
                'options' => array(
                    'changeYear' => true,           // can change year
                    'changeMonth' => true,          // can change month
                ),
                'htmlOptions' => array(
                    'size' => '10',         // textField size
                    'maxlength' => '10',    // textField maxlength
                    'placeholder' => 'Start Date',
                ),
            )); ?>
            <?php

            if (isset($_GET['end_date']) && $_GET['end_date']) {
                $end_date = date('d-m-Y', strtotime($_GET['end_date']));
            } else {
                $end_date = "";
            }

            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'end_date',
                'value' => $end_date,
                'options' => array(
                    'changeYear' => true,           // can change year
                    'changeMonth' => true,          // can change month
                ),
                'htmlOptions' => array(
                    'size' => '20',         // textField size
                    'maxlength' => '20',    // textField maxlength
                    'placeholder' => 'End Date',
                ),
            )); ?>
        </label>

        <input type="submit" value="Search">
        <input type="button" value="Reset" onclick="reloadPage()">
    </form>

    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Order Id</th>
                <th>Date</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Delivery Type</th>
                <th>Order Status</th>
                <th>Item Price</th>
                <th>Item</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) {
                $cartitem = json_decode($m['json_data']); ?>
                <tr>
                    <td style="text-align: center"><?php echo $m['order_id']; ?></td>
                    <td style="text-align: center"><?php echo date('d-m-Y', strtotime($m['customer_order']['order_date'])); ?></td>
                    <td style="text-align: center"><?php echo ucwords($m['customer_order']['customer']['username']); ?></td>
                    <td style="text-align: center"><?php echo $m['customer_order']['customer']['mobile']; ?></td>
                    <td style="text-align: center"><?php echo ($m['customer_order']['is_delivery']) ? 'Delivery' : 'Pick-Up'; ?></td>

                    <td style="text-align: center">
                        <?php if ($m['customer_order']['status'] == 1) {
                            echo "PENDING";
                        } elseif ($m['customer_order']['status'] == 2) {
                            echo "PREPARED";
                        } elseif ($m['customer_order']['status'] == 3) {
                            echo 'SENT';
                        } elseif ($m['customer_order']['status'] == 4) {
                            echo 'RETURN';
                        }elseif ($m['customer_order']['status'] == 5) {
                            echo 'CANCELLED';
                        }
                        ?>
                    </td>
                    <td style="text-align: center"><?php echo $m['price']; ?></td>
                    <td style="text-align: center"><?php echo $cartitem->name; ?></td>

                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->
