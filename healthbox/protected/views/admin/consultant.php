<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-projects-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>



    <?php echo $form->errorSummary($model); ?>
    <div class="col-right">
        <h4>Add Consultant</h4>
        <hr>
        <div class="row">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name'); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description'); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'description_2'); ?>
            <?php echo $form->textArea($model, 'description_2'); ?>
            <?php echo $form->error($model, 'description_2'); ?>
        </div>

        <div class="row">
            <?php if (is_array($model2)) {
                foreach ($model2 as $item) {
                    ?>
                    <?php echo $form->labelEx($item, 'tab_id'); ?>

                    <?php echo $form->dropDownList($item, 'tab_id', CHtml::listData(Tabs::model()->findAllByAttributes(array(
                        'page_type' => 'consultation'
                    )), 'id', 'title'),
                        array(
                            'prompt' => '---NONE---',
                            'multiple' => 'multiple',
                        )
                    ); ?>
                    <?php echo $form->error($item, 'tab_id'); ?>
                <?php
                }
            }else{ ?>
                <?php echo $form->labelEx($model2, 'tab_id'); ?>

                <?php echo $form->dropDownList($model2, 'tab_id', CHtml::listData(Tabs::model()->findAllByAttributes(array(
                    'page_type' => 'consultation'
                )), 'id', 'title'),
                    array(
                        'multiple' => 'multiple',
                    )
                ); ?>
                <?php echo $form->error($model2, 'tab_id'); ?>
           <?php  } ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'image'); ?>
            <?php echo CHtml::activeFileField($model, 'image'); ?> // by this we can upload image
            <?php echo $form->error($model, 'image'); ?>
        </div>
        <?php echo $this->checkImage($model->image, "Images"); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'sort_order'); ?>
            <?php echo $form->textField($model, 'sort_order'); ?>
            <?php echo $form->error($model, 'sort_order'); ?>
        </div>

        <div class="row">
            <div class="span8">
                <?php echo $form->label($model, 'is_active') ?>(Check the box below to activate this blog)
                <?php echo $form->checkBox($model, 'is_active', array(
                    'class' => 'input-block-level form-control ',
                )); ?>
            </div>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>