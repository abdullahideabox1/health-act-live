<?php
if ($model['id']) {
    $title = "Update Food Area";
} else {
    $title = "Add Food Area";
}
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'areas-area-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'area') ?>
            <?php echo $form->textField($model, 'area', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Area Name")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'min_order') ?>
            <?php echo $form->textField($model, 'min_order', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Minimum Order")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'delivery_fee') ?>
            <?php echo $form->textField($model, 'delivery_fee', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Delivery Fee")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'free_delivery_amount') ?>
            <?php echo $form->textField($model, 'free_delivery_amount', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Free Delivery Amonut")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'delivery_time') ?>
            <?php echo $form->textField($model, 'delivery_time', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Delivery Time")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'is_active') ?>
            <?php echo $form->checkBox($model, 'is_active', array(
                'class' => 'input-block-level form-control ',
            )); ?>
        </div>
    </div>

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Update Food Area" ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Add Food Area" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#areaAccordian").show();
    });
</script>