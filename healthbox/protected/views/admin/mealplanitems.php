<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-projects-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>



    <?php echo $form->errorSummary($model); ?>
    <div class="col-right">
        <h4>Add Consultant</h4>
        <hr>

        <div class="row">
            <?php echo $form->labelEx($model, 'mealplans_id'); ?>

            <?php echo $form->dropDownList($model, 'mealplans_id', CHtml::listData(Mealplans::model()->findAll(), 'id', 'title', 'sub_title'),
                array(
                    'prompt' => '---Select Meal Plan---',
                )
            ); ?>
            <?php echo $form->error($model, 'mealplans_id'); ?>
        </div>
        <br>
        <button id="add_more" type="button" class="float-right btn ">Add More</button>
        <br>
        <br>

        <div class="row">
            <div style="width: 12%; display: inline-block; position: relative;">
                <?php if ($model) {
                    ?>
                    <label for="MealplansItems_days" class="required">Days <span class="required">*</span></label>
                    <select
                        name="MealplansItem[days][]" id="MealplansItems_days">
                        <option value="<?php echo $model['days'] ?>"><?php echo $model['days'] ?></option>
                        <option value="">Please Select</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                        <option value="Sunday">Sunday</option>
                    </select>
                <?php } else { ?>
                    <label for="MealplansItems_days" class="required">Days <span class="required">*</span></label>
                    <select
                        name="MealplansItem[days][]" id="MealplansItems_days">
                        <option value="">Select Day</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                        <option value="Sunday">Sunday</option>
                    </select>
                <?php } ?>

                <?php echo $form->error($model, 'days'); ?>
            </div>
            <div style="width: 12%; display: inline-block;">
                <?php if ($model) {
                    ?>
                    <label for="MealplansItems_item_day_section" class="required">Day Section <span
                            class="required">*</span></label>
                    <select
                        name="MealplansItem[day_section][]" id="MealplansItems_item_day_section">
                        <option value="<?php echo $model['day_section'] ?>"><?php echo $model['day_section'] ?></option>
                        <option value="">Select Day Section</option>
                        <option value="BreakFast">BreakFast</option>
                        <option value="Lunch">Lunch</option>
                        <option value="Dinner">Dinner</option>
                    </select>
                    <?php echo $form->error($model, 'day_section'); ?>
                <?php } else { ?>
                    <label for="MealplansItems_item_day_section" class="required">Day Section <span
                            class="required">*</span></label>
                    <select
                        name="MealplansItem[day_section][]" id="MealplansItems_item_day_section">
                        <option value="">Select Day Section</option>
                        <option value="BreakFast">BreakFast</option>
                        <option value="Lunch">Lunch</option>
                        <option value="Dinner">Dinner</option>
                    </select>
                <?php } ?>
            </div>
            <div style="width: 25%; display: inline-block;">
                <label for="MealplansItems_item_title" class="required">Item Title <span
                        class="required">*</span></label>
                <textarea name="MealplansItem[item_title][]" id="MealplansItems_item_title" type="text" maxlength="255"><?php echo $model['item_title'] ?></textarea>
                <?php echo $form->error($model, 'item_title'); ?>
            </div>
            <div style="width: 30%; display: inline-block;">
                <label for="MealplansItems_item_detail" class="required">Item Detail <span
                        class="required">*</span></label>
                <textarea name="MealplansItem[item_detail][]" id="MealplansItems_item_detail" type="text"
                       maxlength="255"><?php echo $model['item_detail'] ?></textarea>
                <?php echo $form->error($model, 'item_detail'); ?>
            </div>
            <div style="width: 15%; display: inline-block;">
                <label for="MealplansItems_item_detail" class="required">Sort Order <span
                        class="required">*</span></label>
                <input name="MealplansItem[sort_order][]" id="MealplansItems_sort_order" type="text"
                       maxlength="255" value="<?php echo $model['sort_order'] ?>" style="width: 20%;">
                <?php echo $form->error($model, 'sort_order'); ?>
            </div>
            <div id="new_fields"></div>
        </div>


        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>

<script>
    $(document).ready(function () {
        var $counter = 0;
        $('#add_more').on("click", function () {
            var new_fields = $('#new_fields');

            $counter++;

            new_fields.append(
                '           <div style="width: 12%; display: inline-block; position: relative; top: -26px;">  ' +
                '                   <label for="MealplansItems_days" class="required">Days <span class="required">*</span></label> ' +
                '                   <select name="MealplansItem[days][]" id="MealplansItems_days">  ' +
                '                       <option value="">Select Day</option>  ' +
                '                       <option value="Monday">Monday</option>  ' +
                '                       <option value="Tuesday">Tuesday</option>  ' +
                '                       <option value="Wednesday">Wednesday</option>  ' +
                '                       <option value="Thursday">Thursday</option>  ' +
                '                       <option value="Friday">Friday</option>  ' +
                '                       <option value="Saturday">Saturday</option>  ' +
                '                       <option value="Sunday">Sunday</option>  ' +
                '                   </select>  ' +
                '              </div>  '
            );
            new_fields.append(
                '           <div style="width: 12%; display: inline-block; position: relative; top: -26px;">  ' +
                '                   <label for="MealplansItems_days" class="required">Day Section <span class="required">*</span></label> ' +
                '                   <select name="MealplansItem[day_section][]" id="MealplansItems_item_mealplans_items">  ' +
                '                       <option value="">Select Day Section</option>  ' +
                '                       <option value="BreakFast">BreakFast</option>  ' +
                '                       <option value="Lunch">Lunch</option>  ' +
                '                       <option value="Dinner">Dinner</option>  ' +
                '                   </select>  ' +
                '              </div>  '
            );

            new_fields.append(
                '              <div style="width: 25%; display: inline-block;">  ' +
                '                   <label for="MealplansItems_item_title" class="required">Item Title <span  ' +
                '                           class="required">*</span></label>  ' +
                '                   <textarea name="MealplansItem[item_title][]" id="MealplansItems_item_title" type="text" maxlength="255">  ' +
                '                   </textarea>  ' +
                '              </div>  '
            );
            new_fields.append(
                '             <div style="width: 30%; display: inline-block;">  ' +
                '                   <label for="MealplansItems_item_detail" class="required">Item Detail <span  ' +
                '                           class="required">*</span></label>  ' +
                '                   <textarea name="MealplansItem[item_detail][]" id="MealplansItems_item_detail" type="text" maxlength="255">  ' +
                '                   </textarea>  ' +
                '              </div>  '
            );
            new_fields.append(
                '             <div style="width: 15%; display: inline-block;">  ' +
                '                   <label for="MealplansItems_sort_order" class="required">Sort Order <span  ' +
                '                           class="required">*</span></label>  ' +
                '                   <input name="MealplansItem[sort_order][]" id="MealplansItems_sort_order" type="text" maxlength="255" style="width: 20%">  ' +
                '              </div>  '
            );
        });
    });
</script>