
<?php
if ($model['id']) {
	$page = "Update User";
	$title = "Update User";
} else {
	$page = "Add User";
	$title = "Add User";
}
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
	<h4><?php echo $title; ?></h4>
	<hr>

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'user-users-form',
		'enableAjaxValidation'=>false,
		'clientOptions' => array(
			'validateOnSubmit'=>true
		),
	)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="span8">
			<?php echo $form->label($model , 'first_name')?>
			<?php echo $form->textField($model, 'first_name', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "First Name")); ?>
		</div>
	</div>


	<div class="row">
		<div class="span8">
			<?php echo $form->label($model , 'last_name')?>
			<?php echo $form->textField($model, 'last_name', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Last Name")); ?>
		</div>
	</div>

	<div class="row">
		<div class="span8">
			<?php echo $form->label($model , 'email')?>
			<?php echo $form->textField($model, 'email', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Email Address",
				'readonly' => true)); ?>
		</div>
	</div>

	<div class="row">
		<div class="span8">
			<?php echo $form->label($model , 'password')?>
			<?php echo $form->passwordField($model, 'password', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Password",
                //'value' => "",
                )); ?>
		</div>
	</div>

	<div class="row">
		<div class="span8">
            <?php  $model['confirm_password'] = $model['password'] ?>
			<?php echo $form->label($model , 'confirm_password')?>
			<?php echo $form->passwordField($model, 'confirm_password', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Confirm Password")); ?>
		</div>
	</div>

	<div class='row'>
		<div class="span3">

			<?php if ($model['id']) { ?>
				<button type="submit" class="btn btn-success btn-small"><?php echo "Update User" ?></button>
			<?php } else { ?>
				<button type="submit" class="btn btn-success btn-small"><?php echo "Add User"?></button>
			<?php } ?>
		</div>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- end col right-->

