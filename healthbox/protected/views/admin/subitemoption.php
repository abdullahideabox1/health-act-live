<?php
if ($model['id']) {
    $title = "Update Food Sub - Selection";
} else {
    $title = "Add Food Sub - Selection";
}
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subitem-option-subitemoption-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->hiddenField($model, 'subitem_id') ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'option_name') ?>
            <?php echo $form->textField($model, 'option_name', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Option Name")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'item_fats') ?>
            <?php echo $form->textField($model, 'item_fats', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Fats")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'item_protein') ?>
            <?php echo $form->textField($model, 'item_protein', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Proteins")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'item_carbo') ?>
            <?php echo $form->textField($model, 'item_carbo', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Carbohydrates")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'item_calory') ?>
            <?php echo $form->textField($model, 'item_calory', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Calories")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'is_active') ?>
            <?php echo $form->checkBox($model, 'is_active', array(
                'class' => 'input-block-level form-control ',
                'checked' => 'checked'
            )); ?>
        </div>
    </div>


    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit"
                        class="btn btn-success btn-small"><?php echo "Update Food Sub - Selection Option" ?></button>
            <?php } else { ?>
                <button type="submit"
                        class="btn btn-success btn-small"><?php echo "Add Food Sub - Selection Option" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>