<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-projects-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>



    <?php echo $form->errorSummary($model); ?>
    <div class="col-right">
        <h4>Add Consultant</h4>
        <hr>
        <div class="row">
            <?php echo $form->labelEx($model, 'image'); ?>
            <?php echo CHtml::activeFileField($model, 'image'); ?> // by this we can upload image
            <?php echo $form->error($model, 'image'); ?>
        </div>
        <?php echo $this->checkImage($model->image, "MealPlans"); ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'tab_id'); ?>

            <?php echo $form->dropDownList($model, 'tab_id', CHtml::listData(Tabs::model()->findAllByAttributes(array(
                'page_type' => 'Meal Plans'
            )), 'id', 'title'),
                array(
                    'prompt' => '---Select Tab---',
                )
            ); ?>
            <?php echo $form->error($model, 'tab_id'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title'); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'sub_title'); ?>
            <?php echo $form->textField($model, 'sub_title'); ?>
            <?php echo $form->error($model, 'sub_title'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'total_price'); ?>
            <?php echo $form->textField($model, 'total_price'); ?>
            <?php echo $form->error($model, 'total_price'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'free_meal'); ?>
            <?php echo $form->textField($model, 'free_meal'); ?>
            <?php echo $form->error($model, 'free_meal'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'delivery_charges'); ?>
            <?php echo $form->textField($model, 'delivery_charges'); ?>
            <?php echo $form->error($model, 'delivery_charges'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'price_entire_meal'); ?>
            <?php echo $form->textField($model, 'price_entire_meal'); ?>
            <?php echo $form->error($model, 'price_entire_meal'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'meal_gst'); ?>
            <?php echo $form->textField($model, 'meal_gst'); ?>
            <?php echo $form->error($model, 'meal_gst'); ?>
        </div> <div class="row">
            <?php echo $form->labelEx($model, 'without_saturday'); ?>
            <?php echo $form->textField($model, 'without_saturday'); ?>
            <?php echo $form->error($model, 'without_saturday'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'without_saturday_gst'); ?>
            <?php echo $form->textField($model, 'without_saturday_gst'); ?>
            <?php echo $form->error($model, 'without_saturday_gst'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>