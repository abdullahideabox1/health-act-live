<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Food Area Listings";
$title = "Food Area Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>
    <a href="<?php echo Yii::app()->baseUrl . '/admin/area' ?>" style="margin-bottom: 10px"
       class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add Areas</a>

    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Area Name</th>
                <th>Minimum Order</th>
                <th>Delivery Fee</th>
                <th>Delivery Free Amount</th>
                <th>Delivery Time</th>
                <th>Action</th>
                <th></th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>

                    <td><?php echo ucwords($m['area']); ?></td>
                    <td><?php echo ucwords($m['min_order']); ?></td>
                    <td><?php echo ucwords($m['delivery_fee']); ?></td>
                    <td><?php echo ucwords($m['free_delivery_amount']); ?></td>
                    <td><?php echo ucwords($m['delivery_time']); ?></td>

                    <td><a href="<?php echo Yii::app()->baseUrl . '/admin/area/' . $m['id']; ?>"
                           class="btn btn-mini btn-warning">Edit</a></td>

                    <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                           href="<?php echo Yii::app()->baseUrl . '/admin/removearea/' . $m['id']; ?>"
                           class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#areaAccordian").show();
    });
</script>
