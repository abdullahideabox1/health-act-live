<script src="<?php echo Yii::app()->request->baseUrl ?>/js/admin/jquery.js"></script>
<div id="divToPrint" style="display:none;font-size: 10px !important;">

    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style rel="stylesheet">
        #sec1 {
            text-align: center;
            padding-bottom: 10px;
        }

        #sec2 {
            padding-bottom: 10px;
        }

        .leftSide {
            font-weight: bold;
            margin-left: 10px;
        }

        #saleRecpt {
            text-align: center;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            margin-bottom: 10px;
        }

        .borderAll {
            margin-right: 0px;
            margin-left: 0px;
            border: 1px solid black;
        }

        .borderBottom {
            margin-right: 0px;
            margin-left: 0px;
            border-bottom: 1px solid black;
            padding-bottom: 5px;
        }

        .marginLRZero {
            margin-right: 0px;
            margin-left: 0px;
        }

        .tableHead {
            font-size: 13px;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            font-weight: bold;
        }

        .centerBoldLarge {
            text-align: center;
            font-weight: bold;
            font-size: large;
        }

        .withoutBorderBottom {
            margin-left: 0px;
            margin-right: 0px;
            padding-bottom: 10px;
        }
    </style>
    <div>
        <div id="sec1">
            <h2>The Health Act</h2>
            <address>
                25/C ,10TH STREET KH-E-SHAMSHEER, PHASE-5, DHA,
                KARACHI, PAKISTAN <br/>
                PH# 021-35852168-69<br/>
                www.thehealthact.com
            </address>

        </div>
        <div id="saleRecpt"><h4>Order Receipt</h4></div>
        <div>
            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Invoice#</div>
                <div class="col-md-3"><?php echo $m['id'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Date</div>
                <div
                    class="col-md-3"><?php echo date('D , d M Y ', strtotime($m['order_date'])) ?></div>
            </div>

            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Time</div>
                <div
                    class="col-md-3"><?php echo date('h:iA', strtotime($m['order_time'])); ?></div>
            </div>

            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Name</div>
                <div class="col-md-3"><?php echo ucwords($m['customer']['username']) ?></div>
            </div>

            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Mobile</div>
                <div class="col-md-3"><?php echo $m['customer']['mobile'] ?></div>
            </div>

            <div class="row">
                <div class="col-md-3 col-xs-4 leftSide">Address</div>
                <div class="col-md-3"><?php echo $m['shipping_address'] ?></div>
            </div>
        </div>

        <div class="row marginLRZero tableHead">
            <div class="col-sm-2 col-xs-4" style="text-align: left;">Price</div>
            <div class="col-sm-2 col-xs-2" style="text-align: center;">Qty</div>
            <div class="col-sm-1 col-xs-6" style="text-align: right;">Amount</div>
        </div>
        <div class="row centerBoldLarge">Order Details</div>
        <?php if ($m['order_detail']) {
            $total_amount = 0;
            foreach ($m['order_detail'] as $item) {
                $cartitem = json_decode($item['json_data']);
                $total_amount += $cartitem->qty * $cartitem->price ?>


                <div class="row">
                    <div class="col-sm-10 col-xs-10 col-xs-offset-1">
                        <?php echo $cartitem->name ?><br/>
                        <?php if (isset($cartitem->radio) && $cartitem->radio) {
                            foreach ($cartitem->radio as $variety => $value) {
                                $option = SubitemOption::model()->findByPk($value); ?>
                                <!--<b ><?php /*echo str_replace('_', " ", $option['option']['heading']); */ ?>:</b><br/>-->
                                <span>(<?php echo $option['option_name'] ?>)</span><br/>

                            <?php }
                        } ?>
                    </div>

                </div>
                <div class="row borderBottom">
                    <div class="col-sm-2 col-xs-4"
                         style="text-align: left;"><?php echo number_format($cartitem->price) ?></div>
                    <div class="col-sm-2 col-xs-2" style="text-align: center;">
                        x<?php echo $cartitem->qty ?></div>
                    <div class="col-sm-1 col-xs-6"
                         style="text-align: right"><?php echo number_format($cartitem->qty * $cartitem->price) ?></div>
                </div>
            <?php }
        } ?>

        <div class="row withoutBorderBottom" style="font-weight: bold;">
            <div class="col-xs-6" style="font-size: smaller">SubTotal</div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold;margin-bottom: 2px">
                <?php echo number_format($total_amount) ?>
            </div>

            <?php if (number_format($m['discounted_amount'])) { ?>
                <div class="col-xs-6" style="font-size: smaller">Discount</div>
                <div class="col-xs-6"
                     style="text-align: right;border-bottom: 1px solid black;font-weight: bold;margin-bottom: 2px">
                    <?php echo number_format($m['discounted_amount'] , 0);
                    $total_amount -= $m['discounted_amount'];?>
                </div>
            <?php } ?>

            <div class="col-xs-6" style="font-size: smaller">Tax (13%)</div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold;margin-bottom: 2px">
                <?php echo $tax = round(($total_amount * 13) / 100); ?>
            </div>
            <div class="col-xs-6" style="font-size: smaller">Delivery Fee</div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold;margin-bottom: 2px">
                <?php echo ($m['delivery_charges']) ? $m['delivery_charges'] : 0; ?>
            </div>
            <div class="col-xs-6"></div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold"></div>
        </div>

        <div class="row withoutBorderBottom" style="font-weight: bold;">
            <div class="col-xs-6" style="font-size: smaller">Total</div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold;margin-bottom: 2px">
                Rs. <?php echo number_format($total_amount + $tax + $m['delivery_charges'], 0); ?>
            </div>
            <div class="col-xs-6"></div>
            <div class="col-xs-6"
                 style="text-align: right;border-bottom: 1px solid black;font-weight: bold"></div>
        </div>
        <br/><br/>

        <div id="sec2">
            <div>A Body Project Company</div>

            <div>NTN:4330745-7</div>
        </div>

        <div id="sec1">
            <h2>Thank you</h2>
        </div>


        <!-- <div class="row centerBoldLarge">Nutritional Facts</div>
        <?php /*if ($m['order_detail']) {
            $total_amount = 0;
            foreach ($m['order_detail'] as $item) {
                $cartitem = json_decode($item['json_data']);
                $total_amount += $cartitem->qty * $cartitem->price */ ?>


                <div class="row">
                    <div class="col-sm-10 col-xs-10 col-xs-offset-1 ">
                        <b><?php /*echo $cartitem->name */ ?></b></div>
                </div>
                <div class="row borderBottom">
                    <div class="col-sm-10 col-xs-6" style="text-align: left;font-size: 10px">
                        Fats: <?php /*echo (float)$item['item']['fats'] */ ?></div>
                    <div class="col-sm-10 col-xs-6" style="text-align: left;font-size: 10px">
                        Proteins: <?php /*echo (float)$item['item']['protein'] */ ?></div>
                    <div class="col-sm-10 col-xs-6" style="text-align: left;font-size: 10px">
                        Carbo: <?php /*echo (float)$item['item']['carbo'] */ ?></div>
                    <div class="col-sm-10 col-xs-6" style="text-align: left;font-size: 10px">
                        Caleries: <?php /*echo (float)$item['item']['calory'] */ ?></div>
                </div>
            --><?php /*}
        } */ ?>


    </div>
</div>


<script>
    $(document).ready(function () {
        var divToPrint = document.getElementById('divToPrint');
        var popupWin = window.open('', '_self', 'width=500');
        popupWin.document.open();

        popupWin.document.write('<body onload="window.print() ">' + divToPrint.innerHTML);
        popupWin.document.close();
    });
</script>