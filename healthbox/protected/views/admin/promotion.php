
<?php
if ($model['id']) {
	$page = "Update Food Item";
	$title = "Update Food Item";
} else {
	$page = "Add Food Item";
	$title = "Add Food Item";
}
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
	<h4><?php echo $title; ?></h4>
	<hr>

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'promotion-promotion-form',
		'enableAjaxValidation'=>false,
		'clientOptions' => array(
			'validateOnSubmit'=>true
		),
	)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="span8">
			<?php echo $form->labelEx($model , 'promotion_code')?>
			<?php echo $form->textField($model, 'promotion_code', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Promotion Code")); ?>
		</div>
	</div>



	<div class="row">
		<div class="span8">
			<?php echo $form->labelEx($model , 'date_from')?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'date_from',
				'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
				),
			));
			?>
		</div>
	</div>

	<div class="row">
		<div class="span8">
			<?php echo $form->labelEx($model , 'date_to')?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'date_to',
				'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
				),
			));
			?>

		</div>
	</div>

	<div class="row">
		<div class="span8">
			<?php echo $form->labelEx($model , 'discounted_value')?>
			<?php echo $form->textField($model, 'discounted_value', array(
				'class' => 'input-block-level form-control ',
				"placeholder" => "Discounted Price in Percentage")); ?>
		</div>
	</div>

	<div class="row">
		<div class="span8">
			<?php echo $form->label($model, 'is_active') ?>(Check the box below to activate this promotion)
			<?php echo $form->checkBox($model, 'is_active', array(
				'class' => 'input-block-level form-control ',
			)); ?>
		</div>
	</div>

	<div class='row'>
		<div class="span3">

			<?php if ($model['id']) { ?>
				<button type="submit" class="btn btn-success btn-small"><?php echo "Update Promotion" ?></button>
			<?php } else { ?>
				<button type="submit" class="btn btn-success btn-small"><?php echo "Add Promotion"?></button>
			<?php } ?>
		</div>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
	$( document ).ready(function() {
		$("#promotionAccordian").show();
	});
</script>
