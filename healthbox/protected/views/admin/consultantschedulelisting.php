<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php
$page = "Consultant Schedule Listings";
$title = "Consultant Schedule Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


    <div class="col-right">
        <h4><?php echo $title; ?></h4>
        <hr>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/consultantschedule' ?>" style="margin-bottom: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add Consultant Schedule</a>

        <?php if ($model) { ?>

            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Consultant Name</th>
                    <th>Day</th>
                    <th>Time</th>

                    <th>Status</th>
                    <th>Is Table</th>
                    <th>Action</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($model as $m) { ?>
                    <tr>

                        <td><?php echo ucwords($m['consultant']['name']); ?></td>
                        <td><?php echo ucwords($m['days']); ?></td>
                        <td><?php echo ucwords($m['time']); ?></td>

                        <?php if($m['is_active'] == 1){
                            $status  = "Active";
                        }else{
                            $status  = "Inactive";
                        }?>
                        <td><?php echo $status;?></td>
                        <?php if($m['is_table'] == 1){
                            $is_table  = "Yes";
                        }else{
                            $is_table  = "No";
                        }?>
                        <td><?php echo $is_table;?></td>
                        <td><a href="<?php echo Yii::app()->baseUrl . '/admin/consultantschedule/' . $m['id']; ?>"
                               class="btn btn-mini btn-warning">Edit</a></td>

                        <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                               href="<?php echo Yii::app()->baseUrl . '/admin/removeConsultantSchedule/' . $m['id']; ?>"
                               class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php
            // $this->widget('CLinkPager', array(
            //      'pages' => $pages,
            //  ));
        } else { ?>
            <h6>You don't have any Records</h6>
        <?php } ?>

    </div>
    <!-- end col right-->



<script>
    $(document).ready(function () {
        $("#PhotoboothAccordian").show();
    });
</script>
