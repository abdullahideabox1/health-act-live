<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "bSort": false,
        });
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "Meal Plans Listings";
$title = "Meal Plans Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>
    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>User Name</th>
                <th>User Mobile</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th>Created Date</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>

                    <td><?php echo ucwords($m['userDetail']['username']); ?></td>
                    <td><?php echo ucwords($m['userDetail']['mobile']); ?></td>
                    <td><?php echo ucwords($m['total_amount']); ?></td>
                    <td><?php if($m['status'] == 2){
                            echo 'Completed';
                        }//echo ucwords($m['status']); ?></td>
                    <td><?php echo ucwords($m['created']); ?></td>

                    <td><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplans/' . $m['id']; ?>"
                           class="btn btn-mini btn-warning">View Meal Plan</a></td>

                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->


<script>
    $(document).ready(function () {
        $("#mealPlanAccordian").show();
    });
</script>
