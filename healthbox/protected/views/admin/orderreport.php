<?php

$condition = "  ";
$criteria = new CDbCriteria;
if (isset($_GET) && $_GET) {

    if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
        if ($_GET['start_date'] && $_GET['end_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $end_date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
        } elseif ($_GET['start_date']) {
            $date = date('Y-m-d', strtotime($_GET['start_date']));
            $condition .= " AND order_date = '$date'";
        } elseif ($_GET['end_date']) {
            $date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date = '$date'";
        }
    }
}
$criteria->condition = "1 = 1 $condition";
$criteria->order = "order_id DESC";

$model = OrderDetails::model()->with('customer_order')->findAll($criteria);

?>

<h2 style="text-align: center">FOOD ORDERING REPORT
    <h2>

        <table border="1">
            <tr>
                <th>Order Id</th>
                <th>Date</th>
                <th>Time</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Address</th>
                <th>Item Price</th>
                <th>Delivery Type</th>
                <th>Order Status</th>
                <th>Item</th>
                <th>Option</th>
                <th>Comments</th>
            </tr>

            <tbody>
            <?php

            $y = 0;
            foreach ($model as $m) {
                $cartitem = json_decode($m['json_data']); ?>
                <tr>
                    <td style="text-align: center"><?php echo $m['order_id']; ?></td>
                    <td style="text-align: center"><?php echo date('d-m-Y', strtotime($m['customer_order']['order_date'])); ?></td>
                    <td style="text-align: center"><?php echo date('h:iA', strtotime($m['customer_order']['order_time'])); ?></td>
                    <td style="text-align: center"><?php echo ucwords($m['customer_order']['customer']['username']); ?></td>
                    <td style="text-align: center"><?php echo $m['customer_order']['customer']['mobile']; ?></td>
                    <td style="text-align: center"><?php echo $m['customer_order']['customer']['email']; ?></td>
                    <td style="text-align: center"><?php echo ucwords($m['customer_order']['shipping_address']); ?></td>
                    <td style="text-align: center"><?php echo $m['price']; ?></td>
                    <td style="text-align: center"><?php echo ($m['customer_order']['is_delivery']) ? 'Delivery' : 'Pick-Up'; ?></td>

                    <td style="text-align: center">
                        <?php if ($m['customer_order']['status'] == 1) {
                            echo "PENDING";
                        } elseif ($m['customer_order']['status'] == 2) {
                            echo "PREPARED";
                        } elseif ($m['customer_order']['status'] == 3) {
                            echo 'SENT';
                        } elseif ($m['customer_order']['status'] == 4) {
                            echo 'RETURN';
                        } elseif ($m['customer_order']['status'] == 5) {
                            echo 'CANCELLED';
                        }
                        ?>
                    </td>

                    <td style="text-align: center"><?php echo $cartitem->name; ?></td>
                    <?php if (isset($cartitem->radio) && $cartitem->radio) {
                        foreach ($cartitem->radio as $variety => $value) {
                            $option = SubitemOption::model()->findByPk($value); ?>
                            <td style="text-align: center"><?php echo $option['option_name'] ?></td>
                        <?php }
                    }else{ ?>
                        <td style="text-align: center"> - </td>
                    <?php } ?>
                    <td style="text-align: center"><?php echo $m['customer_order']['comments']; ?></td>
                </tr>

            <?php } ?>
            </tbody>
        </table>