<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-projects-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>



    <?php echo $form->errorSummary($model); ?>
    <div class="col-right">
        <h4>Add Albums Images</h4>
        <hr>



        <div class="row">
            <?php echo $form->labelEx($model, 'consultant_id'); ?>
            <?php echo $form->dropDownList($model, 'consultant_id', CHtml::listData(Consultant::model()->findAll(), 'id', 'name'),
                array('prompt' => 'Select Consultant')
            ); ?>
            <?php echo $form->error($model, 'consultant_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'days'); ?>
            <?php echo $form->dropDownList($model, 'days',
                array('Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday',
                    'Friday' => 'Friday', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday'
                ),
                array('prompt' => 'Select Page')
            ); ?>
            <?php echo $form->error($model, 'days'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'time'); ?>
            <?php echo $form->textField($model, 'time'); ?>
            <?php echo $form->error($model, 'time'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'sort_order'); ?>
            <?php echo $form->textField($model, 'sort_order'); ?>
            <?php echo $form->error($model, 'sort_order'); ?>
        </div>

        <div class="row">
            <div class="span8">
                <?php echo $form->label($model, 'is_active') ?>
                <?php echo $form->checkBox($model, 'is_active', array(
                    'class' => 'input-block-level form-control ',
                )); ?>
            </div>
        </div>
        <div class="row">
            <div class="span8">
                <?php echo $form->label($model, 'is_table') ?>(This is for table view)
                <?php echo $form->checkBox($model, 'is_table', array(
                    'class' => 'input-block-level form-control ',
                )); ?>
            </div>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>