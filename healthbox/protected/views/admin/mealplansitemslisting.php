<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php
$page = "Meal Plans Items Listings";
$title = "Meal Plans Items Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


    <div class="col-right">
        <h4><?php echo $title; ?></h4>
        <hr>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/mealplanitems' ?>" style="margin-bottom: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add Meal Plans Items</a>

        <?php if ($model) { ?>

            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Meal Plans Name</th>
                    <th>Meal Plans Days</th>
                    <th>Meal Plans Item Title</th>
                    <th>Meal Plans Item Detail</th>
                    <th>Sort Order</th>
                    <th>Action</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($model as $m) {

                    ?>
                    <tr>
                        <td><?php echo ucwords($m['mealplans']['title'] . '-' .$m['mealplans']['sub_title']); ?></td>
                        <td><?php echo ucwords($m['days']); ?></td>
                        <td><?php echo ucwords($m['item_title']); ?></td>
                        <td><?php echo ucwords($m['item_detail']); ?></td>
                        <td><?php echo ucwords($m['sort_order']); ?></td>

                        <td><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplanitems/' . $m['id']; ?>"
                               class="btn btn-mini btn-warning">Edit</a></td>

                        <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                               href="<?php echo Yii::app()->baseUrl . '/admin/removeMealPlanItems/' . $m['id']; ?>"
                               class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php
            // $this->widget('CLinkPager', array(
            //      'pages' => $pages,
            //  ));
        } else { ?>
            <h6>You don't have any Records</h6>
        <?php } ?>

    </div>
    <!-- end col right-->



<script>
    $(document).ready(function () {
        $("#PhotoboothAccordian").show();
    });
</script>
