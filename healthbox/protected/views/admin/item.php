<?php
if ($model['id']) {
    $page = "Update Food Item";
    $title = "Update Food Item";
} else {
    $page = "Add Food Item";
    $title = "Add Food Item";
}
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'item-item-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>


    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'name') ?>
            <?php echo $form->textField($model, 'name', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Name")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'name') ?>
            <?php echo $form->dropDownList($model, 'category_id', CHtml::listData(Category::model()->findAll(array('order' => 'category_name ASC')), 'id', 'category_name'), array('empty' => 'Select Category')) ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'fats') ?>
            <?php echo $form->textField($model, 'fats', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Fats")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'protein') ?>
            <?php echo $form->textField($model, 'protein', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Proteins")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'carbo') ?>
            <?php echo $form->textField($model, 'carbo', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Carbohydrates")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'calory') ?>
            <?php echo $form->textField($model, 'calory', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Calories")); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'price') ?>
            <?php echo $form->textField($model, 'price', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Price")); ?>
        </div>
    </div>

    <script type="text/javascript"
            src="http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript"
            src="http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>


    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'description') ?>
            <?php $this->widget('application.extensions.tinymce.ETinyMce',
                array(
                    'model' => $model,
                    'attribute' => 'description',
                    'editorTemplate' => 'simple',
                    'options' => array(
                        'theme' => 'advanced',
                    ),
                    'htmlOptions' => array('rows' => 6, 'cols' => 50, 'class' => 'tinymce'),
                )); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'item_image') ?>
            <?php echo $form->fileField($model, 'item_image', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Item Image")); ?>
        </div>
    </div>

    <?php echo $this->checkImage($model->item_image , "ItemImages");?>

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Update Food Item" ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Add Food Item" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>