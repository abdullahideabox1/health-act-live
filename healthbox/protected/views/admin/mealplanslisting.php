<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php
$page = "Meal Plans Listings";
$title = "Meal Plans Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


    <div class="col-right">
        <h4><?php echo $title; ?></h4>
        <hr>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/mealplan' ?>" style="margin-bottom: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add Meal Plans</a>

        <?php if ($model) { ?>

            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Meal Plans Title</th>
                    <th>Meal Plans Sub Title</th>
                    <th>Total Price</th>
                    <th>Action</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($model as $m) { ?>
                    <tr>

                        <td><img src="/upload/Mealplans/<?php echo $m['image']; ?>" width="20%"/> </td>
                        <td><?php echo ucwords($m['title']); ?></td>
                        <td><?php echo ucwords($m['sub_title']); ?></td>
                        <td><?php echo ucwords($m['total_price']); ?></td>

                        <td><a href="<?php echo Yii::app()->baseUrl . '/admin/mealplan/' . $m['id']; ?>"
                               class="btn btn-mini btn-warning">Edit</a></td>

                        <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                               href="<?php echo Yii::app()->baseUrl . '/admin/removeMealPlans/' . $m['id']; ?>"
                               class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php
            // $this->widget('CLinkPager', array(
            //      'pages' => $pages,
            //  ));
        } else { ?>
            <h6>You don't have any Records</h6>
        <?php } ?>

    </div>
    <!-- end col right-->



<script>
    $(document).ready(function () {
        $("#PhotoboothAccordian").show();
    });
</script>
