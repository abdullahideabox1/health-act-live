<?php
if ($model['id']) {
    $title = "Update Food Days Timing";
} else {
    $title = "Add Food Days Timing";
}
?>

<style>
    #ui-datepicker-div {
        width: 800px !important;
    }
</style>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">
    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'days-timing-daystiming-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'day_id') ?>
            <?php echo $form->dropDownList($model, 'day_id', CHtml::listData(Weekdays::model()->findAll(array('order' => 'id ASC')), 'id', 'day_added'), array('empty' => 'Select Day')) ?>
        </div>
    </div>


    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'timing_from') ?>
            <?php $this->widget('ext.timepicker.timepicker', array(
                'model' => $model,
                'name' => 'timing_from',
                'select' => 'time',

                'options' => array(
                    'showOn' => 'focus',
                    'timeFormat' => 'hh:mm',
                    'hourGrid' => 2,
                    'minuteGrid' => 5,
                    'minuteMin' => 00,
                    'minuteMax' => 60,
                    'hourMin' => 0,
                    'hourMax' => 24,
                ),

            )); ?>
        </div>
    </div>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'timing_to') ?>
            <?php $this->widget('ext.timepicker.timepicker', array(
                'model' => $model,
                'name' => 'timing_to',
                'select' => 'time',

                'options' => array(
                    'showOn' => 'focus',
                    'timeFormat' => 'hh:mm',
                    'hourGrid' => 2,
                    'minuteGrid' => 5,
                    'minuteMin' => 00,
                    'minuteMax' => 60,
                    'hourMin' => 0,
                    'hourMax' => 24,
                ),

            )); ?>
        </div>
    </div>


    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'is_active') ?>
            <?php echo $form->checkBox($model, 'is_active', array(
                'class' => 'input-block-level form-control ',
            )); ?>
        </div>
    </div>

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Update Food Timing" ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Add Food Timing" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#daysAccordian").show();
    });
</script>


