<?php

$condition = "  ";
$criteria = new CDbCriteria;
if (isset($_GET) && $_GET) {

    if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
        if ($_GET['start_date'] && $_GET['end_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $end_date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
        } elseif ($_GET['start_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= CURDATE()";
        } elseif ($_GET['end_date']) {
            $start_date = date('Y-m-d', strtotime($_GET['start_date']));
            $end_date = date('Y-m-d', strtotime($_GET['end_date']));
            $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
        }
    }
}
$criteria->condition = "1 = 1 $condition";
$criteria->order = "order_date ASC";

$model = CustomerOrder::model()->findAll($criteria);

?>

<h2 style="text-align: center">SALES REPORT
    <h2>

        <table border="1">
            <tr>
                <th>Order Id</th>
                <th>Date</th>
                <th>Time</th>
                <th>Bill Amount</th>
                <th>GST</th>
                <th>Delivery Charges</th>
                <th>Order Details</th>
                <th>Item Count</th>
                <th>Comments</th>
                <th>If Returned</th>
            </tr>

            <tbody>
            <?php
            foreach ($model as $m) { ?>
                <tr>
                    <td style="text-align: center"><?php echo $m['id']; ?></td>
                    <td style="text-align: center"><?php echo date('d-m-Y', strtotime($m['order_date'])); ?></td>
                    <td><?php echo date('h:iA', strtotime($m['order_time'])); ?></td>
                    <td style="text-align: center"><?php echo $m['total_price']; ?></td>
                    <td style="text-align: center"><?php echo round(($m['total_price'] * 13) / 100); ?></td>
                    <td style="text-align: center"><?php echo $m['delivery_charges']; ?></td>


                    <td style="text-align: center">
                        <?php foreach ($m['order_detail'] as $order_detail) {
                            $cartitem = json_decode($order_detail['json_data']);
                            echo $cartitem->name.',';
                        } ?></td>
                    <td style="text-align: center"><?php echo count($m['order_detail']); ?></td>
                    <td style="text-align: center"><?php echo $m['comments']; ?></td>
                    <td style="text-align: center"><?php if ($m['status'] == 4) {
                            echo "Returned";
                        } elseif($m['status'] == 5){
                            echo "Canceled";
                        }else {
                            " - ";
                        } ?></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>