<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "bSort": false
        });
        $("#myTable_filter").remove();
        $("#myTable_length").remove();
    });
</script>

<?php
$search = "?";


if (isset($_GET['start_date']) && $_GET['start_date']) {
    $start_date = $_GET['start_date'];
    $search .= "&start_date=$start_date";
} else {
    $search .= "&start_date=0";
}
if (isset($_GET['end_date']) && $_GET['end_date']) {
    $end_date = $_GET['end_date'];
    $search .= "&end_date=$end_date";
} else {
    $search .= "&end_date=0";
}
?>


<div class="col-right">
    <h4>KITCHEN REPORT</h4>
    <hr>
    <?php if ($model) { ?>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/kitchenreport/' . $search ?>"
           style="margin-bottom: 10px;margin-right: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Export Data</a>
    <?php } ?>
    <form method="GET" action="<?php echo Yii::app()->baseUrl . '/admin/kitchenreportlisting' ?>">


        <label>Search By Arrival Date:<br/>

            <?php
            if (isset($_GET['start_date']) && $_GET['start_date']) {
                $start_date = date('d-m-Y', strtotime($_GET['start_date']));
            } else {
                $start_date = "";
            }
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'start_date',
                'value' => $start_date,
                'options' => array(
                    'changeYear' => true,           // can change year
                    'changeMonth' => true,          // can change month
                ),
                'htmlOptions' => array(
                    'size' => '10',         // textField size
                    'maxlength' => '10',    // textField maxlength
                    'placeholder' => 'Start Date',
                ),
            )); ?>
            <?php

            if (isset($_GET['end_date']) && $_GET['end_date']) {
                $end_date = date('d-m-Y', strtotime($_GET['end_date']));
            } else {
                $end_date = "";
            }

            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'end_date',
                'value' => $end_date,
                'options' => array(
                    'changeYear' => true,           // can change year
                    'changeMonth' => true,          // can change month
                ),
                'htmlOptions' => array(
                    'size' => '20',         // textField size
                    'maxlength' => '20',    // textField maxlength
                    'placeholder' => 'End Date',
                ),
            )); ?>
        </label>

        <input type="submit" value="Search">
        <input type="button" value="Reset" onclick="reloadPage()">
    </form>

    <?php if ($model) { ?>

        <table id="myTable" class="table table-striped">
            <thead>
            <tr>
                <th>Order Id</th>
                <th>Date</th>
                <!--<th>Count</th>-->
                <th>Recieved Time</th>
                <th>Prepared Time</th>
                <th>Delivered Time</th>
                <th>Return Time</th>
                <th>Order Details</th>
                <th>Item Count</th>
                <th>If Returned</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $m) {
                $i = 1; ?>
                <tr>
                    <td style="text-align: center"><?php echo $m['id']; ?></td>
                    <td style="text-align: center;width: 100px"><?php echo date('d-m-Y', strtotime($m['order_date'])); ?></td>
                    <!--<td style="text-align: center;width: 100px"><?php /*echo count($m['order_status']); */ ?></td>-->

                    <?php if (count($m['order_status']) == 1) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                        <?php } ?>
                        <td> -</td>
                        <td> -</td>
                        <td> -</td>
                    <?php }
                    if (count($m['order_status']) == 2 && $m['status'] == 5) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                        <?php } ?>
                        <td> -</td>
                        <td> -</td>
                        <td> -</td>
                    <?php }
                    if (count($m['order_status']) == 2 && $m['status'] == 2) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                        <?php $i++;} ?>
                        <td> -</td>
                        <td> -</td>
                    <?php }
                    if (count($m['order_status']) == 3) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 3 && $status['status_id'] == 3) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                            <?php $i++;
                        } ?>
                        <td> -</td>
                    <?php } ?>
                    <?php if (count($m['order_status']) == 4) {
                        foreach ($m['order_status'] as $status) { ?>
                            <?php if ($i == 1 && $status['status_id'] == 1) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 2 && $status['status_id'] == 2) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 3 && $status['status_id'] == 3) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } elseif ($i == 4 && $status['status_id'] == 4) { ?>
                                <td><?php echo date('h:iA', strtotime($status['date_added'])); ?></td>
                            <?php } ?>
                            <?php $i++;
                        } ?>
                    <?php } ?>

                    <td style="text-align: center">
                        <?php foreach ($m['order_detail'] as $order_detail) {
                            $cartitem = json_decode($order_detail['json_data']);
                            echo $cartitem->name . ',';
                        } ?></td>
                    <td style="text-align: center"><?php echo count($m['order_detail']); ?></td>
                    <td style="text-align: center"><?php if ($m['status'] == 4) {
                            echo "Returned";
                        } elseif($m['status'] == 5){
                            echo "Canceled";
                        }else {
                            " - ";
                        } ?></td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?php
        // $this->widget('CLinkPager', array(
        //      'pages' => $pages,
        //  ));
    } else { ?>
        <h6>You don't have any Records</h6>
    <?php } ?>

</div>
<!-- end col right-->

<script>
    function reloadPage() {
        window.location.href = '<?php echo Yii::app()->baseUrl?>/admin/saleslisting';
    }
</script>
