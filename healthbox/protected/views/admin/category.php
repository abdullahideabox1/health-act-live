<?php
if ($model['id']) {
    $title = "Update Food Category";
} else {
    $title = "Add Food Category";
}
?>

<!-- =========================Start Col right section ============================= -->

<div class="col-right">

    <h4><?php echo $title; ?></h4>
    <hr>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'category-category-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->labelEx($model, 'category_name') ?>
            <?php echo $form->textField($model, 'category_name', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Category Name")); ?>
        </div>
    </div>


    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'category_image') ?>
            <?php echo $form->fileField($model, 'category_image', array(
                'class' => 'input-block-level form-control ',
                "placeholder" => "Category Image")); ?>
        </div>
    </div>

    <?php echo $this->checkImage($model->category_image , "category");?>


    <div class="row">
        <div class="span8">
            <?php echo $form->label($model, 'is_active') ?>
            <?php echo $form->checkBox($model, 'is_active', array(
                'class' => 'input-block-level form-control ',
            )); ?>
        </div>
    </div>

    <div class='row'>
        <div class="span3">

            <?php if ($model['id']) { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Update Food Category" ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-success btn-small"><?php echo "Add Food Category" ?></button>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- end col right-->


<script>
    $(document).ready(function () {
        $("#menuAccordian").show();
    });
</script>

