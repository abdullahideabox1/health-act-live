<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $("#myTable_length").hide();
    });
</script>

<?php
$page = "User Listings";
$title = "User Listings";
$userType = "Admin";
?>

<!-- =========================Start Col right section ============================= -->


    <div class="col-right">
        <h4><?php echo $title; ?></h4>
        <hr>
        <a href="<?php echo Yii::app()->baseUrl . '/admin/users' ?>" style="margin-bottom: 10px"
           class="btn btn-success btn-small pull-right btn-icon graphic-plus">Add User</a>

        <?php if ($model) { ?>

            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date</th>
                    <th>Active</th>
                    <th>Action</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($model as $m) { ?>
                    <tr>
                        <td><?php echo ucwords($m['first_name'].' '.$m['last_name']); ?></td>
                        <td><?php echo $m['email']; ?></td>
                        <td><?php echo date('d F Y' , strtotime($m['date_added'])) ?></td>
                        <?php if($m['is_active'] == 1){
                            $featured  = "Deactivate";
                            $btn = 'success';
                            $action = "deactivateuser";
                        }else{
                            $featured  = "Activate";
                            $btn = 'warning';
                            $action = "activateuser";
                        }?>
                        <td><a href="<?php echo Yii::app()->baseUrl . '/admin/'.$action.'/' . $m['id']; ?>"
                               class="btn btn-mini btn-<?php echo $btn ?>"><?php echo $featured;?></a></td>

                        <td><a href="<?php echo Yii::app()->baseUrl . '/admin/users/' . $m['id']; ?>"
                               class="btn btn-mini btn-warning">Edit</a></td>

                        <td><a onclick="return confirm('Are you sure you want to delete this item?');"
                               href="<?php echo Yii::app()->baseUrl . '/admin/removeuser/' . $m['id']; ?>"
                               class="btn btn-mini btn-danger"><span class="icon-remove icon-large"></span></a></td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php
            // $this->widget('CLinkPager', array(
            //      'pages' => $pages,
            //  ));
        } else { ?>
            <h6>You don't have any Records</h6>
        <?php } ?>

    </div>
    <!-- end col right-->



<script>
    $(document).ready(function () {
        $("#userAccordian").show();
    });
</script>
