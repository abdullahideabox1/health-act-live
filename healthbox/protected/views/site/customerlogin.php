<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    //'action' => Yii::app()->createUrl('site/customerlogin'),
    'enableClientValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
    ),
)); ?>
<style>
    .loginBtn{
        width: 160px;
        padding: 8px;
        height: 34px;
        margin-top: 10px;
        border-color:#60B49D ;
    }

    input {
        width: 80%;
        box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        -webkit-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        -moz-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        border: 1px solid #CCC;
        background: #FFF;
        margin: 0 0 5px;
        padding: 10px;
        border-radius: 5px;
    }
    #email:-webkit-autofill, input:-webkit-autofill, select:-webkit-autofill {
        -webkit-box-shadow: 0 0 0px 1000px white inset;
    }
    #password:-webkit-autofill, input:-webkit-autofill, select:-webkit-autofill {
        -webkit-box-shadow: 0 0 0px 1000px white inset;
    }
    input[type=submit] {
        padding:5px 15px;
        background:#60B49D;
        border-color: #60B49D;
        border:0 none;
        cursor:pointer;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }
    @media screen  and (max-width: 380px) ,(max-width: 380px) {
        #passwordArea{
            padding-left: 15px;!important;
            padding-right: 15px;!important;
        }
    }
    @media screen  and (min-width: 1000px) {
        #passwordArea{
            padding-left: 0px;

        }
    }

</style>

<div class="row clearfix">

    <div class="col-md-12 col-xs-12">

        <fieldset>

            <h3 class="green" style="font-size: 24px">Existing User? Sign In Here</h3>
            <div class="row">
            <div class="col-md-3 col-xs-12 control-group">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control','id'=>'email')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>

            <div id="passwordArea" class="col-md-3 col-xs-12 control-group">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password', array('class' => ' form-control','id'=>'password')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>

            <div class="col-md-4 col-xs-12 control-group" style="padding-top:25px" >
                <?php echo CHtml::submitButton('Login' , array('class' => ' btn btn-success loginBtn')); ?>
            </div>
            </div>
        </fieldset>

        <?php $this->endWidget(); ?>
    </div>
</div>

<!--<br><div>You can also sign in with facebook.</div><br/>-->

<?php //$this->actionFbTest(); ?>




