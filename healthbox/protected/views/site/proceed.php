<?php
if ($model['id']) {
    $customer = $this->getCustomerDetail($model['id']);
    $model->username = $customer['username'];
    $model->email = $customer['email'];
    $model->mobile = $customer['mobile'];
    $model->address = $customer['address'];
    $model->address2 = $customer['address2'];
    $model->address3 = $customer['address3'];
} else {
    $this->actionCustomerLogin();
}
?>




<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'customer-proceed-form',
    'enableAjaxValidation' => false,
)); ?>

<style>
    h3 {
        font-size: 20px;
    }

    .green {
        color: #60B49D;
    }

    label {
        font-family: 'OpenSans Regular';
        color: #757575;
    }

    input {
        width: 80%;
        box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        -webkit-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        -moz-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
        border: 1px solid #CCC;
        background: #FFF;
        margin: 0 0 5px;
        padding: 10px;
        border-radius: 0;
    }

    #cpayment {

    }

    input[type=submit] {
        padding: 5px 15px;
        background: #60B49D;
        border-color: #60B49D;
        border: 0 none;
        cursor: pointer;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }

    #lins {
        margin: 0;
        padding: 0;
        margin: 30px 0px 10px 0px;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }

    .loginBtn {
        border-color: #60B49D;
        font-size: 16px;
        height: auto;
        margin-top: 0px;
        padding: 10px;
        width: 160px;
    }
</style>
<!--
<div class="row"  style="margin: 30px 0px 10px 0px;">
    <div class="col-md-6 col-xs-5" style="margin-top: 14px; border:1px double #CCC; padding: 0px 0px 0px 0px"></div>
    <div class="col-md-1 col-xs-1" style="width:initial;  padding: 0px 0px 0px 0px"><img src="<?php /*echo Yii::app()->baseUrl*/ ?>/img/circles-hr.png" ></div>
    <div class="col-md-5 col-xs-5" style=" margin-top: 14px; border:1px double #CCC;padding: 0px 0px 0px 0px"></div>

</div>-->
<br>
<br>
<div style="border:1px double #CCC; width:100%; height: 0.1px; float:left;     text-align: center;"><img
        src="<?php echo Yii::app()->baseUrl; ?>/img/circles-hr.png"
        style="margin-top:-30px;"
        alt="The Health Act Logo Circles">
</div>
<br>
<br>
<h3 class="green" style="font-size: 24px">New User? Place Order Here</h3>
<div class="row clearfix">

    <div class="col-md-6  column">

        <?php //$this->pre($model)//echo $form->errorSummary($model); ?>

        <fieldset>
            <h3 class="green">Personal Details</h3>

            <div class="control-group">
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'username'); ?>

            </div>

            <div class="control-group">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>

            <div class="control-group">
                <?php echo $form->labelEx($model, 'mobile'); ?>
                <?php echo $form->textField($model, 'mobile', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'mobile'); ?>
            </div>
        </fieldset>

        <fieldset>
            <?php if (Yii::app()->session['userId']) { ?>

                <div class="control-group">
                    <input style="width: 18px;box-shadow: unset" type="radio" name="Customer[addr_radio]" value="1"
                           checked><?php echo $form->labelEx($model, 'address'); ?>
                    <?php echo $form->textArea($model, 'address', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'address'); ?>
                </div>


                <div class="control-group">
                    <input style="width: 18px;box-shadow: unset" type="radio" name="Customer[addr_radio]"
                           value="2"><?php echo $form->labelEx($model, 'address2'); ?>
                    <?php echo $form->textArea($model, 'address2', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'address2'); ?>
                </div>


                <div class="control-group">
                    <input style="width: 18px;box-shadow: unset" type="radio" name="Customer[addr_radio]"
                           value="3"><?php echo $form->labelEx($model, 'address3'); ?>
                    <?php echo $form->textArea($model, 'address3', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'address3'); ?>
                </div>
            <?php } else { ?>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'address'); ?>
                    <?php echo $form->textArea($model, 'address', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'address'); ?>
                </div>
            <?php } ?>

            <div class="control-group">

                <?php echo $form->labelEx($model, 'comments'); ?>
                <?php echo $form->textArea($model, 'comments', array('class' => 'form-control')); ?>
            </div>

        </fieldset>

    </div>
    <style>
        @media screen  and (max-width: 380px) {
            #orderTable {
                padding-left: 15px;
            !important;
                padding-right: 0px;
            !important;
            }
        }

        @media screen  and (min-width: 1000px) {
            #orderTable {

                padding-right: 0px;
            }
        }

    </style>

    <div id="orderTable" class="col-md-6">
        <fieldset>
            <h3 class="green">Your Order</h3>


            <table style="width: 100%">
                <?php if (isset(Yii::app()->Session['item']) && Yii::app()->Session['item']) {
                    $total_price = 0;
                    $i = 0;
                    foreach (Yii::app()->Session['item'] as $cartitem) {
                        $total_price += $cartitem['qty'] * $cartitem['price']; ?>
                        <tr style="margin-bottom: 100px;border-bottom: 1px solid #ccc;">
                            <td style="padding-bottom: 10px;padding-top: 10px;">

                                (x <?php echo $cartitem['qty'] ?>)
                                <?php echo $cartitem['name'] ?><br/>


                                <?php if (isset($cartitem['radio']) && $cartitem['radio']) {
                                    foreach ($cartitem['radio'] as $variety => $value) {
                                        $option = SubitemOption::model()->findByPk($value); ?>
                                        <!-- <b><?php /*echo str_replace('_', " ", $option['option']['heading']); */ ?>:</b><br/>-->
                                        <span><?php echo $option['option_name'] ?></span><br/>

                                    <?php }
                                } ?>

                                <?php if (isset($cartitem['checkbox']) && $cartitem['checkbox']) {
                                    $option_name = "";
                                    foreach ($cartitem['checkbox'] as $variety => $value) {
                                        $option = SubitemOption::model()->findByPk($value);
                                        if ($option_name != $option['option']['heading']) { ?>
                                            <!-- <b><?php /*echo $option_name = str_replace('_', " ", $option['option']['heading']); */ ?>
                                                :</b><br/>-->
                                        <?php } ?>
                                        <span><?php echo $option['option_name'] ?></span><br/>

                                    <?php }
                                } ?>


                            </td>
                            <td style="padding-right: 20px;padding-bottom: 10px;padding-top: 10px; text-align: right;"
                                valign="top">
                                Rs.<?php echo number_format($cartitem['price'], 0) ?>
                            </td>

                        </tr>

                        <?php $i++;
                    }
                } ?>


                <tr>
                    <td style="padding-top: 5px">&nbsp;</td>
                    <td style="padding-top: 5px">&nbsp;</td>
                    <td style="padding-top: 5px">&nbsp;</td>
                </tr>

                <?php
                if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
                    $total_amount = $_SESSION['total_price'];
                    $display = 'table-row';
                } else {
                    $total_amount = 0;
                    $display = 'none';
                }
                ?>
                <tr>
                    <td style="padding-top: 5px;">SubTotal</td>
                    <td style="padding-top: 5px; padding-right: 20px; text-align: right; ">
                        Rs. <?php echo $total_amount; ?>
                    </td>
                    <td style="padding-top: 5px; ">&nbsp;</td>
                </tr>


                <?php if ($promotion) { ?>
                    <tr>
                    <td style="padding-top: 5px;">Discount (<?php echo $promotion['discounted_value'] ?>%)</td>
                    <td style="padding-top: 5px; padding-right: 20px; text-align: right; ">
                        Rs. <?php echo $discount = round(($total_amount * $promotion['discounted_value']) / 100);
                        $total_amount -= $discount;?>
                    </td>
                    <td style="padding-top: 5px; ">&nbsp;</td>
                    </tr><?php } else {
                    $discount = 0;
                } ?>


                <tr>
                    <td style="padding-top: 5px;">Tax (13%)</td>
                    <td style="padding-top: 5px; padding-right: 20px; text-align: right; ">
                        Rs. <?php echo $tax = round(($total_amount * 13) / 100);
                        $total_amount += $tax;?>
                    </td>
                    <td style="padding-top: 5px; ">&nbsp;</td>
                </tr>



                <tr>
                    <td style="padding-top: 5px;">Delivery Fee</td>
                    <?php if (isset($_SESSION['delivery_fee']) && $_SESSION['delivery_fee']) {
                        if (isset($_SESSION['free_delivery_fee']) && $_SESSION['free_delivery_fee']) {
                            if ($_SESSION['free_delivery_fee'] < $total_amount) {
                                $_SESSION['delivery_fee'] = 0;
                            }
                        }
                        $total_amount += $_SESSION['delivery_fee'] ?>
                        <td style="padding-top: 5px; padding-right: 20px; text-align: right;">
                            Rs. <?php echo $_SESSION['delivery_fee'] ?></td>
                    <?php } else { ?>
                        <td style="padding-top: 5px; padding-right: 20px; text-align: right;">Rs. 0</td>
                    <?php } ?>
                    <td style="padding-top: 5px;">&nbsp;</td>
                </tr>


                <tr>
                    <td style=" font-size: 22px;padding-top: 5px; border-top:1px solid #ccc "><b>Total</b></td>
                    <td style=" font-size: 22px;padding-top: 5px; padding-right: 20px; text-align: right;border-top:1px solid #ccc ">
                        Rs.<b> <?php echo number_format($total_amount, 0); ?></b></td>
                    <td style=" padding-top: 5px;">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3">
                        <!--kaam-->
                        <button id="place_order" onclick="disableButton()" type="submit" name="payment_type"
                                value="cash_delivery" style="margin-bottom: 50px;"
                                class="btn btn-success loginBtn">
                            Place order
                        </button>

                        <a href="<?php echo Yii::app()->baseUrl . '/' ?>" style="margin-bottom: 50px;"
                           class="btn btn-success loginBtn">Go back to order
                        </a>
                    </td>
                </tr>



            </table>


        </fieldset>
    </div>

    <div class="col-md-6">
        <fieldset>
            <h3 class="green">Do you have a discount code</h3>


            <table style="width: 100%">
                <tr>
                    <td>
                        <!--kaam-->
                        <input id="discount_code" name="discount_code" value="" placeholder="Enter Code Here">
                        </input>

                        <a href="javascript:void(0)" onclick="getdiscount()" style="margin-bottom: 100px;"
                           required="true"
                           class="btn btn-success loginBtn">Get Discount
                        </a>
                    </td>
                </tr>

            </table>


        </fieldset>
    </div>

</div>

<?php $this->endWidget(); ?>


<style>
    .errorSummary {
        color: red;
    }

    .error {
        color: red;
    }

    .errorMessage {
        color: red;
    }
</style>


<script>


    $(document).ready(function () {
        $("#customer-proceed-form").submit(function () {
            $("#place_order").attr("disabled", true);
            return true;
        });
    });

    function getdiscount() {
        var discount = $("#discount_code").val();
        var pathname = window.location.pathname; // Returns path only
        //var url      = window.location.href;     // Returns full URL
        if (discount) {
            window.location.href = pathname + '?discount=' + discount
        }

    }

    /* function disableButton(){
     //alert('test');
     $("#place_order").attr('disabled', 'disabled');
     console.log('test');
     }*/
</script>


