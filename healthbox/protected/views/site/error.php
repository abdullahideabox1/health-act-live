<?php

$this->pageTitle=Yii::app()->name . ' - Error';

?>


<div class="top-content just-text">
	<h1 class="skin-font-color4 custom_heading0123"><span class="bold">Sorry the requested page link is currently not available.</span></h1>
	 <p class="skin-font-color4 font-size-16px custom_p0123"><?php echo CHtml::encode($message); ?></p>
</div>