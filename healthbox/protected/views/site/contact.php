<div class="main-content">
	<h2 class="skin-font-color5">Check us on <span class="bold">map</span></h2>
	<iframe class="google-map" src="https://maps.google.pl/maps?source=s_q&amp;f=q&amp;hl=pl&amp;geocode=&amp;q=Pa%C5%82ac+Kultury+i+Nauki,+Plac+Defilad,+Warszawa&amp;aq=0&amp;oq=pa%C5%82a%C4%87+kultury&amp;sll=52.025459,19.204102&amp;sspn=12.453771,33.815918&amp;t=m&amp;ie=UTF8&amp;hq=Pa%C5%82ac+Kultury+i+Nauki,+Plac+Defilad,+Warszawa&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
</div>

<div class="clearfix"></div>

<div class="main-content">
	<h2 class="skin-font-color5">Send <span class="bold">email</span></h2>
	<form method="post" class="contact-form">
		<div class="one-third">
			<input type="text" id="contact_name" class="skin-border-color4 skin-font-color13" value="Name" onblur="if(this.value == '') { this.value='Name'}" onfocus="if (this.value == 'Name') {this.value=''}">
			<input type="text" id="contact_email" class="skin-border-color4 skin-font-color13" value="Email address" onblur="if(this.value == '') { this.value='Email address'}" onfocus="if (this.value == 'Email address') {this.value=''}">
			<input type="text" id="contact_subject" class="skin-border-color4 skin-font-color13" value="Topic" onblur="if(this.value == '') { this.value='Topic'}" onfocus="if (this.value == 'Topic') {this.value=''}">
			<input type="text" class="skin-border-color4 skin-font-color13" id="contact_verify" value="7-2=">

		</div>
		<div class="two-third last">
			<textarea id="contact_text" class="skin-border-color4 skin-font-color13" onblur="if(this.value == '') { this.value='Message'}" onfocus="if (this.value == 'Message') {this.value=''}">Message</textarea>
			<input type="submit" id="contact_button" value="Send email" class="button-normal skin-background-color1 skin-font-color3 skin-background-hover3">
		</div>
	</form>
</div>

<h1>Contact Us</h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>

<script>
	$(document).ready(function () {
		$("#contact").css({color:"#fcc712"});
	});
</script>
