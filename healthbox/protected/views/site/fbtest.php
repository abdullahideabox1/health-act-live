
<div id="mainBody">
    <div id="login_panel">
        <div id="fb-root"></div>
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({appId: '1729621357325697', status: true, cookie: true, xfbml: true});
                /* All the events registered */
                FB.Event.subscribe('auth.login', function(response) {
                    // do something with response
                    login();
                });
                FB.Event.subscribe('auth.logout', function(response) {
                    // do something with response
                    logout();
                });
                FB.getLoginStatus(function(response) {
                    if (response.session) {
                        // logged in and connected user, someone you know
                        login();
                    }

                });
                FB.getLoginStatus(function(response) {
                    login();
                    logout();
                });
            };
            (function() {
                var e = document.createElement('script');
                e.type = 'text/javascript';
                e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
                e.async = true;
                document.getElementById('fb-root').appendChild(e);
            }());

            function login(){
                FB.api('/me?fields=id,name,email',function(response) {
                    console.log(response);

                    if(typeof response.name != 'undefined')
                    {
                        window.location.href = '<?php echo Yii::app()->baseUrl ?>/site/fblogin/id/'+response.id+'/name/'+response.name;
                    }

                });
            }

            function logout(){
                document.getElementById('login').style.display = "none";
            }
        </script>

        <fb:login-button autologoutlink="true" width="500" scope="public_profile,email"></fb:login-button>

        <div id="login">

            <div id="name"></div>
            <div id="email"></div>
            <div id="birthday"></div>
            <div id="username"></div>
            <div id="bio"></div>
        </div>
    </div><!-- end login_panel -->

</div><!-- end mainBody -->


</body>
</html>