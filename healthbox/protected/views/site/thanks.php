<div class="row">
    <div class="col-md-12 col-xs-12"
         style="margin-top: 10px; text-align: left;color: #60B49D; padding: 15px 30px 10px 30px;border-radius: 4px;font-weight: bold">
        <p>Thank you for placing your order. Your order ID is <?php echo $order_id ?>.</p>
    </div>

</div>
<div class="row" style="text-align: center">
    <img width="100%" height="450" src="<?php echo Yii::app()->baseUrl ?>/img/thankyou.jpg">
</div>