<?php //$this->pre(Yii::app()->Session['item']);

?>
<div id="shoppingcart">

    <table style="width: 100%">

        <tbody>
        <?php

        //$this->pre($_SESSION , 1);

        if (isset(Yii::app()->Session['item']) && Yii::app()->Session['item']) {
            $total_price = 0;
            $i = 0;
            foreach (Yii::app()->Session['item'] as $cartitem) {
                $total_price += $cartitem['qty'] * $cartitem['price']; ?>
                <tr style="border-bottom: 1px solid #ccc;" class="mb-10 lh-1-8">
                    <td valign="top" class="cart-td">
                        (x<?php echo $cartitem['qty'] ?>)
                        <?php echo $cartitem['name'] ?>

                        <br/>
                        <?php if (isset($cartitem['radio']) && $cartitem['radio']) {
                            foreach ($cartitem['radio'] as $variety => $value) {
                                $option = SubitemOption::model()->findByPk($value); ?>
                                <!--<b><?php /*echo str_replace('_', " ", $option['option']['heading']); */ ?>:</b><br/>-->
                                <span>(<?php echo $option['option_name'] ?>)</span><br/>

                            <?php }
                        } ?>

                        <?php if (isset($cartitem['checkbox']) && $cartitem['checkbox']) {
                            $option_name = "";
                            foreach ($cartitem['checkbox'] as $variety => $value) {
                                $option = SubitemOption::model()->findByPk($value);
                                if ($option_name != $option['option']['heading']) { ?>
                                    <!-- <b><?php /*echo $option_name = str_replace('_', " ", $option['option']['heading']); */ ?>
                                        :</b><br/>-->
                                <?php } ?>
                                <span>(<?php echo $option['option_name'] ?>)</span><br/>

                            <?php }
                        } ?>


                    </td>
                    <td valign="top" class="">
                        <?php echo number_format($cartitem['price']) ?></td>
                    <td valign="top">
                        <button class="mybtn"
                                onclick="Deleteitem(<?php echo $cartitem['item_id'] ?> , <?php echo $cartitem['item_position'] ?>)">
                            x
                        </button>
                    </td>
                </tr>
                <?php $i++;
            }
        } ?>

        <?php
        if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
            $total_amount = $_SESSION['total_price'];
            $display = 'table-row';
        } else {
            $total_amount = 0;
            $display = 'none';
        }
        ?>

        <tr id="total_price" style="display: <?php echo $display ?>;">
            <td class="total-heading">Total</td>
            <td>
                <span id="subtotal" class="total-price">Rs.<?php echo number_format($total_amount) ?></span></td>
        </tr>


        </tbody>
    </table>

    <?php if (isset(Yii::app()->Session['item']) && Yii::app()->Session['item']) { ?>
        <form id="cartform" method="post" action="<?php echo Yii::app()->baseUrl . '/site/proceed' ?>">
            <input type="hidden" name="SubTotal" id="SubTotal" value="<?php echo $total_amount ?>">
            <input type="hidden" name="min_order" id="min_order"
                   value="<?php echo (isset($_SESSION['min_order']) && $_SESSION['min_order']) ? $_SESSION['min_order'] : 0 ?>">
            <input type="hidden" name="deliveryDistance" id="distance"
                   value="<?php echo (isset($_SESSION['delivery_time']) && $_SESSION['delivery_time']) ? $_SESSION['delivery_time'] : '' ?>"/>
            <input type="hidden" name="deliveryType" value=""/>
            <input type="hidden" name="deliveryPC" value=""/>
            <input type="hidden" name="deliveryTime" value=""/>

            <div id="addtocartbutton">
                <p class="text-centered" style="margin-top: 10px">
                    <button type="button" onclick="CheckOrder();" class="btn btn-success" style="border-radius: 0">
                        Place Order
                    </button>
                </p>
            </div>
            <h5 class="myfonthead" style="font-size: 16px;text-align: center">Try our new sauces! </h5>
            <a href="<?php echo Yii::app()->baseUrl . '/#p5' ?>"><img
                    src="<?php echo Yii::app()->baseUrl . '/images/sauces.png' ?>"></a>
        </form>
    <?php } ?>
</div>
