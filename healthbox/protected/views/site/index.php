<!-- MAIN CONTENT -->

<div class="three-fourth main-content-responsive">

    <!-- news -->
    <?php if (!$model) {
        $i = 1; ?>
        <div class="main-content">
            <a href="<?php echo Yii::app()->baseUrl . '/newslisting' ?>"
               class="main-content-link-absolute skin-font-color6 skin-color-hover1 bold font-weight-normal">view all news</a>

            <h2 class="skin-font-color5">Latest <span class="bold">News</span></h2>


            <?php foreach ($model as $m) { ?>
                <?php if ($i == 3) {
                    $i == 1 ?>
                    <div class="one-third news last">
                        <a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>"><img
                                onError="this.onerror=null;this.src='<?php echo Yii::app()->baseUrl . '/images/default.jpg' ?>';"
                                src="<?php echo Yii::app()->baseUrl . '/upload/news/' . $m['banner'] ?>" alt="news"></a>

                        <div class="img-hover font">
                            <a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>">
                                <span class="skin-font-color3 font-size-72px">+</span>
                            </a>
                        </div>
                        <h4 class="bold"><a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>"
                                            class="skin-font-color5 skin-color-hover1"><?php echo ucwords($m['heading']) ?></a>
                        </h4>
                        <span
                            class="skin-font-color6 bold font-weight-normal"><?php echo date('d F Y', strtotime($m['date_added'])) ?></span>

                        <p>

                            <?php if (strlen($m['description']) > 250) {
                                $pos = strpos($m['description'], ' ', 250);
                                echo substr($m['description'], 0, $pos) ?>...
                            <?php } else {
                                echo $m['description'];
                            };
                            ?>

                        </p>
                    </div>
                <?php } else { ?>
                    <div class="one-third news">
                        <a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>">
                            <img
                                onError="this.onerror=null;this.src='<?php echo Yii::app()->baseUrl . '/images/default.jpg' ?>';"
                                src="<?php echo Yii::app()->baseUrl . '/upload/news/' . $m['banner'] ?>" alt="news"></a>

                        <div class="img-hover font">
                            <a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>">
                                <span class="skin-font-color3 font-size-72px">+</span>
                            </a>
                        </div>
                        <h4 class="bold"><a href="<?php echo Yii::app()->baseUrl . '/news/' . $m['id'] ?>"
                                            class="skin-font-color5 skin-color-hover1"><?php echo ucwords($m['heading']) ?></a>
                        </h4>
                        <span
                            class="skin-font-color6 bold font-weight-normal"><?php echo date('d F Y', strtotime($m['date_added'])) ?></span>

                        <p>

                            <?php if (strlen($m['description']) > 250) {
                                $pos = strpos($m['description'], ' ', 250);
                                echo substr($m['description'], 0, $pos) ?>...
                            <?php } else {
                                echo $m['description'];
                            };
                            ?>

                        </p>
                    </div>
                <?php }
                $i++; ?>


            <?php } ?>
        </div>
    <?php } ?>
    <!-- /news -->


    <div class="clearfix hr-custom"></div>


    <!-- Album -->
    <?php if ($album) {
        $i = 1; ?>


        <div class="main-content custom-marginbottom50px">

                <h2 class="skin-font-color5"><span class="Newfont-weight-normal">New </span><span class="bold">Releases</span></h2>
                <?php foreach ($album as $m) {
                    if ($i == 3) { ?>
                        <div class="one-third album last height_100">
                            <img
                                onError="this.onerror=null;this.src='<?php echo Yii::app()->baseUrl . '/images/default.jpg' ?>';"
                                src="<?php echo Yii::app()->baseUrl . '/upload/album/' . $m['album_image'] ?>" alt="album">


                            <div class="img-hover-album font" style="opacity: 1;">


                                <a href="<?php echo Yii::app()->baseUrl . '/album/' . $m['id'] ?>"></a>


                            </div>


                            <h4 class="bold"><a
                                    href="<?php echo Yii::app()->baseUrl . '/album/' . $m['id'] ?>"
                                    class="skin-font-color5 skin-color-hover1"><?php echo $m['album_name'];?></a>
                            </h4>

                            <span class="skin-font-color6 bold font-weight-normal"><?php echo $m['artistalbum']['artist']['artist_name']; ?></span>

                            <p>
                                <span class="custom_p_34">Release Year:</span> <?php echo $m['release_date']; ?><br/>

                                <span class="custom_p_34">Top Song:</span> <?php echo $m['top_song']['song_name']; ?>
                            </p>

                        </div>

                    <?php } else { ?>
                        <div class="one-third album height_100">
                            <img
                                onError="this.onerror=null;this.src='<?php echo Yii::app()->baseUrl . '/images/default.jpg' ?>';"
                                src="<?php echo Yii::app()->baseUrl . '/upload/album/' . $m['album_image'] ?>" alt="album">


                            <div class="img-hover-album font" style="opacity: 1;">


                                <a href="<?php echo Yii::app()->baseUrl . '/album/' . $m['id'] ?>"></a>


                            </div>


                            <h4 class="bold"><a
                                    href="<?php echo Yii::app()->baseUrl . '/album/' . $m['id'] ?>"
                                    class="skin-font-color5 skin-color-hover1"><?php echo $m['album_name'];?></a>
                            </h4>

                            <span class="skin-font-color6 bold font-weight-normal"><?php echo $m['artistalbum']['artist']['artist_name']; ?></span>

                            <p>

                                <span class="custom_p_34"> Release Year:</span> <?php echo $m['release_date']; ?><br/>

                                <span class="custom_p_34">Top Song:</span> <?php echo $m['top_song']['song_name']; ?></p>


                        </div>
                    <?php }
                    $i++; ?>
                <?php } ?>




        </div>


    <?php } ?>

    <!-- /videos -->

</div>

<!-- /MAIN CONTENT -->

<script>
    $(document).ready(function () {
        $("#home").css({color:"#fcc712"});
    });
</script>