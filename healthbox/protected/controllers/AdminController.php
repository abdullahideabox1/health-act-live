<?php

class AdminController extends Controller
{
    public function filters()
    {
        $this->layout = "adminLayout";
        date_default_timezone_set('Asia/Karachi');
        $this->checkSession();
        if (!Yii::app()->session['userType']) {
            $this->redirect(Yii::app()->baseUrl . '/site/login');
        }

        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';
        $max = CustomerOrder::model()->find($criteria);

        if (Yii::app()->session['max_order_id'] != $max->id) {
            Yii::app()->session['max_order_id'] = $max->id;
        } else {
            Yii::app()->session['repeat_max_order_id'] = Yii::app()->session['max_order_id'];
        }
    }

    public function actionIndex()
    {
        $this->render('index', array());
    }

    public function actionUsers($id = "")
    {
        $this->checkSuperAdmin();

        if ($id) {
            $model = User::model()->findByPk($id);
            if (!$model || $id == 2) {
                $this->page404();
            }
            $pass = $model['password'];
        } else {
            $pass = "";
            $model = new User();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            //$model->is_active = 1;
            $model->user_type = 2; // 1 is admin and 2 is sub admin..
            $model->date_added = date('c');

            if ($model->validate()) {

                if ($_POST['User']['password'] != $pass) {
                    $model->password = md5($_POST['User']['password']);
                } else {

                    $model->password = $pass;
                }
                $model->save(false);
                // $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/userlisting/');
                return;
            }
        }
        $this->render('users', array('model' => $model));
    }

    public function actionUserLogs()
    {
        $this->checkSuperAdmin();
        $model = UserLogs::model()->findAll();
        $this->render("user_logs", array("model" => $model));
    }

    public function actionChangePassword($id)
    {
        $model = User::model()->findByPk($id);
        if (!$model || $id == 2) {
            $this->page404();
        }
        $pass = $model['password'];

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->is_active = 1;
            $model->user_type = 2; // 1 is admin and 2 is sub admin..
            $model->date_added = date('c');

            if ($model->validate()) {

                if ($_POST['User']['password'] != $pass) {
                    $model->password = md5($_POST['User']['password']);
                } else {

                    $model->password = $pass;
                }
                $model->save(false);
                $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/userlisting/');
                return;
            }
        }
        $this->render('changepassword', array('model' => $model));
    }

    public function actionUserListing()
    {
        $this->checkSuperAdmin();
        $model = User::model()->findAll('user_type != :user_type', array(':user_type' => 1));
        $this->render('userlisting', array('model' => $model));
    }

    public function actionDeactivateUser($id)
    {
        $this->checkSuperAdmin();
        $model = User::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/userlisting/');
    }

    public function actionActivateUser($id)
    {
        $model = User::model()->findByPk($id);
        if ($model) {
            $model->is_active = 1;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/userlisting/');
    }

    public function actionRemoveUser($id)
    {
        $model = User::model()->findByPk($id);
        if ($model) {
            $model->delete();
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/userlisting/');
    }

    public function actionCategory($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        if ($id) {
            $model = Category::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
            $image = $model->category_image;
        } else {
            $image = "";
            $model = new Category('add');
        }

        if (isset($_POST['Category'])) {
            $_POST['Category'] = Yii::app()->input->xssClean($_POST['Category']);
            $news_path = Yii::app()->basePath . '/../upload/category';
            if (!is_dir($news_path)) {
                mkdir($news_path, 0777, true);
            }

            $model->attributes = $_POST['Category'];

            $uploadedFile = CUploadedFile::getInstance($model, 'category_image');
            $ext = pathinfo($uploadedFile, PATHINFO_EXTENSION);
            $fileName = $_POST['Category']['category_name'] . '.' . $ext;  // random number + file name
            $model->category_image = $fileName;

            if (empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->category_image = $image;
            }


            if ($model->validate() && $model->save()) {
                if (!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../upload/category/' . $fileName);
                }

                $this->UserLog();

                $this->redirect(Yii::app()->baseUrl . '/admin/categorylisting/');
                return;

            }

        }
        $this->render('category', array('model' => $model));
    }

    public function actionCategoryListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        $model = Category::model()->findAll();
        $this->render('categorylisting', array('model' => $model));
    }

    public function actionRemoveCategory($id)
    {
        $model = Category::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/categorylisting/');
    }

    public function actionItemListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        $model = Item::model()->findAll('is_active = :is_active', array('is_active' => 1));
        $this->render('itemlisting', array('model' => $model));
    }

    public function actionRemoveItem($id)
    {
        $model = Item::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/itemlisting/');
    }

    public function actionItem($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        if ($id) {
            $model = Item::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
            $image1 = $model->item_image;
        } else {
            $image1 = "hello";
            $model = new Item;
        }

        if (isset($_POST['Item'])) {
            $rnd = rand(0, 9999);
            $model->attributes = $_POST['Item'];
            $model->description = $_POST['Item']['description'];
            $model->is_active = 1;

            $project_path = Yii::app()->basePath . '/../upload/ItemImages/';
            if (!is_dir($project_path)) {
                mkdir($project_path, 0777, 1);
            }

            $uploadedFile = CUploadedFile::getInstance($model, 'item_image');

            if ($uploadedFile) {
                $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                $model->item_image = $fileName;
            } else {
                $model->item_image = $image1;
            }
            if ($model->validate()) {

                if (!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../upload/ItemImages/' . $fileName);  // image will uplode to rootDirectory/banner/
                } else {
                    $model->item_image = $image1;
                }
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/itemlisting/');
                return;
            }
        }
        $this->render('item', array('model' => $model));
    }

    public function actionSubItemListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        $model = Subitem::model()->findAll('is_active = :is_active', array('is_active' => 1));
        $this->render('subitemlisting', array('model' => $model));
    }

    public function actionRemoveSubItem($id)
    {
        $model = Subitem::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/subitemlisting/');
    }

    public function actionSubitem($id = '')
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        if ($id) {
            $model = Subitem::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new Subitem;

        }

        if (isset($_POST['Subitem'])) {
            $model->attributes = $_POST['Subitem'];
            $model->selection_type = 1;
            $model->is_active = 1;
            if ($model->validate()) {
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/subitemlisting/');
                return;
            }
        }
        $this->render('subitem', array('model' => $model));
    }

    public function actionSubItemOptionListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }
        $model = SubitemOption::model()->findAll();
        $this->render('subitemoptionlisting', array('model' => $model));
    }

    public function actionRemoveSubItemOption($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }

        $model = SubitemOption::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/subitemoptionlisting/');
    }

    public function actionSubItemOption($id, $id2 = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_menu']) {
            $this->page404();
        }

        if ($id2) {
            $model = SubitemOption::model()->findByPk($id2);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new SubitemOption;
        }
        $model->subitem_id = $id;
        if (isset($_POST['SubitemOption'])) {
            $model->attributes = $_POST['SubitemOption'];
            //$model->is_active = 1;
            if ($model->validate()) {
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/subitemoptionlisting/');
                return;
            }
        }
        $this->render('subitemoption', array('model' => $model));
    }

    public function actionAreaListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_venue']) {
            $this->page404();
        }
        $model = Areas::model()->findAll('is_active = :is_active', array('is_active' => 1));
        $this->render('arealisting', array('model' => $model));
    }

    public function actionRemoveArea($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_venue']) {
            $this->page404();
        }

        $model = Areas::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/arealisting/');
    }

    public function actionArea($id = '')
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_venue']) {
            $this->page404();
        }

        if ($id) {
            $model = Areas::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new Areas;
        }


        if (isset($_POST['Areas'])) {
            $model->attributes = $_POST['Areas'];
            if ($model->validate()) {
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/arealisting/');
                return;
            }
        }
        $this->render('area', array('model' => $model));
    }

    public function actionDaysListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }
        $model = DaysTiming::model()->bystatus()->findAll();
        $this->render('dayslisting', array('model' => $model));
    }

    public function actionRemoveDays($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }

        $model = DaysTiming::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/dayslisting/');
    }

    public function actionDays($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }

        if ($id) {
            $model = Weekdays::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new Days;
        }


        if (isset($_POST['Days'])) {
            $model->attributes = $_POST['Days'];
            if ($model->validate()) {
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/dayslisting/');
                return;
            }
        }
        $this->render('days', array('model' => $model));
    }

    public function actionDaystiming($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }

        if ($id) {
            $model = DaysTiming::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new DaysTiming;
        }


        if (isset($_POST['DaysTiming'])) {
            $model->attributes = $_POST['DaysTiming'];
            if ($model->validate()) {
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl . '/admin/dayslisting/');
                return;
            }
        }
        $this->render('daystiming', array('model' => $model));
    }

    public function actionKitchen($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_kitchen']) {
            $this->page404();
        }

        $condition = "1 = 1 AND ";
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        }

        $model = CustomerOrder::model()->findAll("$condition status = 1 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1));
        $this->render('kitchenlisting', array('model' => $model));
    }

    public function actionReciept($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }

        $condition = "1 = 1 AND ";
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        }

        $model = CustomerOrder::model()->find("$condition  id =:id AND status = 3 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1, ':id' => $id));
        $this->render('kitchenlisting', array('model' => $model));

    }

    public function actionPrintReciept($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }

        $m = CustomerOrder::model()->findByPk($id);
        $this->renderPartial('reciept', array('m' => $m));
    }

    public function actionOrderPrepared($id)
    {

        $userRightights = $this->userRights();
        if (!$userRightights['is_kitchen']) {
            $this->page404();
        }

        $order = CustomerOrder::model()->findByPk($id);
        if ($order) {
            $model = OrderStatus::model()->find("status_id =:status_id AND order_id = :order_id", array(":status_id" => 3, ':order_id' => $id));
            if (!$model) {
                $model = new OrderStatus();
            }
            $model->order_id = $id;
            $model->status_id = 2; // Prepared
            $model->is_active = 1;
            $model->date_added = date('c');
            $model->save(false);
            $order->status = 2;
            $order->save(false);
            $this->redirect(Yii::app()->baseUrl . '/admin/kitchen/');
        }
    }

    public function actionPending($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        } else {
            $condition = "";
        }

        $model = CustomerOrder::model()->findAll("$condition status = 1 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1));

        $this->render('pendinglisting', array('model' => $model));
    }

    public function actionPrepared($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        } else {
            $condition = "";
        }

        $model = CustomerOrder::model()->findAll("$condition status = 2 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1));
        $this->render('preparedlisting', array('model' => $model));
    }

    public function actionSent($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        } else {
            $condition = "";
        }

        $model = CustomerOrder::model()->findAll("$condition status = 3 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1));
        $this->render('sentlisting', array('model' => $model));
    }

    public function actionReturn($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }
        if ($id) {
            $current_date = date("Y-m-d");
            $condition = "order_date = '$current_date' AND";
        } else {
            $condition = "";
        }

        $model = CustomerOrder::model()->findAll("$condition status = 4 AND is_active = :is_active ORDER BY id DESC", array('is_active' => 1));
        $this->render('returnlisting', array('model' => $model));
    }

    public function actionOrderSent($id)
    {

        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }

        $order = CustomerOrder::model()->findByPk($id);
        if ($order) {
            $model = OrderStatus::model()->find("status_id =:status_id AND order_id = :order_id", array(":status_id" => 3, ':order_id' => $id));
            if (!$model) {
                $model = new OrderStatus();
            }
            $model->order_id = $id;
            $model->status_id = 3; // Prepared
            $model->is_active = 1;
            $model->date_added = date('c');
            $model->save(false);
            $order->status = 3;
            $order->save(false);
            $this->redirect(Yii::app()->baseUrl . '/admin/prepared/');
        }
    }

    public function actionOrderReturn($id)
    {

        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }

        $order = CustomerOrder::model()->findByPk($id);
        if ($order) {
            $model = OrderStatus::model()->find("status_id =:status_id AND order_id = :order_id", array(":status_id" => 4, ':order_id' => $id));
            if (!$model) {
                $model = new OrderStatus();
            }
            $model->order_id = $id;
            $model->status_id = 4; // Prepared
            $model->is_active = 1;
            $model->date_added = date('c');
            $model->save(false);
            $order->status = 4;
            $order->save(false);
            $this->redirect(Yii::app()->baseUrl . '/admin/sent/');
        }
    }

    public function actionOrderCancel($id)
    {
        $order = CustomerOrder::model()->findByPk($id);
        if ($order) {
            $model = OrderStatus::model()->find("status_id =:status_id AND order_id = :order_id", array(":status_id" => 5, ':order_id' => $id));
            if (!$model) {
                $model = new OrderStatus();
            }
            $model->order_id = $id;
            $model->status_id = 5; // Prepared
            $model->is_active = 1;
            $model->date_added = date('c');
            $model->save(false);
            $order->status = 5;
            $order->save(false);
            $this->redirect(Yii::app()->baseUrl . '/admin/pending/');
        }
    }


    public function actionDeliveryDetail($id)
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_delivery']) {
            $this->page404();
        }
        $model = CustomerOrder::model()->find('id = :order_id AND is_active = :is_active', array(':order_id' => $id, 'is_active' => 1));

        $this->render('deliverydetail', array('model' => $model));
    }

    public function actionCustomerListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_customer']) {
            $this->page404();
        }
        $model = Customer::model()->findAll('is_active = :is_active', array('is_active' => 1));
        $this->render('customerlisting', array('model' => $model));
    }

    public function UserLog($uploadfile = "")
    {
        // $this->pre($_SERVER ,1 );
        $model = new UserLogs;
        $model->user_id = Yii::app()->session['userId'];
        $model->user_ip = $_SERVER['REMOTE_ADDR'];
        $model->action = $_SERVER['REDIRECT_URL'];
        $model->date_action = date('c');
        $model->file_uplaod = $uploadfile;
        $model->save(false);
    }

    public function actionReportListing()
    {

        $condition = "  ";
        $criteria = new CDbCriteria;
        if (isset($_GET) && $_GET) {

            if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
                if ($_GET['start_date'] && $_GET['end_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
                } elseif ($_GET['start_date']) {
                    $date = date('Y-m-d', strtotime($_GET['start_date']));
                    $condition .= " AND order_date = '$date'";
                } elseif ($_GET['end_date']) {
                    $date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date = '$date'";
                }
            }
        }
        $criteria->condition = "1 = 1 $condition";
        $criteria->order = "order_id DESC";

        $model = OrderDetails::model()->with('customer_order')->findAll($criteria);
        $this->render('reportlisting', array('model' => $model));
    }

    public function actionSalesListing()
    {

        $condition = "  ";
        $criteria = new CDbCriteria;
        if (isset($_GET) && $_GET) {

            if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
                if ($_GET['start_date'] && $_GET['end_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
                } elseif ($_GET['start_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= CURDATE()";
                } elseif ($_GET['end_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
                }
            }
        }
        $criteria->condition = "1 = 1 $condition";
        $criteria->order = "order_date ASC";

        $model = CustomerOrder::model()->findAll($criteria);
        $this->render('saleslisting', array('model' => $model));
    }

    public function actionKitchenReportListing()
    {

        $condition = "  ";
        $criteria = new CDbCriteria;
        if (isset($_GET) && $_GET) {

            if ((isset($_GET['start_date']) && $_GET['start_date']) || (isset($_GET['end_date']) && ($_GET['end_date']))) {
                if ($_GET['start_date'] && $_GET['end_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
                } elseif ($_GET['start_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= CURDATE()";
                } elseif ($_GET['end_date']) {
                    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
                    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
                    $condition .= " AND order_date >= '$start_date' AND order_date <= '$end_date'";
                }
            }
        }
        $criteria->condition = "1 = 1 $condition";
        $criteria->order = "order_date ASC";

        $model = CustomerOrder::model()->findAll($criteria);
        $this->render('kitchenreportlisting', array('model' => $model));
    }

    public function actionOrderReport()
    {
        ob_start();
        include './protected/views/admin/orderreport.php';
        //die();
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Food Order Report.xls");

    }

    public function actionKitchenReport()
    {
        ob_start();
        include './protected/views/admin/kitchenreport.php';
        // die();
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Kitchen Report.xls");

    }

    public function actionSalesReport()
    {
        ob_start();
        include './protected/views/admin/salesreport.php';
        //die();
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Sales Report.xls");

    }

    public function actionCustomerReport()
    {
        ob_start();
        include './protected/views/admin/customerreport.php';
        //die();
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Customer Report.xls");

    }

    public function actionPromotion($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }
        if ($id) {
            $model = Promotion::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new Promotion;
        }

        if(isset($_POST['Promotion']))
        {
            $model->attributes=$_POST['Promotion'];
            if($model->validate())
            {
                $model->date_from = date('Y-m-d' , strtotime($_POST['Promotion']['date_from']));
                $model->date_to = date('Y-m-d' , strtotime($_POST['Promotion']['date_to']));
                $model->save(false);
                $this->redirect(Yii::app()->baseUrl.'/admin/promotionlisting');
                return;
            }
        }
        $this->render('promotion',array('model'=>$model));
    }

    public function actionPromotionListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_schedule']) {
            $this->page404();
        }
        $model = Promotion::model()->findAll();
        $this->render('promotionlisting', array('model' => $model));
    }

    public function actionRemovePromotion($id)
    {
        $model = Promotion::model()->findByPk($id);
        if ($model) {
            $model->is_active = 0;
            $model->save(false);
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/promotionlisting/');
    }
    
    
    //Abdullah
    
    public function actionMealPlanListing()
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_meal_plan']) {
            $this->page404();
        }
        $model = UserMenu::model()->with(array('userDetail'))->findAll('status = :status', array(
            ':status' => 2
        ));
        $this->render('mealplanlisting', array('model' => $model));
    }

    public function actionMealPlans($id = "")
    {
        $userRightights = $this->userRights();
        if (!$userRightights['is_meal_plan']) {
            $this->page404();
        }
        $model = UserMenuDetail::model()->findAllByAttributes(array(
            'user_menu_id' => $id
        ));
        $output = array();
        foreach ($model as $key => $value) {

            $product = array();
            $product['product_name'] = $value['product_name'];
            $product['product_price'] = $value['product_price'];

            $product['product_quantity'] = $value['product_quantity'];
            $product['delivery_time'] = $value['delivery_time'];
            $product['option_value'] = $value['option_value'];
            $product['option_value_id'] = $value['option_value_id'];
            $output[$value['calender_date']][$value['calender_type']][] = $product;
        }

        $this->render('mealplans', array('output' => $output));
    }

    public function actionImages($id = '')
    {
        $this->layout = "adminLayout";
        $model = new Images();
        $this->checkSession();
        $model->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = Images::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
            $image1 = $model->image;
        } else {
            $image1 = "";
            $model = new Images('add');
        }

        if (isset($_POST['Images'])) {
            $rnd = rand(0, 9999);
            $model->attributes = $_POST['Images'];
            //$model->posted_date = date('c');
            $project_path = Yii::app()->basePath . '/../upload/Images/';
            if (!is_dir($project_path)) {
                mkdir($project_path, 0777, 1);
            }

            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            if ($uploadedFile) {
                $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                $model->image = $fileName;
            } else {
                $model->image = $image1;
            }


            if ($model->validate()) {

                if (!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../upload/Images/' . $fileName);  // image will uplode to rootDirectory/banner/
                } else {
                    $model->image = $image1;
                }


                $model->save(false);
                $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/imageslisting/');
            }
        }
        $this->render('images', array('model' => $model));
    }

    public function actionImagesListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Images::model()->findAll();

        $this->render('imageslisting', array('model' => $model));
    }

    public function actionRemoveImages($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Images::model()->findByPk($id);
        if ($model) {
            $model->delete();
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/imageslisting/');
    }

    public function actionTabs($id = '')
    {
        $this->layout = "adminLayout";
        $model = new Tabs();
        $this->checkSession();
        $model->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = Tabs::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new Tabs('add');
        }

        if (isset($_POST['Tabs'])) {
            $model->attributes = $_POST['Tabs'];

            if ($model->validate()) {
                $model->save(false);
                $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/tabslisting/');
            }
        }
        $this->render('tabs', array('model' => $model));
    }

    public function actionTabsListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Tabs::model()->findAll();

        $this->render('tabslisting', array('model' => $model));
    }

    public function actionRemoveTabs($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Tabs::model()->findByPk($id);
        if ($model) {
            $model->delete();
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/tabslisting/');
    }

    public function actionConsultant($id = '')
    {
        $this->layout = "adminLayout";
        $model = new Consultant();
        $model2 = new ConsultantTabs();
        $this->checkSession();
        $model->setScenario('update');
        $model2->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = Consultant::model()->findByPk($id);
            $model2 = ConsultantTabs::model()->findAllByAttributes(array(
                'consultant_id' => $id
            ));

            if (!$model) {
                $this->page404();
            }
            $image1 = $model->image;
        } else {
            $image1 = "";
            $model = new Consultant('add');
            $model2 = new ConsultantTabs('add');
        }


        if (isset($_POST['Consultant'])) {
            $rnd = rand(0, 9999);
            $model->attributes = $_POST['Consultant'];
            $project_path = Yii::app()->basePath . '/../upload/Consultant/';
            if (!is_dir($project_path)) {
                mkdir($project_path, 0777, 1);
            }
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            if ($uploadedFile) {
                $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                $model->image = $fileName;
            } else {
                $model->image = $image1;
            }

            if ($model->validate()) {

                if (!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../upload/Consultant/' . $fileName);  // image will uplode to rootDirectory/banner/
                } else {
                    $model->image = $image1;
                }

                $model->save();
                //   $this->UserLog();
                //  $this->redirect(Yii::app()->baseUrl . '/admin/consultantlisting/');
            }
        }


        if (isset($_POST['ConsultantTabs'])) {
            $tabsPost = $_POST['ConsultantTabs'];
            $arr = array();
            ConsultantTabs::model()->deleteAllByAttributes(array(
                'consultant_id' => $id,
            ));
            foreach ($tabsPost['tab_id'] as $key => $value) {
                $tabs = new ConsultantTabs;
                $tabs->consultant_id = $model['id'];
                $tabs->tab_id = $value;

                if ($tabs->validate()) {
                    $tabs->save();
                }
            }
            $this->UserLog();
            $this->redirect(Yii::app()->baseUrl . '/admin/consultantlisting/');
        }

        $this->render('consultant', array('model' => $model, 'model2' => $model2));
    }

    public function actionConsultantListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Consultant::model()->findAll();

        $this->render('consultantlisting', array('model' => $model));
    }

    public function actionRemoveConsultant($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Consultant::model()->findByPk($id);
        if ($model) {
            $model->delete();
            ConsultantTabs::model()->deleteAllByAttributes(array(
                'consultant_id' => $id,
            ));
            $this->UserLog();
        }

        $this->redirect(Yii::app()->baseUrl . '/admin/consultantlisting/');
    }

    public function actionConsultantQueriesListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = ConsultantQueries::model()->findAll();

        $this->render('consultantquerieslisting', array('model' => $model));
    }

    public function actionConsultantSchedule($id = '')
    {
        $this->layout = "adminLayout";
        $model = new ConsultantSchedule();
        $this->checkSession();
        $model->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = ConsultantSchedule::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }
        } else {
            $model = new ConsultantSchedule('add');
        }

        if (isset($_POST['ConsultantSchedule'])) {
            $model->attributes = $_POST['ConsultantSchedule'];

            if ($model->validate()) {
                $model->save(false);
                $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/consultantschedulelisting/');
            }
        }
        $this->render('consultantschedule', array('model' => $model));
    }

    public function actionConsultantScheduleListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = ConsultantSchedule::model()->with(array('consultant'))->findAll();

        $this->render('consultantschedulelisting', array('model' => $model));
    }

    public function actionRemoveConsultantSchedule($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = ConsultantSchedule::model()->findByPk($id);
        if ($model) {
            $model->delete();
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/consultantschedulelisting/');
    }

    public function actionMealPlan($id = '')
    {
        $this->layout = "adminLayout";
        $model = new Mealplans();
        $this->checkSession();
        $model->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = Mealplans::model()->findByPk($id);

            if (!$model) {
                $this->page404();
            }
            $image1 = $model->image;
        } else {
            $image1 = "";
            $model = new Mealplans('add');
        }


        if (isset($_POST['Mealplans'])) {
            $rnd = rand(0, 9999);
            $model->attributes = $_POST['Mealplans'];
            $model->date_created = date('c');
            $project_path = Yii::app()->basePath . '/../upload/Mealplans/';
            if (!is_dir($project_path)) {
                mkdir($project_path, 0777, 1);
            }
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            if ($uploadedFile) {
                $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                $model->image = $fileName;
            } else {
                $model->image = $image1;
            }

            if ($model->validate()) {

                if (!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../upload/Mealplans/' . $fileName);  // image will uplode to rootDirectory/banner/
                } else {
                    $model->image = $image1;
                }

                $model->save();
                $this->UserLog();
                $this->redirect(Yii::app()->baseUrl . '/admin/mealplanslisting/');
            }
        }

        $this->render('mealplan', array('model' => $model));
    }

    public function actionMealPlansListing()
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Mealplans::model()->findAll();

        $this->render('mealplanslisting', array('model' => $model));
    }

    public function actionRemoveMealPlans($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = Mealplans::model()->findByPk($id);
        if ($model) {
            $model->delete();
            MealplansItems::model()->deleteAllByAttributes(array(
                'mealplans_id' => $id,
            ));
            $this->UserLog();
        }

        $this->redirect(Yii::app()->baseUrl . '/admin/mealplanslisting/');
    }

    public function actionMealPlanItems($id = '')
    {
        $this->layout = "adminLayout";
        $model = new MealplansItems();
        $this->checkSession();
        $model->setScenario('update');
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        if ($id) {
            $model = MealPlansItems::model()->findByPk($id);
            if (!$model) {
                $this->page404();
            }

        } else {
            $model = new MealPlansItems('add');
        }

        if (isset($_POST['MealplansItem'])) {
            $items = $_POST['MealplansItem']["days"];

            $model = MealPlansItems::model()->deleteByPk($id);
            foreach ($items as $key => $item) {
                $model = new MealplansItems();
                $model->mealplans_id = $_POST['MealplansItems']["mealplans_id"];
                $model->days = $item;
                $model->item_title = $_POST['MealplansItem']["item_title"][$key];
                $model->item_detail = $_POST['MealplansItem']["item_detail"][$key];
                $model->day_section = $_POST['MealplansItem']["day_section"][$key];
                $model->sort_order = $_POST['MealplansItem']["sort_order"][$key];


                if ($model->validate()) {
                    $model->save(false);
                    $this->UserLog();

                }
            }


            $this->redirect(Yii::app()->baseUrl . '/admin/mealplansitemslisting/');
        }
        $this->render('mealplanitems', array('model' => $model));
    }

    public function actionMealPlansItemsListing()
    {
        $this->layout = "adminLayout";
        $criteria=new CDbCriteria;
        $criteria->order='sort_order ASC';
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = MealplansItems::model()->with(array('mealplans'))->findAll($criteria   );

        $this->render('mealplansitemslisting', array('model' => $model));
    }

    public function actionRemoveMealPlanItems($id)
    {
        $this->layout = "adminLayout";
        $userRightights = $this->userRights();
        if (!$userRightights['is_frontend']) {
            $this->page404();
        }
        $model = MealplansItems::model()->findByPk($id);
        if ($model) {
            $model->delete();
            $this->UserLog();
        }
        $this->redirect(Yii::app()->baseUrl . '/admin/mealplansitemslisting/');
    }
}