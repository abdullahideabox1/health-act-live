<?php

class SiteController extends Controller
{
    public function actions()
    {

        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters()
    {
        $this->setTime();
    }

    public function actionCategory()
    {

    }

    public function actionIndex()
    {
        $this->render('cart');
    }

    public function actionAjaxOption()
    {
        $option_aray = array();
        if (isset($_POST['option_id']) && $_POST['option_id']) {
            $option = SubitemOption::model()->findByPk($_POST['option_id']);
            $option_aray['fats'] = $option['item_fats'] + $option['option']['item']['fats'];
            $option_aray['protein'] = $option['item_protein'] + $option['option']['item']['protein'];
            $option_aray['carbo'] = $option['item_carbo'] + $option['option']['item']['carbo'];
            $option_aray['calory'] = $option['item_calory'] + $option['option']['item']['calory'];
            echo json_encode($option_aray);
        } else {
            return;
        }
    }


    public function actionCheckFood()
    {
        $this->checkFood();
    }

    public function actionAreasSelection()
    {
        $session = new CHttpSession;               //add this line
        $session->open();
        if (isset($_POST['id']) && $_POST['id']) {
            $area = Areas::model()->findByPk($_POST['id']);
            $_SESSION['min_order'] = $area['min_order'];
            $_SESSION['delivery_fee'] = $area['delivery_fee'];
            $_SESSION['free_delivery_fee'] = $area['free_delivery_amount'];
            $_SESSION['delivery_time'] = $area['delivery_time'];
            $_SESSION['delivery_type'] = 'd';

            echo json_encode($area['attributes']);
        }
    }

    public function actionDeleteItem()
    {
        $session = new CHttpSession;               //add this line
        $session->open();
        $item = Item::model()->findByPk($_POST['item_id']);

        if ($item) {
            if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
                $total_price = $_SESSION['total_price'];
            } else {
                $total_price = 0;
            }

            if (isset($_SESSION['item'][$_POST['item_count']]) && $_SESSION['item'][$_POST['item_count']]['item_id'] == $item['id']) {
                $qty = $_SESSION['item'][$_POST['item_count']]['qty'];
                unset($_SESSION['item'][$_POST['item_count']]);
                $total_price -= $qty * $item['price'];
            } else {
            }
            if ($total_price == 0) {
                unset($_SESSION['total_price']);
            } else {
                $_SESSION['total_price'] = $total_price;
            }
        }


    }

    public function actionCart()
    {
        $this->render('cart');
    }

    public function actionCartReload()
    {
        $this->renderPartial('cart');
    }


    public function actionAddToCart()
    {
        $session = new CHttpSession;               //add this line
        $session->open();

        $item = Item::model()->findByPk($_POST['item']);
        if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
            $total_price = $_SESSION['total_price'];
        } else {
            $total_price = 0;
        }

        if (isset($_SESSION['item_count']) && $_SESSION['item_count']) {
            $i = $_SESSION['item_count'];
        } else {
            $i = 0;
        }

        $qty = 1;
        $_SESSION['item'][$i] = array('item_id' => $_POST['item'],
            'name' => $item['name'],
            'price' => $item['price'],
            'qty' => $qty,
            'item_position' => $i,
        );

        if ($item['subitem']) {
            foreach ($item['subitem'] as $subitem) {
                if ($subitem['selection_type'] == 1) {
                    $subitem['heading'] = str_replace(' ', "_", $subitem['heading']);
                    if (isset($_POST[$subitem['heading']]) && $_POST[$subitem['heading']]) {
                        $_SESSION['item'][$i]['radio'][$subitem['heading']] = $_POST[$subitem['heading']];
                    }
                } else {
                    if ($subitem['subitemoption']) {
                        foreach ($subitem['subitemoption'] as $option) {
                            $option['option_name'] = str_replace(' ', "_", $option['option_name']);
                            if (isset($_POST[$option['option_name']]) && $_POST[$option['option_name']]) {
                                if ($_POST[$option['option_name']]) {
                                    $_SESSION['item'][$i]['checkbox'][$option['option_name']] = $_POST[$option['option_name']];
                                } else {
                                    //  unset($_SESSION['item'][$_POST['item']][$option['option_name']]);
                                }
                            }
                        }
                    }

                }
            }
        }

        $total_price += $item['price'];
        /* } */
        $_SESSION['total_price'] = $total_price;
        $i++;
        $_SESSION['item_count'] = $i;

        $cart_array = array(
            'total_price' => $total_price,
            'item_id' => $_POST['item'],
            'qty' => $qty,
            'price' => $item['price'],
            'item_name' => $item['name'],
        );
        echo json_encode($cart_array);
    }

    public function actionProceed($type, $discount = "")
    {
        $promotion= array();
        if (!$type) {
            $this->redirect(Yii::app()->baseUrl . '/site/');
        }
        if (isset($_GET['discount']) && $_GET['discount']) {
           $promotion = Promotion::model()->find("promotion_code =:promotion_code AND is_active = 1 AND date_from <= CURDATE() AND date_to >= CURDATE()" ,
               array(":promotion_code" => $discount ));
            //$this->pre($promotion , 1);
        }
        if ($type == 'c') {
            $_SESSION['delivery_fee'] = 0;
        }

        $this->layout = "customerLayout";

        if (isset($_SESSION['item']) && $_SESSION['item']) {

            if (isset($_POST['Customer']['mobile']) && $_POST['Customer']['mobile'] && empty(Yii::app()->session['customerId'])) {
                $_POST['Customer']['mobile'] = preg_replace('/[^0-9]/s', '', $_POST['Customer']['mobile']);
                $mobile = $_POST['Customer']['mobile'];
                $model = Customer::model()->find('mobile = :mobile ', array(":mobile" => $mobile));
                if (!$model) {
                    $model = new Customer;
                }
            } elseif (Yii::app()->session['customerId']) {
                $model = Customer::model()->find('id = :customer_id', array(":customer_id" => Yii::app()->session['customerId']));
                if (!$model) {
                    $model = new Customer;
                }
            } else {
                $model = new Customer;
            }

            if (isset($_POST['Customer'])) {
                $model->attributes = $_POST['Customer'];
                $model->password = md5($_POST['Customer']['mobile']);
                $model->is_active = 1;
                if ($type == 'c') {
                    $_POST['Customer']['address'] = Controller::ADDRESS;
                }

                if (isset($_POST['Customer']['addr_radio']) && $_POST['Customer']['addr_radio'] == 2) {
                    $model->scenario = "2";
                    $address = $_POST['Customer']['address2'];
                    $model->address2 = $_POST['Customer']['address2'];
                } elseif (isset($_POST['Customer']['addr_radio']) && $_POST['Customer']['addr_radio'] == 3) {
                    $model->scenario = "3";
                    $address = $_POST['Customer']['address3'];
                    $model->address3 = $_POST['Customer']['address3'];
                } else {
                    $address = $_POST['Customer']['address'];
                    $model->address = $_POST['Customer']['address'];
                }

                if ($model->validate()) {
                    if ($model->save(false)) {
                        $lastInsertedId = $model->id;
                        $order = new CustomerOrder;
                        $order->customer_id = $lastInsertedId;
                        $order->shipping_address = $address;
                        $order->total_price = $_SESSION['total_price'];
                        if($promotion){
                            $order->discounted_amount = round(($_SESSION['total_price'] * $promotion['discounted_value']) / 100);
                        }
                        $order->order_date = date('Y-m-d');
                        $order->order_time = date('h:i:sA');
                        $order->comments = $_POST['Customer']['comments'];
                        $order->status = 1;
                        if ($type == 'c') {
                            $order->is_delivery = 0;
                            $order->delivery_charges = $_SESSION['delivery_fee'];

                        } else {
                            $order->is_delivery = 1;
                            $order->delivery_charges = $_SESSION['delivery_fee'];
                        }

                        $order->is_active = 1;
                        if ($order->save(false)) {
                            $orderinsertedId = $order->id;
                            $order_status = new OrderStatus();
                            $order_status->order_id = $orderinsertedId;
                            $order_status->status_id = 1; // Pending
                            $order_status->date_added = date('c');
                            $order_status->is_active = 1;
                            $order_status->save(false);
                            foreach ($_SESSION['item'] as $item) {
                                $order_detail = new OrderDetails;
                                $order_detail->order_id = $orderinsertedId;
                                $order_detail->price = $item['price'];
                                $order_detail->qty = $item['qty'];
                                $order_detail->item_id = $item['item_id'];
                                $order_detail->json_data = json_encode($item);
                                $order_detail->is_active = 1;
                                $order_detail->save(false);
                            }

                            $message = urlencode('Thank you for placing your order. Your order ID is ' . $orderinsertedId);
                            $mobile = $_POST['Customer']['mobile'];
                            $email = $_POST['Customer']['email'];
                            $_SESSION['order_id'] = $orderinsertedId;
                            // $xml = file_get_contents("http://smsctp1.eocean.us:24555/api?action=sendmessage&username=ideabox&password=IdEaB0X12&recipient=" . $mobile . "&originator=IDEABOX&messagedata=$message");

                            /* $ch = curl_init();
                             curl_setopt($ch, CURLOPT_URL, "http://smsctp1.eocean.us:24555/api?action=sendmessage&username=ideabox&password=IdEaB0X12&recipient=" . $mobile . "&originator=IDEABOX&messagedata=$message");
                             curl_setopt($ch, CURLOPT_POST, 1);
                             curl_setopt($ch, CURLOPT_POSTFIELDS, '');
                             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                             curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
                             curl_setopt($ch, CURLOPT_TIMEOUT, 200);
                             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                 'Content-Type: application/json'
                             ));
                             $response = curl_exec($ch);*/

                        }

                        $message = "<div id='shoppingcart'>";
                        $message .= "<h3 class='green' style='font-size: 24px'>Thank you for placing your order. Your order ID is . $orderinsertedId</h3><br/>";
                        $message .= "<h4 class='green' style='font-size: 20px;font-weight: bold;text-decoration: underline'>Client Details</h4>";
                        $message .= "<b>Name:</b> " . $_POST['Customer']['username'] . "<br/><br/>";
                        $message .= "<b>Email:</b> " . $_POST['Customer']['email'] . "<br/><br/>";
                        $message .= "<b>Phone:</b> " . $_POST['Customer']['mobile'] . "<br/><br/>";
                        $message .= "<b>Address:</b> " . $_POST['Customer']['address'] . "<br/><br/>";
                        $message .= "<b>Comments:</b> " . $_POST['Customer']['comments'] . "<br/><br/>";

                        $message .= "<h4 class='green' style='font-size: 20px;font-weight: bold;text-decoration: underline'>Order Details</h4>";
                        $message .= "<table><tbody>";

                        if (isset(Yii::app()->Session['item']) && Yii::app()->Session['item']) {
                            $total_price = 0;
                            $i = 0;
                            foreach (Yii::app()->Session['item'] as $cartitem) {
                                $total_price += $cartitem['qty'] * $cartitem['price'];
                                $message .= "<tr style='border-bottom: 1px solid #ccc;'>";
                                $message .= "<td valign='top' style='padding: 5px 10px 5px 0px; width: 50%;font-family: Helvetica; font-size: 13px;'>";
                                $message .= "(x" . $cartitem['qty'] . ")" . $cartitem['name'];

                                $message .= "<br/>";
                                if (isset($cartitem['radio']) && $cartitem['radio']) {
                                    foreach ($cartitem['radio'] as $variety => $value) {
                                        $option = SubitemOption::model()->findByPk($value);

                                        $message .= "<span>(" . $option['option_name'] . ")</span><br/>";

                                    }
                                }


                                $message .= "</td>";
                                $message .= "<td valign='top' style='padding-right: 20px;padding-bottom: 10px;padding-top: 10px; text-align: right;'>";
                                $message .= number_format($cartitem['price']) . "</td>";
                                $message .= "<td valign='top' style='padding: 0px 0px 5px 0px'>";

                                $message .= "</td></tr>";

                                $i++;
                            }
                        }

                        if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
                            $total_amount = $_SESSION['total_price'];
                        } else {
                            $total_amount = 0;
                        }

                        $message .= "<tr>";
                        $message .= "<td>SubTotal</td>";
                        $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
                        $message .= "Rs. " . $total_amount;
                        $message .= "</td>";
                        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
                        $message .= "</tr>";
                        if($promotion){
                            $message .= "<tr>";
                            $message .= "<td style='padding-top: 5px;'>Discount</td>";
                            $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
                            $message .= "Rs." . $discount = round(($_SESSION['total_price'] * $promotion['discounted_value']) / 100);
                            $total_amount -= $discount;
                            $message .= "</td>";
                            $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
                            $message .= "</tr>";

                        }


                        $message .= "<tr>";
                        $message .= "<td style='padding-top: 5px;'>Tax (13%)</td>";
                        $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
                        $message .= "Rs." . $tax = round(($total_amount * 13) / 100);
                        $total_amount += $tax;
                        $message .= "</td>";
                        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
                        $message .= "</tr>";

                        $message .= "<tr>";
                        $message .= "<td style='padding-top: 5px;'>Delivery Fee</td>";
                        /* if (isset($_SESSION['delivery_fee']) && $_SESSION['delivery_fee']) {
                             $total_amount += $_SESSION['delivery_fee'];
                             $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
                             $message .= "Rs." . $_SESSION['delivery_fee'] . "</td>";
                         } else {
                             $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>Rs. 0</td>";
                         }*/
                        $delivery_charges = $_SESSION['delivery_fee'];
                        $total_amount += $delivery_charges;
                        $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>Rs. $delivery_charges</td>";
                        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
                        $message .= "</tr>";
                        $message .= "<tr>";
                        $message .= "<td style='font-size: 22px;padding-top: 5px; border-top:1px solid #ccc'><b>Total</b></td>";
                        $message .= "<td style='font-size: 22px;padding-top: 5px; padding-right: 20px; text-align: right;border-top:1px solid #ccc'>";
                        $message .= "Rs.<b>" . number_format($total_amount, 0) . "</b></td> ";
                        $message .= "<td style = 'padding-top: 5px;' >&nbsp;</td> ";
                        $message .= "</tr>";
                        $message .= "<tr>";
                        $message .= "<td colspan = '3' >&nbsp;</td >";
                        $message .= "</tr>";
                        $message .= "</tbody ></table>";
                        $message .= "</div>";

                        $this->mailsend($email, '', '', 'Thank you for placing your order', $message);
                        //$this->mailsend('The Health Act <info@thehealthact.com>', '', '', 'Thank you for placing your order', $message);

                        $this->redirect(Yii::app()->baseUrl . '/site/thanks');
                        return;
                    }

                }
            }
            $this->render('proceed', array('model' => $model, 'type' => $type,'promotion' => $promotion));

        } else {
            $this->redirect(Yii::app()->baseUrl . '/site/');
        }
    }

    public function actionThanks()
    {
        $this->layout = "customerLayout";
        if (isset($_SESSION['item']) && $_SESSION['item']) {
            $order_id = $_SESSION['order_id'];
            unset($_SESSION['item']);
            unset($_SESSION['total_price']);
            unset($_SESSION['item_count']);
            unset($_SESSION['min_order']);
            unset($_SESSION['delivery_fee']);
            unset($_SESSION['delivery_time']);
            unset($_SESSION['delivery_type']);
            $this->render('thanks', array('order_id' => $order_id));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/site/');
        }


    }

    public function actionTest()
    {
        $message = "<div id='shoppingcart'>";
        $message .= "<h3 class='green' style='font-size: 24px'>Thank you for placing your order</h3>";
        $message .= "<table><tbody>";

        if (isset(Yii::app()->Session['item']) && Yii::app()->Session['item']) {
            $total_price = 0;
            $i = 0;
            foreach (Yii::app()->Session['item'] as $cartitem) {
                $total_price += $cartitem['qty'] * $cartitem['price'];
                $message .= "<tr style='border-bottom: 1px solid #ccc;'>";
                $message .= "<td valign='top' style='padding: 5px 10px 5px 0px; width: 50%;font-family: Helvetica; font-size: 13px;'>";
                $message .= "(x" . $cartitem['qty'] . ")" . $cartitem['name'];

                $message .= "<br/>";
                if (isset($cartitem['radio']) && $cartitem['radio']) {
                    foreach ($cartitem['radio'] as $variety => $value) {
                        $option = SubitemOption::model()->findByPk($value);

                        $message .= "<span>(" . $option['option_name'] . ")</span><br/>";

                    }
                }


                $message .= "</td>";
                $message .= "<td valign='top' style='padding-right: 20px;padding-bottom: 10px;padding-top: 10px; text-align: right;'>";
                $message .= number_format($cartitem['price']) . "</td>";
                $message .= "<td valign='top' style='padding: 0px 0px 5px 0px'>";

                $message .= "</td></tr>";

                $i++;
            }
        }

        if (isset($_SESSION['total_price']) && $_SESSION['total_price']) {
            $total_amount = $_SESSION['total_price'];
        } else {
            $total_amount = 0;
        }

        $message .= "<tr>";
        $message .= "<td>SubTotal</td>";
        $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
        $message .= "Rs. " . $total_amount;
        $message .= "</td>";
        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
        $message .= "</tr>";
        $message .= "<tr>";
        $message .= "<td style='padding-top: 5px;'>Tax (13%)</td>";
        $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
        $message .= "Rs." . $tax = round(($total_amount * 13) / 100);
        $message .= "</td>";
        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
        $message .= "</tr>";

        $message .= "<tr>";
        $message .= "<td style='padding-top: 5px;'>Delivery Fee</td>";
        if (isset($_SESSION['delivery_fee']) && $_SESSION['delivery_fee']) {
            $total_amount += $_SESSION['delivery_fee'];
            $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>";
            $message .= "Rs." . $_SESSION['delivery_fee'] . "</td>";
        } else {
            $message .= "<td style='padding-top: 5px; padding-right: 20px; text-align: right;'>Rs. 0</td>";
        }
        $message .= "<td style='padding-top: 5px;'>&nbsp;</td>";
        $message .= "</tr>";
        $message .= "<tr>";
        $message .= "<td style='font-size: 22px;padding-top: 5px; border-top:1px solid #ccc'><b>Total</b></td>";
        $message .= "<td style='font-size: 22px;padding-top: 5px; padding-right: 20px; text-align: right;border-top:1px solid #ccc'>";
        $message .= "Rs.<b>" . number_format($total_amount + $tax, 0) . "</b></td> ";
        $message .= "<td style = 'padding-top: 5px;' >&nbsp;</td> ";
        $message .= "</tr>";
        $message .= "<tr>";
        $message .= "<td colspan = '3' >&nbsp;</td >";
        $message .= "</tr>";
        $message .= "</tbody ></table>";
        $message .= "</div>";
        $this->mailsend('The Health Act <info@thehealthact.com>', '', '', 'Thank you for placing your order', $message);
    }

    public function actionCheckout()
    {
        $this->pre($_POST, 1);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->layout = "proceedsLayout";
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionPage404()
    {
//$this->layout = "viewsLayout";
        $message = "Page Not Found . ";

        $this->render('error', array('message' => $message));

    }

    public function actionContact()
    {
        $this->layout = "viewsLayout";
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: The Health Act <{
        $model->email}>\r\n" .
                    "Reply - To: {
        $model->email}\r\n" .
                    "MIME - Version: 1.0\r\n" .
                    "Content - Type: text / plain; charset = UTF - 8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */

    public function actionCustomerLogin()
    {
        $this->layout = "customerLayout";
        $model = new LoginForm;

// if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (isset($_SESSION['delivery_type']) && $_SESSION['delivery_type']) {
                    $type = $_SESSION['delivery_type'];
                } else {
                    $type = 'c';
                }
                $checkUser = Customer::model()->findByPk(Yii::app()->session['customerId']);
                $checkUser->is_registered = 1;
                $checkUser->save(false);
                $this->redirect(Yii::app()->baseUrl . '/site/proceed/type/' . $type);
            }
        }
// display the login form
//$_SESSION['delivery_type']
        $this->renderPartial('customerlogin', array('model' => $model));
    }

    public function actionFblogin($id, $name)
    {
        $checkUser = Customer::model()->find("fb_user_id = :fb_user", array(':fb_user' => $id));
        if ($checkUser) {
            Yii::app()->session['customerId'] = $checkUser['id'];
        } else {
            $model = new Customer;
            $model->username = $name;
            $model->fb_user_id = $id;
            $model->is_registered = 1;
            $model->is_active = 1;
            $model->save(false);

            Yii::app()->session['customerId'] = $model->id;
        }

        if (isset($_SESSION['delivery_type']) && $_SESSION['delivery_type']) {
            $type = $_SESSION['delivery_type'];
        } else {
            $type = 'c';
        }
        $this->redirect(Yii::app()->baseUrl . '/site/proceed/type/' . $type);
    }

    public function actionFbTest()
    {
//$this->layout = 'nolayout';
        $this->renderPartial('fbtest');
    }


    public function actionLogin()
    {
        if (Yii::app()->session['userId'] && Yii::app()->session['userType']) {
            $this->redirect(Yii::app()->baseUrl . '/site/');
        }
//$this->layout = 'loginlayout';
        $this->layout = "adminLoginLayout";
        $model = new LoginForm;

// if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())

                $this->redirect(Yii::app()->baseUrl . '/admin/index');
        }
// display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionAdmin()
    {
        $this->redirect(Yii::app()->baseUrl . '/admin/index');
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->baseUrl . '/admin');
    }
}