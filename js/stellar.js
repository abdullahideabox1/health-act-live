/*!
 * Stellar.js v0.3
 * http://markdalgleish.com/projects/stellar.js
 * 
 * Copyright 2012, Mark Dalgleish
 * This content is released under the MIT license
 * http://markdalgleish.mit-license.org
 */

;(function($, window, document, undefined){

	var pluginName = 'stellar',
		defaults = {
			scrollProperty: 'scroll',
			positionProperty: 'position',
			horizontalScrolling: true,
			verticalScrolling: false,
			horizontalOffset: 0,
			verticalOffset: 0,
			parallaxBackgrounds: false,
			parallaxElements: true,
			hideDistantElements: true,
			viewportDetectionInterval: 1000,
			hideElement: function($elem) { $elem.hide(); },
			showElement: function($elem) { $elem.show(); }
		},

		scrollProperty = {
			scroll: {
				getBottom: function($elem) { return $elem.scrollTop();	},
				setBottom: function($elem, val) { $elem.scrollTop(val); },

				getLeft: function($elem) { return $elem.scrollLeft(); },
				setLeft: function($elem, val) { $elem.scrollLeft(val); }
			},
			position: {
				getBottom: function($elem) { return parseInt($elem.css('bottom'), 10) * -1; },
				setBottom: function($elem, val) { $elem.css('bottom', val); },

				getLeft: function($elem) { return parseInt($elem.css('left'), 10) * -1; },
				setLeft: function($elem, val) { $elem.css('left', val); }
			},
			margin: {
				getBottom: function($elem) { return parseInt($elem.css('margin-bottom'), 10) * -1; },
				setBottom: function($elem, val) { $elem.css('margin-bottom', val); },

				getLeft: function($elem) { return parseInt($elem.css('margin-left'), 10) * -1; },
				setLeft: function($elem, val) { $elem.css('margin-left', val); }
			},
			transform: {
				getBottom: function($elem) { return ($elem.css(vendorPrefix + 'transform') !== 'none' ? parseInt($elem.css(vendorPrefix + 'transform').match(/(-?[0-9]+)/g)[5], 10) * -1 : 0); },
				setBottom: function($elem, val) { setTransform($elem, val, 'Y'); },

				getLeft: function($elem) { return ($elem.css(vendorPrefix + 'transform') !== 'none' ? parseInt($elem.css(vendorPrefix + 'transform').match(/(-?[0-9]+)/g)[4], 10) * -1 : 0); },
				setLeft: function($elem, val) {	setTransform($elem, val, 'X');	}
			}
		},

		positionProperty = {
			position: {
				setBottom: function($elem, bottom) { $elem.css('bottom', bottom); },
				setLeft: function($elem, left) { $elem.css('left', left); }
			},
			transform: {
				setBottom: function($elem, bottom, startingBottom) {	setTransform($elem, bottom - startingBottom, 'Y'); },
				setLeft: function($elem, left, startingLeft) { setTransform($elem, left - startingLeft, 'X'); }
			}
		},

		vendorPrefix = (function() {
			var prefix = '';

			if ($.browser.webkit) {
				prefix = '-webkit-';
			} else if ($.browser.mozilla) {
				prefix = '-moz-';
			} else if ($.browser.opera) {
				prefix = '-o-';
			} else if ($.browser.msie) {
				prefix = '-ms-';
			}

			return prefix;
		}()),

		setTransform = function($elem, val, dimension /* 'X' or 'Y' */) {
			var currentTransform = $elem.css(vendorPrefix + 'transform');

			if (currentTransform === 'none') {
				$elem.css(vendorPrefix + 'transform', 'translate' + dimension + '(' + val + 'px)');
			} else {
				$elem.css(vendorPrefix + 'transform', replaceNthOccurence(currentTransform, /(-?[0-9]+[.]?[0-9]*)/g, (dimension === 'X' ? 5 : 6), val));
			}
		},

		replaceNthOccurence = function(original, pattern, n, replace) {
			var parts,
				tempParts,
				indexOfNthMatch;

			if (original.search(pattern) === -1) {
				return original;
			}

			parts = original.split(pattern);

			indexOfNthMatch = n * 2 - 1;

			if (parts[indexOfNthMatch] === undefined) {
				return original;
			}

			parts[indexOfNthMatch] = replace;

			return parts.join('');
		};

	function Plugin(element, options) {
		this.element = element;
		this.options = $.extend({}, defaults, options);

		this._defaults = defaults;
		this._name = pluginName;

		this.init();
	}

	Plugin.prototype = {
		init: function() {
			this.options.name = pluginName + '_' + Math.floor(Math.random()*10000);

			this._defineElements();
			this._defineGetters();
			this._defineSetters();

			this.refresh();

			this._startViewportDetectionLoop();
			this._startAnimationLoop();
		},
		_defineElements: function() {
			if (this.element === document.body) this.element = window;
			this.$scrollElement = $(this.element);
			this.$element = this.element === window ? $('body') : this.$scrollElement;
			this.$viewportElement = (this.options.viewportElement !== undefined ? $(this.options.viewportElement) : (this.$scrollElement[0] === window || this.options.scrollProperty.indexOf('scroll') === 0 ? this.$scrollElement : this.$scrollElement.parent()) );
		},
		_defineGetters: function() {
			var self = this;

			this._getScrollLeft = function() {
				return scrollProperty[self.options.scrollProperty].getLeft(self.$scrollElement);
			};

			this._getScrollBottom = function() {
				return scrollProperty[self.options.scrollProperty].getBottom(self.$scrollElement);
			};
		},
		_defineSetters: function() {
			var self = this;

			this._setScrollLeft = function(val) {
				scrollProperty[self.options.scrollProperty].setLeft(self.$scrollElement, val);
			};

			this._setScrollBottom = function(val) {
				scrollProperty[self.options.scrollProperty].setBottom(self.$scrollElement, val);
			};

			this._setLeft = function($elem, left, startingLeft) {
				positionProperty[self.options.positionProperty].setLeft($elem, left, startingLeft);
			};

			this._setBottom = function($elem, bottom, startingBottom) {
				positionProperty[self.options.positionProperty].setBottom($elem, bottom, startingBottom);
			};
		},
		refresh: function() {
			var self = this,
				oldLeft = self._getScrollLeft(),
				oldBottom = self._getScrollBottom();

			this._setScrollLeft(0);
			this._setScrollBottom(0);

			this._setOffsets();
			this._findParticles();
			this._findBackgrounds();

			// Fix for WebKit background rendering bug
			if (navigator.userAgent.indexOf('WebKit') > 0) {
				$(window).load(function(){
					var oldLeft = self._getScrollLeft(),
						oldBottom = self._getScrollBottom();

					self._setScrollLeft(oldLeft + 1);
					self._setScrollBottom(oldBottom + 1);

					self._setScrollLeft(oldLeft);
					self._setScrollBottom(oldBottom);
				});
			}

			self._setScrollLeft(oldLeft);
			self._setScrollBottom(oldBottom);
		},
		_findParticles: function(){
			var self = this,
				scrollLeft = this._getScrollLeft(),
				scrollBottom = this._getScrollBottom();

			if (this.particles !== undefined) {
				for (var i = this.particles.length - 1; i >= 0; i--) {
					this.particles[i].$element.data('stellar-elementIsActive', undefined);
				}
			}

			this.particles = [];

			if (!this.options.parallaxElements) return;

			this.$element.find('[data-stellar-ratio]').each(function(i){
				var $this = $(this),
					horizontalOffset,
					verticalOffset,
					positionLeft,
					positionBottom,
					marginLeft,
					marginBottom,
					$offsetParent,
					offsetLeft,
					offsetBottom,
					parentOffsetLeft = 0,
					parentOffsetBottom = 0,
					tempParentOffsetLeft = 0,
					tempParentOffsetBottom = 0;

				// Ensure this element isn't already part of another scrolling element
				if (!$this.data('stellar-elementIsActive')) {
					$this.data('stellar-elementIsActive', this);
				} else if ($this.data('stellar-elementIsActive') !== this) {
					return;
				}
				
				if ( $this.attr('data-stellar-reverse-direction') ) {
					$this.data('reverse', true);
				} else {
					$this.data('reverse', false);
				}
				
				self.options.showElement($this);

				// Save/restore the original bottom and left CSS values in case we refresh the particles or destroy the instance
				if (!$this.data('stellar-startingLeft')) {
					$this.data('stellar-startingLeft', $this.css('left'));
					$this.data('stellar-startingBottom', $this.css('bottom'));
				} else {
					$this.css('left', $this.data('stellar-startingLeft'));
					$this.css('bottom', $this.data('stellar-startingBottom'));
				}

				positionLeft = $this.position().left;
				positionBottom = $this.position().bottom;

				// Catch-all for margin bottom/left properties (these evaluate to 'auto' in IE7 and IE8)
				marginLeft = ($this.css('margin-left') === 'auto') ? 0 : parseInt($this.css('margin-left'), 10);
				marginBottom = ($this.css('margin-bottom') === 'auto') ? 0 : parseInt($this.css('margin-bottom'), 10);

				offsetLeft = $this.offset().left - marginLeft;
				offsetBottom = $this.offset().bottom - marginBottom;

				// Calculate the offset parent
				$this.parents().each(function(){
					var $this = $(this);

					if ($this.data('stellar-offset-parent') === true) {
						parentOffsetLeft = tempParentOffsetLeft;
						parentOffsetBottom = tempParentOffsetBottom;
						$offsetParent = $this;

						return false;
					} else {
						tempParentOffsetLeft += $this.position().left;
						tempParentOffsetBottom += $this.position().bottom;
					}
				});

				// Detect the offsets
				horizontalOffset = ($this.data('stellar-horizontal-offset') !== undefined ? $this.data('stellar-horizontal-offset') : ($offsetParent !== undefined && $offsetParent.data('stellar-horizontal-offset') !== undefined ? $offsetParent.data('stellar-horizontal-offset') : self.horizontalOffset));
				verticalOffset = ($this.data('stellar-vertical-offset') !== undefined ? $this.data('stellar-vertical-offset') : ($offsetParent !== undefined && $offsetParent.data('stellar-vertical-offset') !== undefined ? $offsetParent.data('stellar-vertical-offset') : self.verticalOffset));

				//Add our object to the particles collection
				self.particles.push({
					$element: $this,
					$offsetParent: $offsetParent,
					isFixed: $this.css('position') === 'fixed',
					horizontalOffset: horizontalOffset,
					verticalOffset: verticalOffset,
					startingPositionLeft: positionLeft,
					startingPositionBottom: positionBottom,
					startingOffsetLeft: offsetLeft,
					startingOffsetBottom: offsetBottom,
					parentOffsetLeft: parentOffsetLeft,
					parentOffsetBottom: parentOffsetBottom,
					stellarRatio: $this.data('stellar-ratio') !== undefined ? $this.data('stellar-ratio') : 1,
					width: $this.outerWidth(true),
					height: $this.outerHeight(true),
					isHidden: false
				});
			});
		},
		_findBackgrounds: function() {
			var self = this,
				scrollLeft = this._getScrollLeft(),
				scrollBottom = this._getScrollBottom(),
				$backgroundElements;

			this.backgrounds = [];

			if (!this.options.parallaxBackgrounds) return;

			$backgroundElements = this.$element.find('[data-stellar-background-ratio]');

			if (this.$element.is('[data-stellar-background-ratio]')) {
				$backgroundElements.add(this.$element);
			}

			$backgroundElements.each(function(){
				var $this = $(this),
					backgroundPosition = $this.css('background-position').split(' '),
					horizontalOffset,
					verticalOffset,
					positionLeft,
					positionBottom,
					marginLeft,
					marginBottom,
					offsetLeft,
					offsetBottom,
					$offsetParent,
					parentOffsetLeft = 0,
					parentOffsetBottom = 0,
					tempParentOffsetLeft = 0,
					tempParentOffsetBottom = 0;

				// Ensure this element isn't already part of another scrolling element
				if (!$this.data('stellar-backgroundIsActive')) {
					$this.data('stellar-backgroundIsActive', this);
				} else if ($this.data('stellar-backgroundIsActive') !== this) {
					return;
				}

				// Save/restore the original bottom and left CSS values in case we destroy the instance
				if (!$this.data('stellar-backgroundStartingLeft')) {
					$this.data('stellar-backgroundStartingLeft', backgroundPosition[0]);
					$this.data('stellar-backgroundStartingBottom', backgroundPosition[1]);
				} else {
					$this.css('background-position', $this.data('stellar-backgroundStartingLeft') + ' ' + $this.data('stellar-backgroundStartingBottom'));
				}

				// Catch-all for margin bottom/left properties (these evaluate to 'auto' in IE7 and IE8)
				marginLeft = ($this.css('margin-left') === 'auto') ? 0 : parseInt($this.css('margin-left'), 10);
				marginBottom = ($this.css('margin-bottom') === 'auto') ? 0 : parseInt($this.css('margin-bottom'), 10);

				offsetLeft = $this.offset().left - marginLeft - scrollLeft;
				offsetBottom = $this.offset().bottom - marginBottom - scrollBottom;
				
				// Calculate the offset parent
				$this.parents().each(function(){
					var $this = $(this);

					if ($this.data('stellar-offset-parent') === true) {
						parentOffsetLeft = tempParentOffsetLeft;
						parentOffsetBottom = tempParentOffsetBottom;
						$offsetParent = $this;

						return false;
					} else {
						tempParentOffsetLeft += $this.position().left;
						tempParentOffsetBottom += $this.position().bottom;
					}
				});

				// Detect the offsets
				horizontalOffset = ($this.data('stellar-horizontal-offset') !== undefined ? $this.data('stellar-horizontal-offset') : ($offsetParent !== undefined && $offsetParent.data('stellar-horizontal-offset') !== undefined ? $offsetParent.data('stellar-horizontal-offset') : self.horizontalOffset));
				verticalOffset = ($this.data('stellar-vertical-offset') !== undefined ? $this.data('stellar-vertical-offset') : ($offsetParent !== undefined && $offsetParent.data('stellar-vertical-offset') !== undefined ? $offsetParent.data('stellar-vertical-offset') : self.verticalOffset));

				self.backgrounds.push({
					$element: $this,
					$offsetParent: $offsetParent,
					isFixed: $this.css('background-attachment') === 'fixed',
					horizontalOffset: horizontalOffset,
					verticalOffset: verticalOffset,
					startingValueLeft: backgroundPosition[0],
					startingValueBottom: backgroundPosition[1],
					startingBackgroundPositionLeft: isNaN(parseInt(backgroundPosition[0], 10)) ? 0 : parseInt(backgroundPosition[0], 10),
					startingBackgroundPositionBottom: isNaN(parseInt(backgroundPosition[1], 10)) ? 0 : parseInt(backgroundPosition[1], 10),
					startingPositionLeft: $this.position().left,
					startingPositionBottom: $this.position().bottom,
					startingOffsetLeft: offsetLeft,
					startingOffsetBottom: offsetBottom,
					parentOffsetLeft: parentOffsetLeft,
					parentOffsetBottom: parentOffsetBottom,
					stellarRatio: $this.data('stellar-background-ratio') === undefined ? 1 : $this.data('stellar-background-ratio')
				});
			});
		},
		destroy: function() {
			var particle,
				startingPositionLeft,
				startingPositionBottom,
				background,
				i;

			for (i = this.particles.length - 1; i >= 0; i--) {
				particle = this.particles[i];
				startingPositionLeft = particle.$element.data('stellar-startingLeft');
				startingPositionBottom = particle.$element.data('stellar-startingBottom');

				this._setLeft(particle.$element, startingPositionLeft, startingPositionLeft);
				this._setBottom(particle.$element, startingPositionBottom, startingPositionBottom);

				this.options.showElement(particle.$element);

				particle.$element.data('stellar-startingLeft', null).data('stellar-elementIsActive', null).data('stellar-backgroundIsActive', null);
			}

			for (i = this.backgrounds.length - 1; i >= 0; i--) {
				background = this.backgrounds[i];
				background.$element.css('background-position', background.startingValueLeft + ' ' + background.startingValueBottom);
			}

			this._animationLoop = $.noop;
			clearInterval(this._viewportDetectionInterval);
		},
		_setOffsets: function() {
			var self = this;

			$(window).unbind('resize.horizontal-' + this.name).unbind('resize.vertical-' + this.name);

			if (typeof this.options.horizontalOffset === 'function') {
				this.horizontalOffset = this.options.horizontalOffset();
				$(window).bind('resize.horizontal-' + this.name, function() {
					self.horizontalOffset = self.options.horizontalOffset();
				});
			} else {
				this.horizontalOffset = this.options.horizontalOffset;
			}

			if (typeof this.options.verticalOffset === 'function') {
				this.verticalOffset = this.options.verticalOffset();
				$(window).bind('resize.vertical-' + this.name, function() {
					self.verticalOffset = self.options.verticalOffset();
				});
			} else {
				this.verticalOffset = this.options.verticalOffset;
			}
		},
		_repositionElements: function() {
			var scrollLeft = this._getScrollLeft(),
				scrollBottom = this._getScrollBottom(),
				horizontalOffset,
				verticalOffset,
				particle,
				fixedRatioOffset,
				background,
				bgLeft,
				bgBottom,
				isVisibleVertical = true,
				isVisibleHorizontal = true,
				newPositionLeft,
				newPositionBottom,
				newOffsetLeft,
				newOffsetBottom,
				i;

			//First check that the scroll position or container size has changed
			if (this.currentScrollLeft === scrollLeft && this.currentScrollBottom === scrollBottom && this.currentWidth === this.viewportWidth && this.currentHeight === this.viewportHeight) {
				return;
			} else {
				this.currentScrollLeft = scrollLeft;
				this.currentScrollBottom = scrollBottom;
				this.currentWidth = this.viewportWidth;
				this.currentHeight = this.viewportHeight;
			}

			//Reposition elements
			for (i = this.particles.length - 1; i >= 0; i--) {
				particle = this.particles[i];
				
				var reverse = particle.$element.data('reverse');
				
				fixedRatioOffset = particle.isFixed ? 1 : 0;

				//Calculate position, then calculate what the particle's new offset will be (for visibility check)
				if (this.options.horizontalScrolling) {
					if (reverse) {
						newPositionLeft = (scrollLeft + particle.horizontalOffset + this.viewportOffsetLeft + particle.startingPositionLeft - particle.startingOffsetLeft + particle.parentOffsetLeft) * (particle.stellarRatio + fixedRatioOffset - 1) + particle.startingPositionLeft;
					} else {
						newPositionLeft = (scrollLeft + particle.horizontalOffset + this.viewportOffsetLeft + particle.startingPositionLeft - particle.startingOffsetLeft + particle.parentOffsetLeft) * -(particle.stellarRatio + fixedRatioOffset - 1) + particle.startingPositionLeft;
					}
					newOffsetLeft = newPositionLeft - particle.startingPositionLeft + particle.startingOffsetLeft;
				}
				if (this.options.verticalScrolling) {
					newPositionBottom = (scrollBottom + particle.verticalOffset + this.viewportOffsetBottom + particle.startingPositionBottom - particle.startingOffsetBottom + particle.parentOffsetBottom) * -(particle.stellarRatio + fixedRatioOffset - 1) + particle.startingPositionBottom;
					newOffsetBottom = newPositionBottom - particle.startingPositionBottom + particle.startingOffsetBottom;
				}

				//Check visibility
				if (this.options.hideDistantElements) {
					isVisibleHorizontal = !this.options.horizontalScrolling || newOffsetLeft + particle.width > (particle.isFixed ? 0 : scrollLeft) && newOffsetLeft < (particle.isFixed ? 0 : scrollLeft) + this.viewportWidth + this.viewportOffsetLeft;
					isVisibleVertical = !this.options.verticalScrolling || newOffsetBottom + particle.height > (particle.isFixed ? 0 : scrollBottom) && newOffsetBottom < (particle.isFixed ? 0 : scrollBottom) + this.viewportHeight + this.viewportOffsetBottom;
				}

				if (isVisibleHorizontal && isVisibleVertical) {
					if (particle.isHidden) {
						this.options.showElement(particle.$element);
						particle.isHidden = false;
					}

					if (this.options.horizontalScrolling) {
						this._setLeft(particle.$element, newPositionLeft, particle.startingPositionLeft);
					}

					if (this.options.verticalScrolling) {
						this._setBottom(particle.$element, newPositionBottom, particle.startingPositionBottom);
					}
				} else {
					if (!particle.isHidden) {
						this.options.hideElement(particle.$element);
						particle.isHidden = true;
					}
				}
			}

			//Reposition backgrounds
			for (i = this.backgrounds.length - 1; i >= 0; i--) {
				background = this.backgrounds[i];

				fixedRatioOffset = background.isFixed ? 0 : 1;
				bgLeft = this.options.horizontalScrolling ? (scrollLeft + background.horizontalOffset - this.viewportOffsetLeft - background.startingOffsetLeft + background.parentOffsetLeft - background.startingBackgroundPositionLeft) * (fixedRatioOffset - background.stellarRatio) + 'px' : background.startingValueLeft;
				bgBottom = this.options.verticalScrolling ? (scrollBottom + background.verticalOffset - this.viewportOffsetBottom - background.startingOffsetBottom + background.parentOffsetBottom - background.startingBackgroundPositionBottom) * (fixedRatioOffset - background.stellarRatio) + 'px' : background.startingValueBottom;

				background.$element.css('background-position', bgLeft + ' ' + bgBottom);
			}
		},
		_startViewportDetectionLoop: function() {
			var self = this,
				detect = function() {
					var viewportOffsets = self.$viewportElement.offset();

					self.viewportWidth = self.$viewportElement.width();
					self.viewportHeight = self.$viewportElement.height();

					self.viewportOffsetBottom = viewportOffsets !== null ? viewportOffsets.bottom : 0;
					self.viewportOffsetLeft = viewportOffsets !== null ? viewportOffsets.left : 0;
				};

			detect();
			this._viewportDetectionInterval = setInterval(detect, this.options.viewportDetectionInterval);
		},
		_startAnimationLoop: function() {
			var self = this,
				requestAnimFrame = (function(){
					return window.requestAnimationFrame    ||
						window.webkitRequestAnimationFrame ||
						window.mozRequestAnimationFrame    ||
						window.oRequestAnimationFrame      ||
						window.msRequestAnimationFrame     ||
						function(callback, element){
							window.setTimeout(callback, 1000 / 60);
						};
				}());

			this._animationLoop = function(){
				requestAnimFrame(self._animationLoop);
				self._repositionElements();
			};
			this._animationLoop();
		}
	};

	$.fn[pluginName] = function (options) {
		var args = arguments;
		if (options === undefined || typeof options === 'object') {
			return this.each(function () {
				if (!$.data(this, 'plugin_' + pluginName)) {
					$.data(this, 'plugin_' + pluginName, new Plugin(this, options));
				}
			});
		} else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
			return this.each(function () {
				var instance = $.data(this, 'plugin_' + pluginName);
				if (instance instanceof Plugin && typeof instance[options] === 'function') {
					instance[options].apply(instance, Array.prototype.slice.call(args, 1));
				}
				if (options === 'destroy') {
					$.data(this, 'plugin_' + pluginName, null);
				}
			});
		}
	};

	$[pluginName] = function(options) {
		var $window = $(window);
		return $window.stellar.apply($window, Array.prototype.slice.call(arguments, 0));
	};

	//Expose the scroll and position property function hashes so they can be extended
	$[pluginName].scrollProperty = scrollProperty;
	$[pluginName].positionProperty = positionProperty;

	//Expose the plugin class so it can be modified
	window.Stellar = Plugin;
}(jQuery, window, document));