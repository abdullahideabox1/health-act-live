$(function () {


    var wWidth = $(window).width();
    var wHeight = $(window).height();
    var speed = 1000;
    var matched, browser;

    var cal = (1320 * $(window).height()) / 100;
    $('body').css('width',cal);

    $("body").mousewheel(function(event, delta) {

        var isCustomModalVisible= $('.overlay-b').hasClass('display');

        if( !isCustomModalVisible) {
            this.scrollLeft -= (delta * 100);
            event.preventDefault();
        }

    });


    // check browser
    jQuery.uaMatch = function (ua) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+)/.exec(ua) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
            /(msie) ([\w.]+)/.exec(ua) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
            [];

        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    };
    matched = jQuery.uaMatch(navigator.userAgent);
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    jQuery.browser = browser;
    //End

    //clock
    $("#timer").countdown("2016/12/04", function (event) {

        $('#time-day').text(event.strftime('%D'));
        $('#time-hrs').text(event.strftime('%H'));
        $('#time-min').text(event.strftime('%M'));
        $('#time-sec').text(event.strftime('%S'));
    });

    $('top-arrow').stellar({
        horizontalScrolling: true
    });


    $('#go-to-home').bind('click', function (e) {
        e.preventDefault();
        $(window).scrollTo(0, 2000, {axis: 'x'});
        return false;
    });

    $('#go-to-why-we-are-different').bind('click', function (e) {
        e.preventDefault();
        var val;
        val = (browser.chrome) ? 392 : 384;
        var cal = (val * $(window).height()) / 100;
        $(window).scrollTo(cal, 2000, {axis: 'x'});
        return false;
    });

    $('#go-to-join-us').bind('click', function (e) {
        e.preventDefault();
        var val; // unit is 'vh'
        val = (browser.chrome) ? 782 : 766;
        var cal = (val * $(window).height()) / 100;
        $(window).scrollTo(cal, 2000, {axis: 'x'});
        return false;
    });

    $('#go-to-contact-us').bind('click', function (e) {
        e.preventDefault();
        var val;
        val = (browser.chrome) ? 1220 : 1180;
        var cal = (val * $(window).height()) / 100;
        $(window).scrollTo(cal, 2000, {axis: 'x'});
        return false;
    });

    $('#show-our-work , #wrapper ,#bottom-row').bind('click', function (e) {
        e.preventDefault();
        /*
         if ( isBottomOpen ) {
         closeBottom();
         } else {
         if( !($(this).attr('id') == 'wrapper' || $(this).attr('id') == 'bottom-row'))
         openBottom();
         }*/

        return false;
    });


});
