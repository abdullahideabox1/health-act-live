<!--PAge Transactions-->
<div class="cd-cover-layer"></div>
<div class="cd-loading-bar"></div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
    $(function () {
        /*Form validate*/

        /*Form validate*/
        var dateToday = new Date();
        dateToday.setDate(dateToday.getDate() + 1);

        var dates = $("#startDate").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate: dateToday,
            onSelect: function (selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
        $("#endDate").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate: dateToday,
            onSelect: function (selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });
</script>
<!--PAge Transactions-->
<script>window.jQuery || document.write('<script src="../../theme/js/vendor/jquery-slim.min.js"><\/script>')</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/owl.carousel.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/common.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/common_ab.js"></script>

<!--PAge Transactions-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/main.js"></script>
<!--PAge Transactions-->

<!--Google Maps-->

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkQB4plPcapLPxaFEJ8ToFpgA-Bz6rOVU&libraries=places"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/mealplan_plugin.js"></script>

<!-- Google Maps settings -->
<script>

    // Regular map
    function regular_map() {
        var var_location = new google.maps.LatLng(24.814137, 67.051239);

        var var_mapoptions = {
            center: var_location,
            zoom: 20
        };

        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);

        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "Health Act"
        });
    }

    // Initialize maps
    google.maps.event.addDomListener(window, 'load', regular_map);


    $("#viewSchedule").click(function () {
        $(".scheduler").show(1500).animate(300).right;
        $('.close').click(function () {
            $(".scheduler").hide(1500);
        })
    });


    $(".weightopen").click(function () {
        $(".weightmeals").show(1500);
        $('.closelunch').click(function () {
            $(".weightmeals").hide(1500);
        })
    });
    $('.studioCarousel').owlCarousel({
        items: 1,
        loop: false,
        margin: 8,
        nav: true,
        navText: ["<img src='/theme/img/leftt-green.svg' width='50px'/>", "<img src='/theme/img/right-green.svg' width='50px'/>"],
        responsive: {
            992: {
                items: 4
            }
        }
    })

</script>


