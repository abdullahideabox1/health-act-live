<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Head -->
    <?php include './protected/views/layouts/head.php'; ?>
    <!-- Head -->
</head>
<body>
<?php include './protected/views/layouts/menu.php'; ?>
<?php echo $content; ?>

<!-- Footer -->
<footer class="footer-social float-lg-right text-center">
    <?php if (isset($_SESSION['userID']) != null) {
        $customer = Customer::model()->findByPk($_SESSION['userID']);
        ?>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard" class="text-white mr-3">Welcome, <?php echo $customer['username'] ?> </a>
    <?php } else { ?>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/clientlogin" class="text-white mr-3">Client Login</a>
    <?php } ?>

    <a class="btn btn-light btn-circle color-green" href="https://www.facebook.com/thehealthact" target="_blank">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a class="btn btn-light btn-circle color-green" href="https://www.instagram.com/thehealthact/" target="_blank">
        <i class="fab fa-instagram"></i>
    </a>
</footer>
<?php include './protected/views/layouts/footer.php'; ?>
<!-- Footer -->
</body>
</html>
