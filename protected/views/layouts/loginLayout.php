<?php
$page = "Login";
$title = "Login";
// set resource path for linking resources in subdirectories
$respath = Yii::app()->request->baseUrl . '/';
?>
<!DOCTYPE html>

<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]-->


<head>
    <!-- Jquery -->
    <script src="<?php echo $respath ?>js/admin/jquery.js"></script>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title> EMI - <?php if (isset($title)) echo $title; ?></title>
    <meta name="description" content="EMI">
    <meta name="author" content="Ansonika">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->

    <link href="<?php echo $respath ?>css/admin/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/bootstrap-responsive.css" rel="stylesheet">


    <link href="<?php echo $respath ?>css/admin/style.css" rel="stylesheet">

    <link href="<?php echo $respath ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $respath ?>css/admin/admincss.css" rel="stylesheet">



    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <!-- Support media queries for IE8 -->
    <script src="<?php echo $respath ?>js/admin/respond.min.js"></script>

    <!-- HTML5 and CSS3-in older browsers-->
    <script src="<?php echo $respath ?>js/admin/modernizr.custom.17475.js"></script>

    <!-- jquery validdte plugin -->

    <script src="<?php echo $respath ?>js/admin/jquery.validate.js"></script>


    <!--[if IE 7]>
    <link rel="stylesheet" href="../font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!-- Style switcher-->
    <link rel="stylesheet" type="text/css" media="screen,projection"
          href="<?php echo $respath ?>src/jquery-sticklr-1.4-light-color.css">



</head>

<body>
<div class="page-wrap">
    <!--[if !IE]><!-->
    <script>if (/*@cc_on!@*/false) {
            document.documentElement.className += ' ie10';
        }</script>
    <!--<![endif]--> <!-- Border radius fixed IE10-->


    <header>
        <div class="container">
            <div class="row">
                <div class="span4" id="logo">
                    <a href="<?php echo Yii::app()->baseUrl.'/'?>"><img src="http://placehold.it/275x45" alt="Logo" width="275" height="45"></a>
                </div>
            </div>
        </div>
    </header>


    <!-- header.php end -->

    <div class="container">
        <div class="row text-center">

            <div id="mid-box-container" class="row text-center">
                <hr>
                <?php echo $content?>

            </div>
        </div>
        <!-- end row-->
    </div>
    <!-- end container-->


    <div id="toTop">Back to Top</div>


</div>
<!--page wrap sticky footer fix -->

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="span11" id="brand-footer">
                <p>2015 - EMI Pakistan. Powered by IdeaBox</p>
            </div>
        </div>
    </div>
</footer>


<!-- End footer-->








</body>


</html>