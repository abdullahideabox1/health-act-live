<div class="headers">

    <nav class="navbar bg-white navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="<?php echo Yii::app()->request->baseUrl; ?>/index" data-type="page-transition"><img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/logo.png"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto header1" >
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/index" id="about"  data-type="page-transition">Home</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/about" id="about"  data-type="page-transition">About</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/consultation" id="consultation" data-type="page-transition">Consultation</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/gift" id="gifts" data-type="page-transition">Gifts</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="http://thehealthact.com/healthbox/" id="menu">Menu</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/MealPlans" id="mealplans" data-type="page-transition">Meal Plans</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/cold" id="juices" data-type="page-transition">Juices</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/FoodService" id="foodservices" data-type="page-transition">Food Services</a>
                </li>
                <li class="nav-item p-3">
                    <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/contact" id="contact" data-type="page-transition">Contact</a>
                </li>
            </ul>
        </div>
    </nav>
</div>