<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Head -->
    <?php include './protected/views/layouts/head.php'; ?>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/dashboardDefault.css" rel="stylesheet">
    <!-- Head -->
</head>
<body>
<?php include './protected/views/layouts/menu.php'; ?>

<nav class="col-md-2 d-none d-md-block bg-light sidebar mt-5">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-home">
                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                    </svg>
                    Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/profile">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-users">
                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                        <circle cx="9" cy="7" r="4"></circle>
                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                    </svg>
                    Profile
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/orders">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-file">
                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                        <polyline points="13 2 13 9 20 9"></polyline>
                    </svg>
                    Orders
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/mymealplans">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-file">
                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                        <polyline points="13 2 13 9 20 9"></polyline>
                    </svg>
                    My Meal Plans
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/logout">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-users">
                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                        <circle cx="9" cy="7" r="4"></circle>
                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                    </svg>
                    Log Out
                </a>
            </li>
        </ul>
    </div>
</nav>
<?php echo $content; ?>



<!--PAge Transactions-->
<div class="cd-cover-layer"></div>
<div class="cd-loading-bar"></div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/jquery-2.1.1.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--PAge Transactions-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../theme/js/vendor/jquery-slim.min.js"><\/script>')</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/owl.carousel.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/common.js"></script>


<!--Google Maps-->

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkQB4plPcapLPxaFEJ8ToFpgA-Bz6rOVU&libraries=places"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/mealplan_plugin_edit.js"></script>
<!-- Footer -->
</body>
</html>
