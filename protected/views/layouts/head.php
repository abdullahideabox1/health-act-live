<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../../../favicon.ico">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">


<title><?php echo CHtml::encode($this->meta_title) ?></title>

<!-- Bootstrap core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/default.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/owl.theme.default.min.css" rel="stylesheet">


<!--PAge Transactions-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/modernizr.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/page-transactions.css"> <!-- Resource style -->
<!--PAge Transactions-->

<!--Date Picker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--Date Picker-->
<script type="text/javascript">

    function toogle_div_fun(id){


        var divelement = document.getElementById(id).style.display = 'none';
        ('#el').style.display = 'none';
        ('click').style.display = "block";

        // divelement.style.display = 'none';

    }

    function toogle_div_show(id){


        var divelement = document.getElementById(id).style.display = 'block';
        ('click').style.display = "none";

        // divelement.style.display = 'display';

    }




</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1204928469657870'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1204928469657870&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
