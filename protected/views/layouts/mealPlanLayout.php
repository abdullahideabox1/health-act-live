<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Head -->
    <?php include './protected/views/layouts/head.php'; ?>



    <!-- Head -->
    <style>
        .footer-social{
            display: none;
        }
    </style>
</head>
<body>
<?php include './protected/views/layouts/menu.php'; ?>
<?php echo $content; ?>


<!-- Footer -->
<?php include './protected/views/layouts/footer.php'; ?>



<!-- Footer -->
</body>
</html>
