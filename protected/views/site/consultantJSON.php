<div class="starter-template">


    <div class="col-12 h-100vh w-100 mt-4 p-0" style="background-color: white;">
        <div class="row ">
            <div class="col-md-8 col-lg-8 col-sm-12 float-left mt-5 p-0 " style="background-color:#60B49D ">
                <div class="row">
                    <div class="col-md-3 col-lg-3  mt-5 ml-3 col-sm-12 float-left">
                        <img
                            src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Consultant/<?php echo $consultants_tab['image'] ?>"
                            class="borderRadiusWhite w-75 mb-3">
                    </div>
                    <div class="col-md-8 col-lg-8 col-sm-12 mt-4 p-4 float-right ">


                        <div class="row">
                            <div class="col-10 mt-4">

                                <h4 style="color: white;font-weight: bolder"><u
                                        id="headertext"><?php echo $consultants_tab['name'] ?></u></h4>
                                <?php if ($consultants_tab['description'] != '') { ?>
                                    <p id="ptextchange" class="text-justify"
                                       style="font-weight: normal; color: white; font-size: 15px;">
                                        <?php echo $consultants_tab['description'] ?>
                                    </p>
                                <?php } ?>
                                <?php if ($consultants_tab['description_2'] != '') { ?>
                                    <p id="info" style="color: white;text-align: justify">
                                        <?php echo $consultants_tab['description_2'] ?>
                                    </p>
                                <?php } ?>
                                <p style="color: white;text-align: justify">
                                    <br>
                                </p>


                            </div>
                            <div class="col-2"></div>
                        </div>

                    </div>


                    <div class=" col-lg-12 col-md-12 col-sm-12 p-0" style="align-self: flex-end;">

                        <h4 class="text-left ml-4" style="color: white;font-weight: bolder;">SCHEDULE</h4>

                        <div class="col-md-12 col-lg-12 col-sm-12 ">
                            <div class="table-responsive">
                                <table class="table table-dark" style="background-color: gray;width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>Mon</th>
                                        <th>Tues</th>
                                        <th>Wed</th>
                                        <th>Thurs</th>
                                        <th>Fri</th>
                                        <th>Sat</th>
                                        <th>Sun</th>

                                    </tr>

                                    </thead>

                                    <tbody>
                                    <tr>
                                        <?php

                                        $days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                                        foreach ($days as $key => $day) { ?>

                                            <td>

                                                <?php
                                                if (isset($arr[$day])) {
                                                    foreach ($arr[$day] as $k) {
                                                        echo '<b>';
                                                        echo $k['time'] . " ";
                                                        echo '</b>';
                                                        echo '<br>';
                                                    }
                                                }

                                                ?>


                                            </td>

                                            <?php

                                        }
                                        ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>


                </div>
            </div>


            <div class="col-md-4 col-lg-4 col-sm-12 mt-5 " style="height: 1000px;">


                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-sm-12 col-md-10">
                        <div class="close-tray closurecons">X</div>
                        <form class="font-setting mt-5 pt-3" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/AppointmentsSuccess">
                            <p style="text-align: center; font-weight: bolder; color:#60B49D;font-size: 20px "
                               class="mb-0">
                                REQUEST APPOINTMENT</p>

                            <p class="text-center">Your contact details will remain private.</p>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm"
                                       id="exampleInputName" aria-describedby="emailHelp"
                                       placeholder="Name" name="ConsultantQueries[name]">
                            </div>

                            <div class="form-group">
                                <input type="number" class="form-control form-control-sm"
                                       id="exampleInputPhoneNumber" placeholder="Phone Number" name="ConsultantQueries[phone]">
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control form-control-sm"
                                       id="exampleInputEmailAddress" placeholder="Email Address" name="ConsultantQueries[email]">
                            </div>

                            <div class="form-group">
                                <select class="form-control form-control-sm"
                                        id="exampleInputSourceOfReferal" name="ConsultantQueries[consultant_name]">
                                    <option><?php echo $consultants_tab['name'] ?></option>
                                </select>
                            </div>

                            <div class="form-group">

                                <select class="form-control form-control-sm"
                                        id="exampleInputSourceOfReferal" name="ConsultantQueries[appointment_time]">

                                    <option>Please Select Appointment Time</option>
                                    <?php if ($consultant_schedule_form) {
                                        foreach ($consultant_schedule_form as $item) {
                                            echo '<option>';
                                            echo $item['time'] . '('. $item['days']. ')';
                                            echo '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm d-none"
                                       id="exampleInputDate" placeholder="Source Of Referral" name="ConsultantQueries[source_referral]">
                            </div>

                            <div class="form-group">
                                <input type="date" class="form-control form-control-sm"
                                       id="exampleInputDate" placeholder="DATE" name="ConsultantQueries[date]">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm"
                                       id="exampleInputComment" placeholder="Comment(Optional)" name="ConsultantQueries[comments]">
                            </div>

                            <div class="form-inline ">
                                <button type="submit" class="btn btn-md text-white"
                                        style="background-color:#60B49D;width: 100% ">REQUEST
                                    APPOINTMENT
                                </button>
                            </div>


                        </form>


                    </div>

                    <div class="col-1">


                    </div>

                </div>


            </div>
        </div>


    </div>
</div>