<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<?php if ($items) {
    foreach ($items as $item) {
        $sub_items = Subitem::model()->findByAttributes(array(
            'is_active' => 1,
            'item_id' => $item['id'],
        ));
        $sub_items_options = SubitemOption::model()->findAll('is_active = :is_active AND subitem_id = :subitem_id', array(
            ':is_active' => 1,
            ':subitem_id' => $sub_items['id'],
        ));
        ?>
        <div class="modal-body">
            <input type="hidden" name="" id="item_id" value="<?php echo $item_id ?>" class="form-control"/>

            <h2><?php echo $item['name'] ?></h2>

            <h3>Rs. <?php echo $item['price'] ?></h3>
            <?php if ($item['item_image'] > 0) { ?>
                <img
                    src="<?php echo Yii::app()->request->baseUrl; ?>/upload/ItemImages/<?php echo $item['item_image'] ?>"
                    class="img-fluid"/>
            <?php } else { ?>
                <img
                    src="https://via.placeholder.com/800x250"
                    class="img-fluid"/>
            <?php } ?>
            <hr>
            <h5>DESCRIPTION</h5>

            <p>
                <?php echo $item['description'] ?>
            </p>

            <form>
                <?php if ($sub_items) { ?>
                    <h5>OPTIONS</h5>

                    <h6><?php echo $sub_items['heading'] ?></h6>
                <?php } ?>

                <div class="form-group">


                    <?php if ($sub_items_options) { ?>
                        <select class="form-control" id="get_option_value">
                            <option value="0">Please Select</option>
                            <?php
                            foreach ($sub_items_options as $sub_items_option) {
								
								$select = "";
								if ($sub_items_option['id'] == $option_value_id){
									$select = " selected";	
								}
                                ?>
                                <option value="<?php echo $sub_items_option['option_name'] ?>"
                                        data-option-id="<?php echo $sub_items_option['id'] ?>" <?=$select?>><?php echo $sub_items_option['option_name'] ?></option>
                            <?php } ?>
                        </select>
                    <?php } ?>

                </div>


                <hr>
                <h6>Quantity</h6>
                <input type="text" name="" id="quantity" value="<?= isset($product_quantity)? $product_quantity : 1?>" class="form-control"/>
                <br>

                <div class="form-group">
                    <label>Delivery Time</label>
                    <select class="form-control" id="get_delivery_time">
                        <option value="0">Please Select Delivery Time</option>
                        <?php
                        $sloat = Yii::app()->params['deleiverTime'][$calender_type];
						
						$sloats =  Yii::app()->functions->getTimeSlot($sloat[0],$sloat[1]);
						
						
						foreach ($sloats as $sloat){
							
							$time = date("h:i A",strtotime($sloat));
							
							$select = "";
							
							if ($time == $delivery_time){
								$select = " selected";	
							}
						
						?>
                       		<option value="<?=$time?>" <?=$select;?>><?=$time?></option>
                        <?php
                        
						}
						?>
                    </select>
                </div>
            </form>
        </div>
    <?php }
} ?>
<div class="modal-footer">
    <a class="btn btn-default" data-dismiss="modal">Close</a>
    <a class="btn btn-success" id="saveModalDetail">Save</a>
</div>
