<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">


            <div class="col-12 h-100vh pt-6 p-0">


                <div class="col-lg-6 col-md-6 col-sm-12 h-100 p-0 bg-white float-left">

                    <div class="contactdisplayer">
                        <!--                    <div class="contactdisplayer">-->
                        <h1 class="mt-5 ml-3" style="color: #60B49D;font-weight: bolder">GET IN TOUCH</h1>

                        <form class="mt-3" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/ContactSuccess">


                            <div class="form-group col-lg-10 ">
                                <label for="exampleFormControlInput1">Your Full Name*:</label>
                                <input type="text" class="form-control set" id="exampleFormControlInput1"
                                name="Contact[full_name]">
                            </div>
                            <div class="form-group col-lg-10 bor">
                                <label for="exampleFormControlInput1">Email address*:</label>
                                <input type="text" class="form-control" id="exampleFormControlInput2"
                                       name="Contact[email_address]">
                            </div>

                            <div class="form-group col-lg-10 bor">
                                <label for="exampleFormControlTextarea1">Message*:</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"
                                          name="Contact[message]"
                                ></textarea>

                                <div class="col-lg-4 col-md-4 col-sm-3 p-0 mt-2">
                                    <button type="submit" class="btn btn-default float-left "
                                            style="background-color: #60B49D; color: white">Submit
                                    </button>
                                </div>
                                <p class="float-right">Required Fields</p>
                            </div>

                        </form>
                        <!--                    </div>-->

                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 h-100vh pt-0 float-right" style="background-color:  #60B49D">


                    <div class="row">
                        <!--                            <div class="col-md-12 col-lg-12 bg-danger pt-1">-->
                        <div id="map-container" class="col-md-12 col-lg-12 p-0 bg-dark " style="height: 350px;"></div>
                        <!--                            </div>-->
                    </div>


                    <div class="element1 p-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-6 ">
                                    <p style="color: white" class="center">

                                    <p style="font-size: 15px;color: white;"><span class="text-white"
                                                                                   style="font-size: 18px;">EMAIL</span>
                                        <br>info@thehealthact.com</p>

                                    <p style="color: white;font-size: 15px" class="center"><span
                                            style="font-size: 18px"> ADDRESS <img
                                                src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/locater.png"></span>
                                        <br>
                                        4th Floor, Building No. 25 C,
                                        10th Street off Kh-e-Shamsheer,
                                        Phase 5, DHA,
                                        Karachi
                                        Pakistan
                                    </p>


                                </div>
                                <div class="col-6">
                                    <p style="color: white" class="center">

                                    <p style="color:white;font-size: 15px;"><span style="font-size: 18px">PHONE</span>
                                        <br> (021) 35853168 <br>(021) 35852169 <br> (021) 35852170 </p>

                                    <div class="text-center d-none">
                                        <p style="font-size: 18px; color: #fff;" class="mb-0">SOCIAL MEDIA</p>
                                        <a class="btn btn-light btn-circle color-green"
                                           href="https://www.facebook.com/thehealthact" target="_blank">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a class="btn btn-light btn-circle color-green"
                                           href="https://www.instagram.com/thehealthact/" target="_blank">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </div>


        </div>
    </div>
</main>