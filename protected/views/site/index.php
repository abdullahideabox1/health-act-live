<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template cls">


            <header>
                <div class="home">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php if ($images) {
                                $counter = 0;
                                foreach ($images as $image) {
                                    if ($counter == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li data-target="#carouselExampleIndicators"
                                        data-slide-to="<?php echo $counter ?>" class="<?php echo $active ?>"></li>
                                    <?php $counter++;
                                }
                            } ?>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <?php if ($images) {
                                $counter = 0;
                                foreach ($images as $image) {
                                    if ($counter == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <div class="carousel-item <?php echo $active ?>">
                                        <div
                                            style="background-image: url('/upload/Images/<?php echo $image['image'] ?>')"
                                            class="w-100 img-fluid img"></div>
                                        <div class="carousel-caption d-none d-md-block">

                                            <!--                            <h3>First Slide</h3>-->
                                            <!--                            <p>This is a description for the first slide.</p>-->
                                        </div>
                                    </div>
                                    <?php $counter++;
                                }

                            } ?>


                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/left-white.svg">
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/right-green.svg">
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </header>

            <!-- Page Content -->
            <section class="py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 text-justify align-content-center justify-content-center "
                             style="height: 150px">
                            <div class=" home ml-md-2 ml-md-2 ml-md-2">
                                <h1 class="color-green font-weight-bold">ABOUT</h1>

                                <h1 class="font-weight-bold" style="color: #5a6268">THE HEALTH ACT</h1>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 " style="height: 150px; color:#5a6268 ">
                            <p class="text-justify">
                                Help your mind achieve the peace it desires, your body the strength it deserves, and
                                your stomach the palatability it craves.<br>
                                At Health Act, we offer an inclusive platform for your mental and physical wellbeing,
                                whilst satiating your gastronomic needs with a calorie-check.

                            </p>

                        </div>
                        <div class="col-md-4 col-lg-4" style="height: 150px;  color:#5a6268">
                            <p class="text-justify">
                                Our team of expert fitness trainers, nutritionists and therapists are on hand to help
                                you with everything from meeting short-term weight loss goals, to transitioning to a
                                new, happier and more health-conscious, lifestyle.
                                <br>
                                Are you ready to get your Health Act together?

                            </p>

                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
</main>