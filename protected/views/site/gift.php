<main role="main">

    <div class="cd-index cd-main-content">
        <!--    Main screen-->


        <div class="starter-template">
            <div class="col-12 h-100vh p-0">
                <div class="col-lg-6 col-md-6 col-sm-12 mt-5 p-0 float-left" style="height: 600px">


                    <div class="col-lg-12 col-md-12 col-sm-12 p-0">

                        <div id="carouselExampleControls" class="carousel slide mt-4" data-ride="carousel">
                            <div class="carousel-inner p-0">
                                <?php if ($images) {
                                    foreach ($images as $image) {
                                        ?>
                                        <div class="carousel-item active">
                                            <img class="w-100"
                                                 src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Images/<?php echo $image['image'] ?>"
                                                 alt="First slide">
                                        </div>
                                        <?php
                                    }

                                } ?>
                            </div>
                        </div>


                    </div>


                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mt-5 float-right" style="height: 800px">


                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 bg-white mt-lg-5 mt-md-5 lg-pt-5 mt-md-5 ">

                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png"
                                 class="right-img-grid">


                            <div class="col-md-10 col-lg-10 col-sm-12 ">


                                <h1 class="text-left color-green pt-5 mt-5" style="font-weight: bolder;font-size: 40px">
                                    OUR
                                    GIFTS</h1>

                                <p class="mt-lg-2 mt-md-2 text-left" style="color: #4e555b">
                                    Wrap happiness and health in a box for your loved ones. Choose a healthy meal plan
                                    for friends wanting to lose a few inches, a delicious but low-calorie meal for that
                                    sibling who is a foodie, a box of nutritious cookies for your child with a sweet
                                    tooth, or a box of wholesome chocolates for your partner to show how much you care.
                                </p>


                                <h4 class="font-weight-bold" style="color: #4e555b">YOU CAN GIFT:</h4>

                            </div>


                            <div class="col-md-2 col-lg-2 col-sm-12"></div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">

                            <div class="row">

                                <div class="col-md-12 mt-0  col-lg-12 col-sm-12 ">

                                    <div class="consultation gifts">

                                        <ul class="a nav nav-tabs responsive-tabs font-weight-bold" id="pills-tab"
                                            role="tablist">
                                            <?php if ($tabs) {
                                                $counter = 0;
                                                foreach ($tabs as $tab) {
                                                    if ($counter == 0) {
                                                        $active = 'active';
                                                    } else {
                                                        $active = '';
                                                    }
                                                    ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo $active ?>"
                                                           id="pills-<?php echo $tab['id'] ?>-tab"
                                                           data-toggle="pill"
                                                           href="#<?php echo $tab['id'] ?>"
                                                           role="tab" aria-controls="pills-<?php echo $tab['id'] ?>"
                                                           aria-selected="true"><?php echo $tab['title'] ?></a>
                                                    </li>
                                                    <?php $counter++;
                                                }
                                            } ?>

                                        </ul>
                                    </div>


                                </div>


                            </div>


                            <div class="col-md-10 col-sm-10 ">


                                <div class="tab-content" id="pills-tabContent">
                                    <?php if ($tabs) {
                                        $counter = 0;
                                        foreach ($tabs as $tab) {
                                            if ($counter == 0) {
                                                $active = 'show active';
                                            } else {
                                                $active = '';
                                            }
                                            ?>
                                            <div class="tab-pane fade <?php echo $active ?>"
                                                 id="<?php echo $tab['id'] ?>" role="tabpanel"
                                                 aria-labelledby="pills-home-tab">

                                                <div class="col-10">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <p class="text-justify"
                                                                   style="color: #4e555b; font-size: 13px">
                                                                    <?php echo $tab['description'] ?>
                                                                </p>
                                                            </div>
                                                            <div class="col-12 ">
                                                                <button class="btn buttonsettergifts mt-1 giftchatopen"
                                                                        data-tab-name="<?php echo $tab['title'] ?>">GIFT
                                                                    THIS!
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <?php $counter++;
                                        }
                                    } ?>

                                </div>

                            </div>


                        </div>

                    </div>


                </div>


                <!--            <img src="img/home-right-img.svg" width="100px" class="bottom-img-gifts">-->


            </div>


        </div>


        <!--    gift chat-->


        <div class="giftchat col-sm-3"
             style="display: none;background-color: white;box-shadow: 0px 2px 4px #888888;height: 1800px;">
            <div class="col-12">
                <div class="starter-template">

                    <div class="h-100vh">
                        <div class="col-md-12 col-sm-12 col-lg-12 h-100vh pt-5 mt-5 ml-2">
                            <div class="col-md-12 col-lg-12 col-sm-12 p-3 float-left">
                                <form class="formset" method="post"
                                      action="<?php echo Yii::app()->request->baseUrl; ?>/giftSuccess">
                                    <label href="#" class="closure float-right"
                                           style="text-decoration: none;color: #5a6268">X</label>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Your Full Name*:</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"
                                               aria-describedby="emailHelp" name="Gift[full_name]">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Your Phone Number*:</label>
                                        <input type="text" class="form-control" id="exampleInputPassword1"
                                               name="Gift[phone_number]"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Reciever's Address*:</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"
                                               aria-describedby="emailHelp"
                                               name="Gift[reciever_address]"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Reciever's Contact*:</label>
                                        <input type="text" class="form-control" id="exampleInputPassword1"
                                               name="Gift[reciever_contact]"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="gifted_items"
                                               name="Gift[items]"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleTextarea">Message*:</label>
                                        <textarea class="form-control" id="exampleTextarea" rows="4"
                                                  name="Gift[message]"></textarea>
                                    </div>

                                    <div class="form-inline ">
                                        <div class="col-12 p-0">
                                            <div class="col-6 p-0 float-left">
                                                <button type="submit" class="btn btn-sm "
                                                        style="background-color:#60B49D ">
                                                    SUBMIT
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                            </div>


                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

</main>



