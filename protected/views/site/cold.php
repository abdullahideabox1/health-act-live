<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">


            <div class="col-12 h-100vh pt-5 p-0">
                <div class="col-lg-6 col-md-6 col-sm-12 h-100 mt-4 p-0 bg-white float-left">


                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner p-0">
                            <?php if ($images) {
                                $counter = 0;
                                foreach ($images as $image) {
                                    if ($counter == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <div class="carousel-item <?php echo $active ?>">
                                        <img class="w-100"
                                             src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Images/<?php echo $image['image'] ?>"
                                             alt="First slide">
                                    </div>
                                    <?php $counter++;
                                }

                            } ?>
                            <div class="carousel-item">
                                <img class="w-100"
                                     src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/coldnewtwo.png"
                                     alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="w-100"
                                     src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/coldnewthree.png"
                                     alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                           data-slide="prev">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/left-white.svg">
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                           data-slide="next">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/right-green.svg">
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <!--                    <img src="theme/sidders.png" class="img-sidders w-25">-->


                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  pt-lg-4 pt-md-4 mt-lg-4  mt-md-4 float-right">

                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png"
                         class="right-img-grid mt-5 pt-4">

                    <div class="container">
                        <div class="row align-content-center justify-content-center pt-md-3 pt-lg-3 mt-lg-3 mt-md-3">

                            <div
                                class="col-md-8 col-lg-8 justify-content-center align-content-center ">

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 juices">
                                        <h1 class="text-left color-green mb-0" style="font-weight: bolder">COLD-PRESSED
                                            JUICES</h1>

                                        <p class="mt-1 text-justify">
                                            Contrary to conventional methods of juice extraction, our fruits and
                                            vegetables are cold pressed in a state-of-the-art hydraulic press, which
                                            enables the juice to conserve its raw goodness and nutrition to the maximum.
                                        </p>

                                        <p class="mt-1 text-justify">
                                            Head on over to our menu to choose from three delicious, healthy blends that include:
                                            The Wingman (Apple, Orange, Cucumber, Celery, Kale, Ginger Root, Lemon) <br>

                                            Turmeric Sunrise (Apple, Pear, Carrot, Celery, Ginger Root, Lemon, Turmeric Root)<br>

                                            Roots (Apple, Beetroot, Carrot, Ginger Root, Lemon)<br>

                                            Mint Lemonade<br>

                                            Apple Lemon Mint

                                        </p>

                                    </div>

                                </div>
                                <!--                                </div>-->

                                <div class="col-md-8 col-lg-8 col-sm-8  mt-3 ">
                                    <a href="http://thehealthact.com/healthbox/#p7" class="btn buttonsetter justify-content-center align-content-center"
                                            style="background-color: white">ORDER NOW
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


            </div>
        </div>


    </div>
    </div>
</main>
