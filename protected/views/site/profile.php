<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-5 pt-5">
    <br>

    <h2>Profile</h2>

    <div class="table-responsive">
        <form action="" method="post">

            <div class="form-group">
                <label>Full Name</label>
                <input type="text" class="form-control" placeholder="Full Name" value="<?php echo $profile->username ?>"
                       name="Customer[username]">
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="text" class="form-control" placeholder="Email Address"
                       value="<?php echo $profile->email ?>" name="Customer[email]">
            </div>
            <div class="form-group">
                <label>Mobile Number</label>
                <input type="text" class="form-control" placeholder="Mobile Number"
                       value="<?php echo $profile->mobile ?>" name="Customer[mobile]">
            </div>
            <div class="form-group">
                <label>Shipping Address</label>
                <input type="text" class="form-control" placeholder="Shipping"
                       value="<?php echo $profile->address ?>" name="Customer[address]">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" placeholder="Password"
                       value="" name="Customer[password]">
            </div>
            <button class="btn btn-primary float-right" type="submit">Update</button>

        </form>
    </div>
</main>