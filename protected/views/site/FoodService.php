<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="col-12 h-100vh  p-0">
                <div class="col-lg-6 col-md-6 col-sm-12 mt-5 p-0 float-left" style="height: 600px">


                    <div class="col-lg-12 col-md-12 col-sm-12 p-0">

                        <div id="carouselExampleControls" class="carousel slide mt-4" data-ride="carousel">
                            <div class="carousel-inner p-0">
                                <?php if ($images) {
                                    $counter = 0;
                                    foreach ($images as $image) {
                                        if ($counter == 0) {
                                            $active = 'active';
                                        } else {
                                            $active = '';
                                        }
                                        ?>
                                        <div class="carousel-item <?php echo $active ?>">
                                            <img class="w-100"
                                                 src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Images/<?php echo $image['image'] ?>"
                                                 alt="First slide">
                                        </div>
                                        <?php $counter++;
                                    }

                                } ?>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                               data-slide="prev">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/left-white.svg">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                               data-slide="next">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/right-green.svg">
                                <span class="sr-only">Next</span>
                            </a>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png"
                                 class="img-sidders mb-3 w-25">
                        </div>


                    </div>


                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mt-5 float-right" style="height: 800px">


                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 bg-white mt-lg-5 mt-md-5 lg-pt-5 mt-md-5 ">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png"
                                 class="right-img-grid">

                            <div class="col-md-10 col-lg-10 col-sm-12 mt-5 pt-3">


                                <h1 class="text-left color-green pt-4 mt-lg-4"
                                    style="font-weight: bolder;font-size: 40px">
                                    FOOD SERVICES</h1>

                                <p class="mt-lg-2 mt-md-2 text-left" style="color: #4e555b; font-size: 14px">
                                    What’s on the menu? Health Act offers a catering menu with delicious, nutrient-dense
                                    meals for all your events.
                                </p>

                            </div>


                            <div class="col-md-2 col-lg-2 col-sm-12"></div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12" style="">

                            <div class="row">

                                <div class="col-md-12 mt-2  col-lg-12 col-sm-12 ">


                                    <div class="consultation foodservices">
                                        <ul class="a nav nav-tabs responsive-tabs font-weight-bold " id="pills-tab"
                                            role="tablist">
                                            <?php if ($tabs) {
                                                $counter = 0;
                                                foreach ($tabs as $tab) {
                                                    if ($counter == 0) {
                                                        $active = 'active';
                                                    } else {
                                                        $active = '';
                                                    }
                                                    ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo $active ?>"
                                                           id="pills-<?php echo $tab['id'] ?>-tab"
                                                           data-toggle="pill"
                                                           href="#<?php echo $tab['id'] ?>"
                                                           role="tab" aria-controls="pills-<?php echo $tab['id'] ?>"
                                                           aria-selected="true"><?php echo $tab['title'] ?></a>
                                                    </li>
                                                    <?php $counter++;
                                                }
                                            } ?>
                                        </ul>
                                    </div>


                                </div>


                            </div>


                            <div class="col-md-12 col-sm-12 ">

                                <div class="tab-content" id="pills-tabContent">
                                    <?php if ($tabs) {
                                        $counter = 0;
                                        foreach ($tabs as $tab) {
                                            if ($counter == 0) {
                                                $active = 'show active';
                                            } else {
                                                $active = '';
                                            }
                                            ?>
                                            <div class="tab-pane fade <?php echo $active ?>"
                                                 id="<?php echo $tab['id'] ?>" role="tabpanel"
                                                 aria-labelledby="pills-<?php echo $tab['id'] ?>-tab">
                                                <div class="col-lg-10 col-md-10 col-sm-12">

                                                    <p class="text-justify pt-3" style="color: #4e555b">
                                                        <?php echo $tab['description'] ?>
                                                    </p>

                                                </div>

                                                <div class="container">
                                                    <div class="row">
                                                        <button
                                                            class="btn buttonsetterfoodservice mt-md-5 mt-lg-5  mt-sm-5 giftchatopen"
                                                            id="viewChat" data-tab-name="<?php echo $tab['title'] ?>">
                                                            BOOK NOW
                                                        </button>
                                                        <div class="col-md-3">

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>


                                            <?php $counter++;
                                        }
                                    } ?>


                                </div>


                            </div>




                        <span style="font-weight: bold; color: grey" href="#" class="d-none"><img
                                src="<?php echo Yii::app()->request->baseUrl; ?>/theme/chat.png" width="80px"
                                class="bottom-img-grid"></span>


                        </div>


                    </div>
                </div>

            </div>
        </div>
        <div class="giftchat col-sm-3 float-right h-100vh"
             style="display: none;background-color: white;box-shadow: 0px 2px 4px #888888;height: 1500px;">

            <div class="starter-template chat bg-white">


                <div class="h-100vh">
                    <div class="h-100vh w-100 bg-white">
                        <div class="home">
                            <div class="col-12 h-100vh mt-5">


                                <form action="<?php echo Yii::app()->request->baseUrl; ?>/foodServicesSuccess"
                                      method="post">


                                    <div class="formchat pt-5">
                                        <div class="form-group col-lg-12">
                                            <a href="#" class="text-center closeChat right-img-grid"
                                               style="font-weight: normal; color: grey">
                                                X
                                            </a>

                                            <p class="color-green mt-5">CHAT WITH US!</p>
                                            <label for="exampleFormControlInput1">INTRODUCE YOURSELF*:</label>
                                            <input type="text" class="form-control set" id="exampleFormControlInput1"
                                                   placeholder="Name,Email" name="FoodService[name]">
                                            <small>or sign in with</small>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label for="exampleFormControlInput1">PHONE NUMBER*:</label>
                                            <input type="text" class="form-control" id="exampleFormControlInput2"
                                                   name="FoodService[phone]">
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <label for="exampleFormControlTextarea1">MESSAGE*:</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1"
                                                  rows="6" name="FoodService[message]"></textarea>

                                            <div
                                                class="col-lg-12 col-md-12 col-sm-12 p-0 mt-3  text-center align-content-center justify-content-center">
                                                <button type="submit" class="btn btn-default "
                                                        style="background-color: #60B49D; color: white">Chat Away
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>


                                <!--                    <div class="col-lg-1 col-md-1 col-sm-0 mt-5"></div>-->
                                <!--                    </div>-->
                                <!--                    </div>-->


                            </div>
                        </div>

                    </div>

                </div>


            </div>


        </div>
    </div>
</main>
