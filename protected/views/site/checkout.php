<style>
    body {
        overflow: auto;
    }
</style>
<form action="" method="post">
    <main role="main">
        <div class="cd-index cd-main-content">
            <div class="starter-template">
                <div class="row featurette pt-5 mt-5 w-100 m-0 col-md-8 float-left">
                    <?php
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="alert alert-danger text-center w-100">' . $message . "</div>\n";
                    }
                    ?>
                    <?php if (isset($_SESSION['userID']) != null) {
                        $customer = Customer::model()->findByPk($_SESSION['userID']);
                        ?>
                        <div class="login col-12 mb-5">
                            <h3>Already login</h3>
                            <h6>Your Details are</h6>

                            <div class="form-group">
                                <label>Email address:</label>
                                <input type="email" class="form-control" name="Customers[email]"
                                       value="<?php echo $customer['email'] ?>">
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" class="form-control" name="Customers[password]"
                                       value="<?php echo $customer['password'] ?>">
                            </div>

                            <?php echo CHtml::submitButton('Order Now', array('class' => 'btn btn-primary', 'id' => 'loginUser', 'name' => 'loginUser')); ?>
                        </div>
                    <?php } else { ?>
                        <div class="login col-12 mb-5">
                            <h3>Existing User? Sign In Here</h3>

                            <div class="form-group">
                                <label>Email address:</label>
                                <input type="email" class="form-control" name="Customers[email]">
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" class="form-control" name="Customers[password]">
                            </div>
                            <div class="form-group">
                                <label>
                                    <a  data-toggle="modal" data-target="#forgotpassword">Forgotten
                                        Password?</a>
                                </label>
                            </div>

                            <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-primary', 'id' => 'loginUser', 'name' => 'loginUser')); ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($_SESSION['userID']) == null) { ?>
                        <div class="Register col-12 mt-5 mb-5">
                            <h3>New User? Place Order Here</h3>
                            <h5>Personal Details</h5>

                            <div class="form-group">
                                <label>Full Name: <span style="color: red">*</span></label>
                                <input type="text" name="Customer[username]" value="" placeholder="Full Name"
                                       class="form-control" id="Customer_Username" data-validation="required"
                                       data-validation-error-msg="Full Name is required">
                            </div>
                            <div class="form-group">
                                <label>Email Address: <span style="color: red">*</span></label>
                                <input type="email" name="Customer[email]" value="" placeholder="Email Address"
                                       class="form-control" id="Customer_email" data-validation="required"
                                       data-validation-error-msg="Email is required">
                            </div>
                            <div class="form-group">
                                <label>Mobile Number: <span style="color: red">*</span></label>
                                <input type="number" name="Customer[mobile]" value="" placeholder="Mobile Number"
                                       class="form-control" id="Customer_mobile" data-validation="required"
                                       data-validation-error-msg="Mobile Number is required">
                            </div>
                            <div class="form-group">
                                <label>Address: <span style="color: red">*</span></label>
                                <input type="text" name="Customer[address]" value="" placeholder="Address"
                                       class="form-control" id="Customer_address" data-validation="required"
                                       data-validation-error-msg="Address is required">
                            </div>


                            <?php echo CHtml::submitButton('Place Order', array('class' => 'btn btn-primary', 'id' => 'RegisterUser', 'name' => 'RegisterUser')); ?>

                        </div>
                    <?php } ?>
                </div>

                <div class="row featurette pt-5 mt-5 w-100 m-0 col-md-4">
                    <div class="cart_section col-12 mb-5">
                        <?php
                        if ($userMenuID) {
                            $customerMenu = UserMenu::model()->findByPk($userMenuID);
                            ?>

                            <h4>Your Order</h4>
                            <h6>Delivery Fee: <span class="float-right">0</span></h6>
                            <hr>
                            <h4>Total: <span class="float-right"><?php echo $customerMenu['total_amount'] ?></span></h4>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</form>
<!-- Modal -->
<div id="forgotpassword" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form class="form-signin p-5 " method="post">
                    <div >
                        <?php
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            echo '<div class="alert alert-danger text-center w-100">' . $message . "</div>\n";
                        }
                        ?>
                        <h1 class="h3 mb-3 font-weight-normal text-center">Forgot Password?</h1>
                        <p class="text-center">Just provide us your email so we can send you the new password.</p>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" name="forgotpassword[email]" class="form-control mb-3" placeholder="Email address" required
                               autofocus>

                        <div style="margin: 0 auto" class="text-center">

                            <button type="submit" class="btn btn-primary" id="forgotpassword_btn" name="forgotpassword_btn">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
