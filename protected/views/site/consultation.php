<main role="main" class="consultation">
    <div class="cd-index cd-main-content">
        <div class="starter-template pt-3">
            <div class="col-12 h-100vh p-0">


                <div class="col-lg-8 col-md-8 col-sm-12 mt-5 p-4 float-left pt-5">

                    <!--                <div class="container">-->
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 consultation ml-3 ">
                            <h1 class="text-justify font-weight-bold" style="color: #60B49D">CONSULTATION</h1>

                            <p style="text-align:justify;color: grey">Let our team of experts guide you on your path to
                                a
                                healthier mind, body and soul. We offer counseling sessions for individuals as well as
                                for
                                couples, families and groups.
                                In addition, as a health and fitness provider, we have specialists dedicated to
                                supporting
                                you with your diet and nutritional needs.

                            </p>
                        </div>
                    </div>
                    <!--                </div>-->

                    <div class="container-">
                        <div class="row">

                            <div class="col-md-12 col-lg-12 mt-lg-4 mt-md-4 col-sm-12 ">


                                <div class="consultation one">
                                    <ul class="nav nav-tabs responsive-tabs font-weight-bold " id="pills-tab"
                                        role="tablist">
                                        <?php if ($tabs) {
                                            $counter = 0;
                                            foreach ($tabs as $tab) {
                                                if ($counter == 0) {
                                                    $active = 'active';
                                                } else {
                                                    $active = '';
                                                }
                                                ?>
                                                <li class="nav-item">
                                                    <a class="nav-link <?php echo $active ?>"
                                                       id="pills-<?php echo $tab['id'] ?>-tab" data-toggle="pill"
                                                       href="#<?php echo $tab['id'] ?>"
                                                       role="tab" aria-controls="pills-<?php echo $tab['id'] ?>"
                                                       aria-selected="true"><?php echo $tab['title'] ?></a>
                                                </li>
                                                <?php $counter++;
                                            }
                                        } ?>
                                    </ul>
                                </div>


                            </div>


                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12 ">

                        <div class="tab-content" id="pills-tabContent">

                            <?php if ($tabs) {
                                $counter = 0;
                                foreach ($tabs as $tab) {
                                    if ($counter == 0) {
                                        $active = 'show active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>

                                    <div class="tab-pane fade <?php echo $active ?>" id="<?php echo $tab['id'] ?>"
                                         role="tabpanel"
                                         aria-labelledby="pills-<?php echo $tab['id'] ?>-tab">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <p class="text-justify mt-3" style="color: #4e555b; font-size: 13px">
                                                <?php echo $tab['description'] ?>
                                            </p>
                                        </div>
                                        <div class="container">
                                            <div class="row">

                                                <?php if ($consultants_tab) {
                                                    foreach ($consultants_tab as $consultanttab) {
                                                        if ($consultanttab['tab_id'] == $tab['id']) {
                                                            $consultants = Consultant::model()->findAllByAttributes(array(
                                                                'id' => $consultanttab['consultant_id']
                                                            ));
                                                            foreach ($consultants as $consultant) {

                                                                ?>


                                                                <div class="col-md-2 p-0 col-sm-6"
                                                                     id="consultation_profile"
                                                                     data-consultant-id="<?php echo $consultant['id'] ?>">
                                            <span id="viewProfile" href="#">
                                                        <img class="circle w-75"
                                                             src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Consultant/<?php echo $consultant['image'] ?>"
                                                             style="border-radius: 100%; border: 2px solid #60B49D;display: block;margin-left: auto;margin-right: auto; ">
                                            </span>

                                                                    <div class="pt-1">
                                                                        <h6 class="color-green text-uppercase"
                                                                            style="text-align: center">
                                                                            <?php echo $consultant['name'] ?>
                                                                        </h6>


                                                                    </div>
                                                                </div>
                                                            <?php }
                                                        }
                                                    }
                                                } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php $counter++;
                                }
                            } ?>
                            <p class="text-justify mt-3" style="color: red; font-size: 13px">
                              NOTE:  Request your appointment by clicking on your consultant picture
                            </p>
                        </div>
                    </div>


                </div>


                <div class="col-lg-4 col-md-4 h-100vh p-5 float-right placerr">
                    <?php if ($images) {
                        foreach ($images as $image) {
                            ?>
                            <img
                                src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Images/<?php echo $image['image'] ?>"
                                class="img-fluid"/>
                            <?php
                        }

                    } ?>
                </div>

            </div>
        </div>
        <div id="Consultant_data" class="profile h-100vh  col-lg-10 col-md-10 col-sm-12"
             style="display: none; height: 1000px; box-shadow: 0px 2px 4px #888888;">
        </div>
    </div>


</main>


