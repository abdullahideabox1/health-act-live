<!-- Begin page content -->
<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="col-12 h-100vh pt-5 p-0">
                <div class="col-lg-6 col-md-6 col-sm-12 h-100 mt-4 p-0 bg-white float-left">


                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner p-0">
                            <?php if ($images) {
                                $counter = 0;
                                foreach ($images as $image) {
                                    if ($counter == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <div class="carousel-item <?php echo $active ?>">
                                        <img class="w-100"
                                             src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Images/<?php echo $image['image'] ?>"
                                             alt="First slide">
                                    </div>
                                    <?php $counter++;
                                }
                            } ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                           data-slide="prev">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/left-white.svg">
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                           data-slide="next">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/right-green.svg">
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png" class="img-sidders w-25">
                </div>


                <div class="col-lg-6 col-md-6 col-sm-12 p-0 float-right">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 " style="height: 200px">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme/img/sidders.png"
                                     class="right-img-grid mt-md-5 mt-lg-5">
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12" style="height: 400px">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-2 "></div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 about ">

                                            <h1 class="text-left color-green align-content-center justify-content-center"
                                                style="font-weight: bolder">ABOUT US</h1>

                                            <p class="mt-2 pt-2 text-justify">The Health Act is here to help your mind
                                                achieve the peace it desires, your body the strength it deserves and
                                                your stomach the palatability it craves.
                                                Catering to all your needs, embark on your Health Act now to trade your
                                                lifestyle for a happier and healthier one.
                                                At the Health Act, we offer an inclusive platform for your mental and
                                                physical well-being, whilst satiating your gastronomic needs with a
                                                calories-check.

                                            </p>

                                            <p class="mt-3 pt-2 text-justify"> To deliver the finest quality of
                                                lifestyle transformation in Karachi our team comprised of expert fitness
                                                trainers and nutritionists.
                                                Our meal delivery menu, specifically designed to serve the dietary
                                                demands of our clientele, makes your journey towards a clean and healthy
                                                regime easier.
                                            </p>


                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script>


</script>

<script>
    $(document).ready(function () {
        $("#about").css({color: "#60B49D"});
    })
</script>