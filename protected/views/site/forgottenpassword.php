<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }

    .form-signin .checkbox {
        font-weight: 400;
    }

    .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>
<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="row">
                <form class="form-signin mt-5" method="post">
                    <div class="mt-5 pt-5">
                        <?php
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            echo '<div class="alert alert-danger text-center w-100">' . $message . "</div>\n";
                        }
                        ?>
                        <h1 class="h3 mb-3 font-weight-normal text-center">Forgot Password?</h1>
                        <p class="text-center">Just provide us your email so we can send you the new password.</p>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" name="forgotpassword[email]" class="form-control mb-3" placeholder="Email address" required
                               autofocus>

                        <div style="margin: 0 auto" class="text-center">
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
