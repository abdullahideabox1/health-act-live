<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="row">
                <div class="col-lg-12 pt-6 p-0 ">
                    <?php

                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="alert alert-success text-center " id="success_message">' . $message . "</div>\n";
                    }
                    ?>

                    <div class="col-lg-6 col-md-6 col-sm-12 p-0 float-left">

                        <div class="pt-4 mt-4">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2 col-sm-1"></div>
                                    <div class="col-md-10 col-lg-10 col-sm-12">

                                        <h1 style="color: #60B49D;font-weight: bolder;">MEAL PLANS</h1>

                                        <p class=" text-justify" style="color: #4e555b">
                                            What’s a better way to help your loved ones embark on the journey to a
                                            healthy lifestyle than gifting our specialized meal plans ideal to balance
                                            their love for food and their struggle for fitness.
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>

            </div>
        </div>

        <div class="row">


            <div class="col-12 p-0 h-100">


                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 pt-2 mt-2">


                        <div class="row">
                            <div class="col-lg-1 col-sm-1"></div>
                            <div class="col-lg-10 col-sm-10" style="height: 200px">
                                <div class="container-">
                                    <div class="row">

                                        <div class="col-md-12 col-lg-12 mt-lg-0 mt-md-4 col-sm-12 ">


                                            <div class="consultation one">
                                                <ul class="nav nav-tabs responsive-tabs font-weight-bold "
                                                    id="pills-tab"
                                                    role="tablist">
                                                    <?php if ($tabs) {
                                                        $counter = 0;
                                                        foreach ($tabs as $tab) {
                                                            if ($counter == 0) {
                                                                $active = 'active';
                                                            } else {
                                                                $active = '';
                                                            }
                                                            ?>
                                                            <li class="nav-item">
                                                                <a class="nav-link  <?php echo $active ?>"
                                                                   id="pills-<?php echo $tab['id'] ?>-tab"
                                                                   data-toggle="pill"
                                                                   href="#<?php echo $tab['id'] ?>"
                                                                   role="tab"
                                                                   aria-controls="pills-<?php echo $tab['id'] ?>"
                                                                   aria-selected="true"><?php echo $tab['title'] ?></a>
                                                            </li>
                                                            <?php $counter++;
                                                        }
                                                    } ?>
                                                    <li class="nav-item position-absolute" style="right: 0">
                                                        <a class="nav-link"
                                                           href="<?php echo Yii::app()->request->baseUrl; ?>/mealplan">+
                                                            CUSTOMIZE YOUR MEAL PLAN</a>
                                                    </li>
                                                </ul>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-12 p-0">

                                    <div class="tab-content" id="pills-tabContent">
                                        <?php if ($tabs) {
                                            $counter = 0;
                                            foreach ($tabs as $tab) {
                                                if ($counter == 0) {
                                                    $active = 'show active';
                                                } else {
                                                    $active = '';
                                                }
                                                $mealplans = Mealplans::model()->findAllByAttributes(array(
                                                    "tab_id" => $tab['id'],
                                                ));
                                                ?>
                                                <div class="tab-pane fade <?php echo $active ?>"
                                                     id="<?php echo $tab['id'] ?>" role="tabpanel"
                                                     aria-labelledby="pills-<?php echo $tab['id'] ?>-tab">
                                                    <div class="container">
                                                        <div class="owl-carousel studioCarousel">
                                                            <?php if ($mealplans) {
                                                                foreach ($mealplans as $items) {

                                                                    ?>
                                                                    <div class="item">
                                                                        <div class="col-md-12 text-center"
                                                                             id="mealPlans_tray"
                                                                             data-id="<?php echo $items['id'] ?>">
                                                                            <a href="javascript:void(0)">
                                                                                <img class="d-block img-circle w-75"
                                                                                     src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Mealplans/<?php echo $items['image'] ?>"
                                                                                     alt="<?php echo $items['title'] ?>">
                                                                            </a>
                                                                            <h5 class="text-center color-green">
                                                                                <?php echo $items['title'] ?></h5>

                                                                            <div class="container mealcolor">
                                                                                <p class="font-11">
                                                                                    <?php echo $items['sub_title'] ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                            } else { ?>
                                                                No Data!
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $counter++;
                                            }
                                        } ?>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class=" col-lg-1 col-sm-1 bg-white" style="height: 200px"></div>
                    </div>
                </div>


            </div>
        </div>
        <div id="mealPlans_tray_data" class="profile h-100vh  col-lg-10 col-md-10 col-sm-12"
             style="display: none; height: 1000px; box-shadow: 0px 2px 4px #888888;">


        </div>
    </div>
</main>
