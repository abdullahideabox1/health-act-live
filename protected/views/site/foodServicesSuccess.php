<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="row featurette pt-5 mt-5 w-100 m-0 col-md-12">
                <h1 class="text-center w-100 mt-5">Thanks! Received your query. We will get back to you shortly!</h1>
            </div>
        </div>
    </div>
</main>