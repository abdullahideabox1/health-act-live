<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-5 pt-5">
    <br>

    <h2>My Meal Plans</h2>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Order ID #</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($mymealplans) {
                foreach ($mymealplans as $items) { ?>
                    <tr>
                        <td><?php echo $items['id'] ?></td>
                        <td><?php echo date("d F, Y",strtotime($items['start_date'])) ?></td>
                        <td><?php echo date("d F, Y",strtotime($items['end_date'])) ?></td>
                        <td><?php echo $items['total_amount'] ?></td>
                        <td>
                            <?php if ($items['status'] == 1) { ?>
                                In Progress
                            <?php }elseif($items['status'] == 2){?>
                                Completed
                            <?php } ?>
                        </td>
                        <td><a href="<?php echo Yii::app()->request->baseUrl; ?>/mealplanEdit/<?php echo $items['id']  ?>" class="btn btn-primary">Edit</a></td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</main>