<?php
$startDate = $startingDate;
$endDate = $endingDate;

// Number Weeks
function getNoOfWeek($startDate, $endDate)
{
    // convert date in valid format
    $startDate = date("Y-m-d", strtotime($startDate));
    $endDate = date("Y-m-d", strtotime($endDate));

    $yearEndDay = 31;
    $weekArr = array();
    $startYear = date("Y", strtotime($startDate));
    $endYear = date("Y", strtotime($endDate));

    if ($startYear != $endYear) {
        $newStartDate = $startDate;
        for ($i = $startYear; $i <= $endYear; $i++) {
            if ($endYear == $i) {
                $newEndDate = $endDate;
            } else {
                $newEndDate = $i . "-12-" . $yearEndDay;
            }

            $startWeek = date("W", strtotime($newStartDate));
            $endWeek = date("W", strtotime($newEndDate));

            if ($endWeek == 1) {
                $endWeek = date("W", strtotime($i . "-12-" . ($yearEndDay - 7)));
            }
            $tempWeekArr = range($startWeek, $endWeek);
            array_walk($tempWeekArr, "week_text_alter",
                array('pre' => 'Week ', 'post' => " 20" . substr($i, 2, 2)));
            $weekArr = array_merge($weekArr, $tempWeekArr);

            $newStartDate = date("Y-m-d", strtotime($newEndDate . "+1 days"));
        }
    } else {
        $startWeek = date("W", strtotime($startDate));
        $endWeek = date("W", strtotime($endDate));

        $endWeekMonth = date("m", strtotime($endDate));
        if ($endWeek == 1 && $endWeekMonth == 12) {
            $endWeek = date("W", strtotime($endYear . "-12-" . ($yearEndDay - 7)));
        }
        $weekArr = range($startWeek, $endWeek);
        $params = $startDate;
        $params2 = $endDate;
        array_walk($weekArr, "week_text_alter",
            array('Start Date' => $params, 'End Date' => $params2, 'pre' => 'Week ', 'post' => " 20" . substr($startYear, 2, 2)));

    }

    $weekArr = array_fill_keys($weekArr, 0);

    return $weekArr;

}


function week_text_alter(&$item1, $key, $prefix)
{

    $range = range($item1, $item1);
    $year = $prefix['post'];
    $start_date = $prefix['Start Date'];
    $end_date = $prefix['End Date'];

    foreach ($range as $week_no) {

        $week_start = new DateTime();
        $week_start->setISODate($year, $week_no);
        $week_start->modify('+0 day');

        $seven_day_week = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
        $week = array();

        for ($i = 0; $i < 7; $i++) {
            $day = $seven_day_week[$i];
            $week[$day] = $week_start->format('Y-n-j');
            $week_start->modify('+1 day');

        }

        ?>

        <table class="table table-bordered table-striped" id="<?php echo $week_no; ?>">
            <thead>
            <tr>
                <td></td>
                <?php

                foreach ($week as $item => $key) {

                    echo '<td id="' . $key . '-' . $week_no . '">';
                    echo $item;
                    echo '</td>';

                } ?>
            </tr>
            <tr>
                <td></td>
                <?php

                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) {
                        echo '<td id="' . $key . '">';
                        echo $key;
                        echo '</td>';

                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>


            </thead>
            <tbody>
            <tr>
                <th scope="row">BREAKFAST</th>

                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) {
                        ?>
                        <td id="<?php echo $key . 'breakfast'  ?>" class="sneaks"
                            ondrop="dropcopy(event, this.id)"
                            ondragover="allowDrop(event)" data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="BREAKFAST">
                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>

            </tr>
            <tr>
                <th scope="row">LUNCH</th>
                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) { ?>
                        <td id="<?php echo $key . 'lunch' ?>" class="sneaks"
                            ondrop="dropcopy(event, this.id)"
                            ondragover="allowDrop(event)" data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="LUNCH">
                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>
            <tr>
                <th scope="row">DINNER</th>
                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) { ?>
                        <td id="<?php echo $key . 'dinner' ?>" class="sneaks"
                            ondrop="dropcopy(event, this.id)"
                            ondragover="allowDrop(event)" data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="DINNER">
                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>
            <tr>
                <th scope="row">SNACKS</th>
                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) { ?>
                        <td id="<?php echo $key . 'snacks' ?>" class="sneaks"
                            ondrop="dropcopy(event, this.id)"
                            ondragover="allowDrop(event)" data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="SNACKS">
                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>
            <tr>
                <th scope="row">JUICES</th>
                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) { ?>
                        <td id="<?php echo $key . 'juices' ?>" class="sneaks"
                            ondrop="dropcopy(event, this.id)"
                            ondragover="allowDrop(event)" data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="JUICES">
                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>
            <tr>
                <th scope="row">Nutritional Info</th>
                <?php
                foreach ($week as $item => $key) {
                    if (new DateTime($start_date) <= new DateTime($key) && new DateTime($end_date) >= new DateTime($key)) { ?>
                        <td id="<?php echo $key . 'calorie' ?>" class="sneaks calaries"
                           data-calender-date="<?php echo $key ?>" data-calender-day="<?php echo $item ?>" data-calender-type="calorie">

                        </td>
                        <?php
                    } else {
                        echo '<td class="empty">';
                        echo '</td>';
                    }
                } ?>
            </tr>
            </tbody>
        </table>

        <?php

    } ?>


<?php }

$weekArr = getNoOfWeek($startDate, $endDate); ?>


