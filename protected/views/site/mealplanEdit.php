<form action="" method="post">
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-5">
        <div class="cd-index cd-main-content">
            <div class="starter-template">
                <div class="row featurette pt-5 mt-5 w-100 m-0">
                    <div class="col-md-2 mealplan_left">
                        <input type="hidden" id="mealPlanID" value="<?php echo $userMenu['id'] ?>">
                        <div id="accordion">
                            <div class="card">
                                <?php if ($category) {
                                    foreach ($category as $c) {
                                        if ($c) {
                                            $showClass = 'show';
                                        } else {
                                            $showClass = '';
                                        }
                                        $items = Item::model()->findAll('is_active = :is_active AND category_id = :category_id', array(
                                            ':is_active' => 1,
                                            ':category_id' => $c['id'],
                                        ));
                                        ?>
                                        <form
                                            action=""
                                            method="POST">

                                            <div class="card-header">
                                                <a class="card-link item-category color-green" draggable="false"
                                                   data-toggle="collapse"
                                                   href="#collapse<?php echo $c['id'] ?>">
                                                    <?php echo $c['category_name'] ?>
                                                </a>
                                            </div>


                                            <div id="collapse<?php echo $c['id'] ?>" class="collapse"
                                                 data-parent="#accordion">
                                                <?php if ($items) {
                                                    foreach ($items as $item) {

                                                        ?>
                                                        <div class="card-body p-2" draggable="true"
                                                             ondragstart="drag(event)"
                                                             data-product-id="<?php echo $item['id'] ?>"
                                                             data-product-price="<?php echo $item['price'] ?>"
                                                             id="drag<?php echo $item['id'] ?>">
                                                            <div class="row featurette" data-toggle="modal"
                                                                 data-target="#ProductModal"
                                                                 data-product-id="<?php echo $item['id'] ?>">

                                                                <div class="col-12">
                                                                <span class="mb-0 item-name d-block font-13"
                                                                      title="Drag This Item!"><?php echo $item['name'] ?></span>

                                                            <span
                                                                class="mb-0 item-price d-block font-13">PKR <?php echo $item['price'] ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                } ?>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-10 mealplan_right">

                        <div class="form-group row mt-5 d-none">
                            <label class="col-sm-2 col-form-label">Start</label>

                            <div class="col-sm-3">
                                <input type="text" id="startDate" class="form-control"
                                       value="<?php echo $userMenu['start_date'] ?>" autocomplete="off" required/>
                            </div>


                            <label class="col-sm-2 col-form-label">END</label>

                            <div class="col-sm-3">
                                <input type="text" id="endDate" class="form-control"
                                       value="<?php echo $userMenu['end_date'] ?>" autocomplete="off" required/>
                            </div>
                            <a class="btn btn-primary" id="showCalendar">GO</a>
                        </div>


                        <div class="calender-table"></div>
                    </div>
                    <div class="p-1 pl-5 pr-5 bg-green w-100 mealplan_bottm_edit">
                        <div>
                           <span id="checkout_btn"> <button type="submit" class="btn btn-primary-white float-right ml-5" disabled>CHECK OUT</button></span>
                            <span class="float-right mr-5 text-white mt-2">TOTAL PRICE: <span
                                    id="total_amount"></span><input type="hidden" name="UserMenu[total_amount]" id="total_price"
                                                                    value="0">
                            <input type="hidden" name="UserMenu[start_date]" id="start_date"
                                   value="0">
                                 <input type="hidden" name="UserMenu[end_date]" id="end_date"
                                        value="0">
                                <input type="hidden" name="UserMenu[status]" id="status"
                                       value="0">
                            </span>

                        </div>
                    </div>
                </div>
            </div>


            <!-- ProductDetailModal -->
            <a class="d-none" data-toggle="modal" data-target="#ProductDetailModal"
               id="ShowProductDetailModal">Product Details
            </a>

            <div id="ProductDetailModal" class="modal fade ProductDetailModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">


                    </div>

                </div>
            </div>
            <!-- ProductModal -->
            <div id="ProductModal" class="modal fade ProductModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">

                    </div>
                </div>
            </div>
        </div>
    </main>
</form>




