<?php
/* draws a calendar */
function draw_calendar($month, $date, $year)
{
    $findedStartedDate = $_POST['findedStartedDate'] - 6;
    $findedEndedDate = $_POST['findedEndedDate'];
    $findedStartedMonth = $_POST['findedStartedMonth'];
    $findedStartedYear = $_POST['findedStartedYear'];

    /* draw table */
    $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

    /* table headings */
    $headings = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $calendar .= '<tr class="calendar-row"><td class="calendar-day-head">' . implode('</td><td class="calendar-day-head">', $headings) . '</td></tr>';


    /* days and weeks vars now ... */
    $running_day = date('w', mktime(0, 0, 0, $month, $date, $year));
    $days_in_month = date('t', mktime(0, 0, 0, $month, $date, $year));

    $days_in_this_week = 1;
    $day_counter = 1;
    $dates_array = array();
    $weekCount = 1;
    $weeBound = 1;

    /* row for week one */
    $calendar .= '<tr class="calendar-rowaaaa">';


    /* print "blank" days until the first of the current week */
    for ($x = 1; $x < $running_day; $x++):
        $calendar .= '<td class="calendar-day-np"> </td>';
        $days_in_this_week++;
    endfor;


    /* keep going with days.... */
    for ($list_day = $findedStartedDate; $list_day <= $findedEndedDate; $list_day++):
        if ($list_day == $findedStartedDate || $days_in_this_week == 1 || $days_in_this_week == 7 || $list_day == $findedEndedDate) {
            if ($weeBound == 1) {
                echo '<b> Week ' . $weekCount . '</b>: ' . $list_day . ' - ';
                $weeBound++;
            } else {
                echo $list_day . ' ';
                $weeBound = 1;
                echo '<br>';
                echo '<br>';
            }
        }

        $calendar .= '<td class="calendar-day">';
        /* add in the day number */
        $calendar .= '<div class="day-number">' . $list_day . '</div>';


        /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
        $calendar .= str_repeat('<p> </p>', 2);

        $calendar .= '</td>';

        if ($running_day == 7):
            $weekCount++;
            $calendar .= '</tr>';

            if (($day_counter + 1) != $days_in_month):

                $calendar .= '<tr class="calendar-rowGGGGG">';
            endif;
            $running_day = 0;
            $days_in_this_week = 0;

        endif;

        /* if ($days_in_this_week == 1):
             print_r(' Start Week Days: ' . $list_day . ' ' . 'End Week Days: ' . '<br><br>');

         endif;*/

        $days_in_this_week++;
        $running_day++;
        $day_counter++;


    endfor;


    /*Asignment*/

    /*Asignment*/

    /* finish the rest of the days in the week */
    if ($days_in_this_week < 8):
        for ($x = 1; $x <= (8 - $days_in_this_week); $x++):
            $calendar .= '<td class="calendar-day-np"> </td>';
        endfor;
    endif;


    /* final row */
    $calendar .= '</tr>';

    /* end the table */
    $calendar .= '</table>';

    /* all done, return result */
    return $calendar;


}

/* sample usages */
echo '<h2>'. $findedStartedMonth .' /'.$findedStartedYear.'</h2>';
echo draw_calendar($findedStartedMonth, $findedStartedDate, $findedStartedYear);

?>