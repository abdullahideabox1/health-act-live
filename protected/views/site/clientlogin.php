<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }

    .form-signin .checkbox {
        font-weight: 400;
    }

    .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>

<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="row">
                <form class="form-signin mt-5" method="post">
                    <div class="mt-5 pt-5">
                        <?php
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            echo '<div class="alert alert-danger text-center w-100">' . $message . "</div>\n";
                        }
                        ?>
                        <h1 class="h3 mb-3 font-weight-normal text-center">Please sign in</h1>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" name="loginUser[email]" class="form-control mb-3" placeholder="Email address" required
                               autofocus>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password"  name="loginUser[password]" class="form-control" placeholder="Password" required>

                        <div class="checkbox mb-3">
                            <label>
                               <a href="<?php echo Yii::app()->request->baseUrl; ?>/forgottenpassword">Forgotten Password?</a>
                            </label>
                        </div>
                       <div style="margin: 0 auto" class="text-center">
                           <button type="submit" class="btn btn-primary">Sign in</button>
                       </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
