<main role="main">
    <div class="cd-index cd-main-content">
        <div class="starter-template">
            <div class="row featurette pt-5 mt-5 w-100 m-0 col-md-12">
                <h1 class="text-center w-100 mt-5">Thanks! Order Placed Successfully</h1>
                <h5 class="text-center w-100">Order Details Emailed You. Please Check Your Email.</h5>
                <div class="text-center w-100 mt-5">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard" class="btn btn-primary">View Dashboard</a>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/logout" class="btn btn-primary">Logout</a>
                </div>
            </div>
        </div>
    </div>
</main>