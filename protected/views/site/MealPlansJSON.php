<div class="starter-template pt-5">
    <div class="col-12 h-100vh w-100 mt-4 p-0" style="background-color: white;">
        <div class="row ">
            <div class="col-md-8 col-lg-8 col-sm-12 float-left p-0"
                 style="background-color:#60B49D; overflow: auto; height: 100vh; ">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 p-5 float-right ">
                        <div class="row">
                            <div class="col-12 mt-4">
                                <h4 style="color: white;font-weight: bolder">
                                    <?php echo $mealplans['title'] ?>
                                    <small><?php echo $mealplans['sub_title'] ?></small>
                                </h4>

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 20px">Days</th>
                                        <th scope="col">Meals</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($output as $item => $data) {
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $item ?></th>

                                            <td>
                                                <?php
                                                if (isset($item)) {
                                                    foreach ($data as $k) { ?>
                                                        <div>
                                                            <?php if($k['day_section']){ ?>
                                                                <i><b><?php echo $k['day_section']?></b></i><br>
                                                            <?php } ?>
                                                            <b><?php echo $k['item_title'] ?></b><br>
                                                                <p class="mb-0">
                                                                    <?php echo $k['item_detail'] ?>
                                                                </p>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </td>




                                        </tr>
                                        <?php
                                    } ?>
                                    </tbody>
                                </table>
                                <table class="table mb-5 total_amounts mt-5">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Total Price:</th>
                                        <td>PKR <?php echo $mealplans['total_price'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Free Meal:</th>
                                        <td>PKR <?php echo $mealplans['free_meal'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Delivery Charges:</th>
                                        <td>PKR <?php echo $mealplans['delivery_charges'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total Price for Entire Meal Plan:</th>
                                        <td>PKR <?php echo $mealplans['price_entire_meal'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total Price for Entire Meal Plan + GST:</th>
                                        <td>PKR <?php echo $mealplans['meal_gst'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total Price for Entire Meal Plan (Without
                                            Saturday):
                                        </th>
                                        <td>PKR <?php echo $mealplans['without_saturday'] ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total Price for Entire Meal Plan (Without Saturday)
                                            + GST:
                                        </th>
                                        <td>PKR <?php echo $mealplans['without_saturday_gst'] ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4 col-lg-4 col-sm-12 mt-5 " style="height: 1000px;">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-sm-12 col-md-10">
                        <div class="close-tray closurecons">X</div>
                        <form class="font-setting mt-5 pt-3" method="post" action="">
                            <p style="text-align: center; font-weight: bolder; color:#60B49D;font-size: 20px "
                               class="mb-0">
                                <?php echo $mealplans['title'] ?></p>

                            <p class="text-center"><?php echo $mealplans['sub_title'] ?></p>


                            <input type="hidden" class="form-control form-control-sm"
                                   id="exampleInputName" aria-describedby="emailHelp"
                                   placeholder="Name" name="MealplansOrders[item_name]" value="<?php echo $mealplans['title'] ?>">
                            <input type="hidden" class="form-control form-control-sm"
                                   id="exampleInputName" aria-describedby="emailHelp"
                                   placeholder="Name" name="MealplansOrders[item_sub_title]" value="<?php echo $mealplans['sub_title'] ?>">
                            <input type="hidden" class="form-control form-control-sm"
                                   id="exampleInputName" aria-describedby="emailHelp"
                                   placeholder="Name" name="MealplansOrders[total_price]" value="<?php echo $mealplans['total_price'] ?>">


                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm"
                                       id="exampleInputName" aria-describedby="emailHelp"
                                       placeholder="Name" name="MealplansOrders[name]">
                            </div>

                            <div class="form-group">
                                <input type="number" class="form-control form-control-sm"
                                       id="exampleInputPhoneNumber" placeholder="Phone Number"
                                       name="MealplansOrders[phone]">
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control form-control-sm"
                                       id="exampleInputEmailAddress" placeholder="Email Address"
                                       name="MealplansOrders[email]">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm"
                                       id="exampleInputEmailAddress" placeholder="Address"
                                       name="MealplansOrders[address]">
                            </div>

                            <div class="form-group">
                                <textarea class="form-control form-control-sm"
                                       id="exampleInputComment" placeholder="Special Instructions"
                                       name="MealplansOrders[comments]"></textarea>
                            </div>

                            <div class="form-inline ">
                                <button type="submit" class="btn btn-md text-white"
                                        style="background-color:#60B49D;width: 100% ">
                                    Order Now
                                </button>
                            </div>


                        </form>


                    </div>

                    <div class="col-1">


                    </div>

                </div>
            </div>
        </div>


    </div>
</div>