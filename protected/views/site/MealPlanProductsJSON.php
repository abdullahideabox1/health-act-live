
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<?php if ($items) {
    foreach ($items as $item)
        ?>
        <div class="modal-body">
        <h2><?php echo $item['name'] ?></h2>

    <h3>Rs. <?php echo $item['price'] ?></h3>
    <?php if ($item['item_image'] > 0) { ?>
        <img
            src="<?php echo Yii::app()->request->baseUrl; ?>/upload/ItemImages/<?php echo $item['item_image'] ?>"
            class="img-fluid"/>
    <?php } else { ?>
        <img
            src="https://via.placeholder.com/800x250"
            class="img-fluid"/>
    <?php } ?>
    <hr>
    <h5>DESCRIPTION</h5>

    <p>
        <?php echo $item['description'] ?>
    </p>

    <hr>
    <label>Fats:   <?php echo $item['fats'] ?></label> | <label>Protein:   <?php echo $item['protein'] ?></label> | <label>Carbs:   <?php echo $item['carbo'] ?></label> | <label>Calories:   <?php echo $item['calory'] ?></label>

    </div>
<?php } ?>
<div class="modal-footer">
    <a class="btn btn-default" data-dismiss="modal">Close</a>
</div>
