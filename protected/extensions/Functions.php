<?php

class Functions extends CApplicationComponent {
   	
	public function getTimeSlot($start,$end)
    {
		
		$count = true;
		$slots = array();
		$dt = DateTime::createFromFormat('H:i', $start);
		do{
			
			$slots[] = $dt->format('H:i');
			$dt->add(new DateInterval('PT30M'));
			
			if ($dt->format('H:i') == $end ){
				
				$slots[] = $dt->format('H:i');		
				break;	
				
			}
			
		}while($count);
		
		
		return $slots;
		
	}
	
}