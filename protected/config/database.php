<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database


	/* Live Server Credentials */

	'connectionString' => 'mysql:host=127.0.0.1;dbname=healthactlive',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',

	/* Live Server Credentials */


	/* Local Server Credentials */

	/*'connectionString' => 'mysql:host=localhost;dbname=food',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',*/

	/* Local Server Credentials */

);