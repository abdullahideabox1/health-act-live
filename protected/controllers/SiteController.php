<?php

class SiteController extends Controller
{
    public $meta_title = "";
    public $meta_description = "";
    public $meta_keywords = "";

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters()
    {
        $this->checkDevice();
    }


    public function actionIndex()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'Home'
        ));
        $this->render('index', array(
            'images' => $images
        ));
    }

    public function actionAbout()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'About'
        ));
        $this->render('about', array(
            'images' => $images
        ));
    }

    public function actionConsultation()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'Consultation'
        ));
        $tabs = Tabs::model()->findAllByAttributes(array(
            'page_type' => 'Consultation',
            'is_active' => 1,
        ));

        $consultants_tab = ConsultantTabs::model()->findAll();

        $this->render('consultation', array(
            'images' => $images,
            'tabs' => $tabs,
            'consultants_tab' => $consultants_tab
        ));
    }

    public function actionConsultantJSON()
    {
        $consultant_id = $_POST['consultation_id'];
        $consultants_tab = Consultant::model()->findByPk($consultant_id);
        $consultant_schedule_table = ConsultantSchedule::model()->findAllByAttributes(array(
            'consultant_id' => $consultant_id,
            'is_table' => 1
        ));
        $consultant_schedule_form = ConsultantSchedule::model()->findAllByAttributes(array(
            'consultant_id' => $consultant_id,
            'is_table' => 0
        ));
        $arr = array();
        foreach ($consultant_schedule_table as $key => $day) {
            $arr[$day['days']][$key]['consultant_id'] = $day['consultant_id'];
            $arr[$day['days']][$key]['days'] = $day['days'];
            $arr[$day['days']][$key]['time'] = $day['time'];
            $arr[$day['days']][$key]['is_active'] = $day['is_active'];
            $arr[$day['days']][$key]['is_table'] = $day['is_table'];
            $arr[$day['days']][$key]['sort_order'] = $day['sort_order'];
        }

        $this->render('consultantJSON', array(
            'consultants_tab' => $consultants_tab,
            'consultant_schedule_table' => $consultant_schedule_table,
            'arr' => $arr,
            'consultant_schedule_form' => $consultant_schedule_form
        ));
    }


    public function actionConsultantAppointmentsSuccess()
    {
        $consultant_queries = new ConsultantQueries();
        if (isset($_POST['ConsultantQueries'])) {
            $consultant_queries->attributes = $_POST['ConsultantQueries'];
            if ($consultant_queries->validate()) {
                $consultant_queries->date_added = date('c');
                $consultant_queries->save(false);
            }
        }
        /*Email Send*/
        $subject = "The Health Act - Consultant Appointment";
        $message = '<b>' . "Thank You for booking an appointment" . '</b><br/>';
        $message .= '<b>' . "Full Name: " . $_POST['ConsultantQueries']['name'] . '</b><br/>';
        $message .= '<b>' . "Phone: " . $_POST['ConsultantQueries']['phone'] . '</b><br/>';
        $message .= '<b>' . "Email Address: " . $_POST['ConsultantQueries']['email'] . '</b><br/>';
        $message .= '<b>' . "Consultant Name: " . $_POST['ConsultantQueries']['consultant_name'] . '</b><br/>';
        $message .= '<b>' . "Appointment Time: " . $_POST['ConsultantQueries']['appointment_time'] . '</b><br/>';
        $message .= '<b>' . "Appointment Date: " . $_POST['ConsultantQueries']['date'] . '</b><br/>';
        $message .= '<b>' . "Comments: " . $_POST['ConsultantQueries']['comments'] . '</b><br/>';

        $cc = 'info@thehealthact.com';
        $this->mailsend($_POST['ConsultantQueries']['email'], '', $cc, $subject, $message);
        /*Email Send*/
        $this->layout = "staticLayout";
        $this->render('ConsultantAppointmentsSuccess');
    }

    public function actionGift()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'Gifts'
        ));
        $tabs = Tabs::model()->findAllByAttributes(array(
            'page_type' => 'Gifts',
            'is_active' => 1,
        ));
        $this->render('gift', array(
            'images' => $images,
            'tabs' => $tabs,
        ));
    }

    public function actionGiftSuccess()
    {
        /*Email Send*/
        $subject = "The Health Act - Gift";
        $message = '<b>' . "Thank You for Gift" . '</b><br/>';
        $message .= '<b>' . "Full Name: " . $_POST['Gift']['full_name'] . '</b><br/>';
        $message .= '<b>' . "Phone Number: " . $_POST['Gift']['phone_number'] . '</b><br/>';
        $message .= '<b>' . "Reciever Address: " . $_POST['Gift']['reciever_address'] . '</b><br/>';
        $message .= '<b>' . "Reciever Contact: " . $_POST['Gift']['reciever_contact'] . '</b><br/>';
        $message .= '<b>' . "Message: " . $_POST['Gift']['message'] . '</b><br/>';
        $message .= '<b>' . "Gifted Item: " . $_POST['Gift']['items'] . '</b><br/>';

        $cc = 'abdullah.ideabox@gmail.com';
        $this->mailsend('abdullah.ideabox@gmail.com', '', $cc, $subject, $message);
        /*Email Send*/


        $this->layout = "staticLayout";
        $this->render('giftSuccess');
    }


    public function actionMealPlan()
    {
        $this->layout = "mealPlanLayout";
        $category = Category::model()->findAll('is_active = :is_active ORDER BY is_sort', array(
            ':is_active' => 1,
        ));

        $this->render('mealplan', array(
            'category' => $category,
        ));
    }

    public function actionMealPlanCalenderJSON()
    {
        $startingDate = $_POST['startDate'];
        $endingDate = $_POST['endDate'];


        $this->render('MealPlanCalenderJSON', array(
            'startingDate' => $startingDate,
            'endingDate' => $endingDate
        ));
    }

    public function actionCheckout()
    {
        $this->layout = "staticLayout";

        $userMenu = new UserMenu;
        $session_id = session_id();
        $session = new CHttpSession;               //add this line
        $session->open();
        $userMenuID = 0;


        if (isset($_POST['UserMenu'])) {

            $userMenu->attributes = $_POST['UserMenu'];
            if ($userMenu->validate()) {
                $userMenu->user_id = $session_id;
                $userMenu->status = 1;
                $userMenu->created = date('c');
                $userMenu->save(false);
            }
        }


        if (isset($_POST['UserMenuDetail'])) {
            $productPost = $_POST['UserMenuDetail'];
            $products = array();

            foreach ($productPost['product_name'] as $key => $value) {
                $userMenuDetails = new UserMenuDetail;
                $products[$key]['product_name'] = $productPost['product_name'][$key];
                $products[$key]['product_id'] = $productPost['product_id'][$key];
                $products[$key]['product_price'] = $productPost['product_price'][$key];
                $products[$key]['calender_date'] = $productPost['calender_date'][$key];
                $products[$key]['calender_day'] = $productPost['calender_day'][$key];
                $products[$key]['calender_type'] = $productPost['calender_type'][$key];
                $products[$key]['product_quantity'] = $productPost['product_quantity'][$key];
                $products[$key]['delivery_time'] = $productPost['delivery_time'][$key];
                $products[$key]['option_value'] = $productPost['option_value'][$key];
                $products[$key]['option_value_id'] = $productPost['option_value_id'][$key];

                $userMenuDetails->user_menu_id = $userMenu->id;
                $userMenuDetails->product_name = $productPost['product_name'][$key];
                $userMenuDetails->product_id = $productPost['product_id'][$key];
                $userMenuDetails->product_price = $productPost['product_price'][$key];
                $userMenuDetails->calender_date = $productPost['calender_date'][$key];
                $userMenuDetails->calender_day = $productPost['calender_day'][$key];
                $userMenuDetails->calender_type = $productPost['calender_type'][$key];
                $userMenuDetails->product_quantity = $productPost['product_quantity'][$key];
                $userMenuDetails->delivery_time = $productPost['delivery_time'][$key];
                $userMenuDetails->option_value = $productPost['option_value'][$key];
                $userMenuDetails->option_value_id = $productPost['option_value_id'][$key];

                if ($userMenuDetails->validate()) {
                    $userMenuDetails->save(false);
                }
            }
        }
        if (empty(Yii::app()->session['userMenuID'])) {
            Yii::app()->session['userMenuID'] = $userMenu['id'];
        }
        $userMenuID = Yii::app()->session['userMenuID'];


        $customerMenu = UserMenu::model()->findAll('status = :status AND id = :id', array(
            ':status' => 1,
            ':id' => $userMenuID
        ));


        $model = new Customer;

        if (isset($_POST['RegisterUser'])) {
            $Customers = Customer::model()->findAll('is_active = :is_active AND is_registered = :is_registered AND (email = :email OR mobile = :mobile)', array(
                'is_active' => 1,
                'is_registered' => 1,
                'email' => $_POST['Customer']['email'],
                'mobile' => $_POST['Customer']['mobile']
            ));

            $model->attributes = $_POST['Customer'];
            if ($model->validate()) {

                if ($Customers) {
                    Yii::app()->user->setFlash('Error', "This Mobile Number is already registered Please do login!!!");
                } else {
                    $model->password = md5($_POST['Customer']['mobile']);
                    $model->is_active = 1;
                    $model->is_registered = 1;
                    $model->username = $_POST['Customer']['username'];
                    $model->email = $_POST['Customer']['email'];
                    $model->mobile = $_POST['Customer']['mobile'];
                    $model->address = $_POST['Customer']['address'];
                    $model->save();
                    $newUserID = UserMenu::model()->findByPk($userMenuID);
                    if ($newUserID) {
                        $newUserID->user_id = $model->id;
                        $newUserID->status = 2;
                        $newUserID->save();
                        /*Email Send*/
                        $subject = "The Health Act - Meal Plan";
                        $message = '<b>' . "Thank You for your query! We will get back to you shortly. Your Login Details Are" . '</b><br/>';
                        $message = '<b>' . "You Can order with following login details now" . '</b><br/>';
                        $message .= '<b>' . "Email Address: " . $_POST['Customer']['email'] . '</b><br/>';
                        $message .= '<b>' . "Password: " . $_POST['Customer']['mobile'] . '</b><br/>';
                        $message .= '<b><h4>' . "Order Details Are:" . '</h4></b>';
                        $message .= '<b>' . "Full Name: " . $_POST['Customer']['username'] . '</b><br/>';
                        $message .= '<b>' . "Email Address: " . $_POST['Customer']['email'] . '</b><br/>';
                        $message .= '<b>' . "Mobile: " . $_POST['Customer']['mobile'] . '</b><br/>';
                        $message .= '<b>' . "Shipping Address: " . $_POST['Customer']['address'] . '</b><br/>';
                        $message .= '<b>' . "TOTAL AMOUNT: " . $newUserID['total_amount'] . '</b><br/>';

                        $cc = 'info@thehealthact.com';
                        $this->mailsend($_POST['Customer']['email'], '', $cc, $subject, $message);
                        /*Email Send*/
                        Yii::app()->session['userMenuID'] = '';
                        /*create session with User ID*/
                        if (empty(Yii::app()->session['userID'])) {
                            Yii::app()->session['userID'] = $model['id'];
                        }
                        $userid = Yii::app()->session['userID'];
                        /*create session with User ID*/
                        $this->redirect(Yii::app()->baseUrl . '/success');
                    }

                }
            }

        } else if (isset($_POST['loginUser'])) {
            $Customers = Customer::model()->findByAttributes(array(
                'is_active' => 1,
                'is_registered' => 1,
                'email' => $_POST['Customers']['email']
            ));
            $newUserID = UserMenu::model()->findByPk($userMenuID);

            if ($Customers['password'] == md5($_POST['Customers']['password'])) {
                //  $this->pre($newUserID,1);

                if ($newUserID) {
                    $newUserID->user_id = $Customers->id;
                    $newUserID->status = 2;
                    $getUserDetails = Customer::model()->findByPk($newUserID['user_id']);

                    /*Email Send*/
                    $subject = "The Health Act - Meal Plan";
                    $message = '<b>' . "Thank You for placing an order" . '</b><br/>';
                    $message .= '<b>' . "Full Name: " . $getUserDetails['username'] . '</b><br/>';
                    $message .= '<b>' . "Email Address: " . $getUserDetails['email'] . '</b><br/>';
                    $message .= '<b>' . "Mobile Number: " . $getUserDetails['mobile'] . '</b><br/>';
                    $message .= '<b>' . "Address: " . $getUserDetails['address'] . '</b><br/>';
                    $message .= '<b>' . "Order Amount: " . $newUserID['total_amount'] . '</b><br/>';
                    $cc = 'info@thehealthact.com';
                    $this->mailsend($getUserDetails['email'], '', $cc, $subject, $message);
                    /*Email Send*/

                    $newUserID->save();


                    Yii::app()->session['userMenuID'] = '';
                    // Yii::app()->session->destroy();
                    /*create session with User ID*/
                    if (empty(Yii::app()->session['userID'])) {
                        Yii::app()->session['userID'] = $Customers['id'];
                    }
                    $userid = Yii::app()->session['userID'];
                    /*create session with User ID*/
                    $this->redirect(Yii::app()->baseUrl . '/success');
                }
                //   Yii::app()->user->setFlash('Success', "Your Order Placed Succesfully");
            } else {
                Yii::app()->user->setFlash('Error', "Invalid Email Address Or Password!");
            }

        }

        if (isset($_POST['forgotpassword_btn'])) {

            $Customers = Customer::model()->findByAttributes(array(
                'is_active' => 1,
                'is_registered' => 1,
                'email' => $_POST['forgotpassword']['email']
            ));

            if ($Customers) {
                $rnd = rand(0, 99999999);
                $Customers->password = md5($rnd);
                $Customers->save();
                /*Email Send*/
                $subject = "The Health Act - Forgot Password";
                $message = '<b>' . "Your password has been reset, Kindly login with new credentials:" . '</b><br/>';
                $message .= '<b>' . "Email Address: " . $_POST['forgotpassword']['email'] . '</b><br/>';
                $message .= '<b>' . "New Password: " . $rnd . '</b><br/>';

                $cc = 'info@thehealthact.com';
                $this->mailsend($_POST['forgotpassword']['email'], '', $cc, $subject, $message);
                //Yii::app()->user->setFlash('Success', "Password Reset! Please check your registered email!");
                /*Email Send*/
            } else {
                Yii::app()->user->setFlash('Error', "Invalid Email Address!");
            }
        }

        $this->render('checkout', array(
            'userMenuID' => $userMenuID,
            'customerMenu' => $customerMenu
        ));
    }

    public function actionSuccess()
    {
        $this->layout = "staticLayout";
        $userid = Yii::app()->session['userID'];
        $this->render('success', array());
    }

    public function actionDashboard()
    {
        $this->layout = "DashboardstaticLayout";
        $this->checkSessionFront();
        $this->render('dashboard', array());
    }

    public function actionOrders()
    {
        $this->layout = "DashboardstaticLayout";
        $this->checkSessionFront();
        $userid = Yii::app()->session['userID'];
        $orders = UserMenu::model()->findAllByAttributes(array(
            'user_id' => $userid
        ));
        $this->render('orders', array('orders' => $orders));
    }

    public function actionProfile()
    {
        $this->layout = "DashboardstaticLayout";
        $this->checkSessionFront();
        $userid = Yii::app()->session['userID'];
        $profile = Customer::model()->findByAttributes(array(
            'id' => $userid
        ));

        if (isset($_POST['Customer'])) {
            if ($profile) {
                $profile->username = $_POST['Customer']['username'];
                $profile->email = $_POST['Customer']['email'];
                $profile->mobile = $_POST['Customer']['mobile'];
                $profile->address = $_POST['Customer']['address'];
                if ($_POST['Customer']['password'] == '') {
                    $profile->password = $profile->password;
                } else {
                    $profile->password = md5($_POST['Customer']['password']);
                }
                $profile->save();
                $this->redirect(Yii::app()->baseUrl . '/profile');
            }
        }


        $this->render('profile', array('profile' => $profile));
    }

    public function actionMyMealPlans()
    {
        $this->layout = "DashboardstaticLayout";
        $this->checkSessionFront();
        $userid = Yii::app()->session['userID'];
        $mymealplans = UserMenu::model()->findAllByAttributes(array(
            'user_id' => $userid
        ));

        $this->render('mymealplans', array('mymealplans' => $mymealplans));
    }

    public function actionMealPlanEdit($slug)
    {
        $this->layout = "DashboardstaticLayout";
        $this->checkSessionFront();
        $split = explode("-", $slug);
        $id = $split[count($split) - 1];
        $userid = Yii::app()->session['userID'];


        //$this->pre($_POST,1);

        $category = Category::model()->findAll('is_active = :is_active', array(
            ':is_active' => 1,
        ));


        $userMenu = UserMenu::model()->findByPk($id);


        if (isset($_POST['UserMenu'])) {
            if ($userMenu) {
                $userMenu->total_amount = $_POST['UserMenu']['total_amount'];
                $userMenu->save();
            }
        }

        if (isset($_POST['UserMenuDetail'])) {
            $productPost = $_POST['UserMenuDetail'];
            $products = array();
            UserMenuDetail::model()->deleteAllByAttributes(array(
                'user_menu_id' => $id,
            ));
            foreach ($productPost['product_name'] as $key => $value) {
                $userMenuDetails = new UserMenuDetail;
                $products[$key]['product_name'] = $productPost['product_name'][$key];
                $products[$key]['product_id'] = $productPost['product_id'][$key];
                $products[$key]['product_price'] = $productPost['product_price'][$key];
                $products[$key]['calender_date'] = $productPost['calender_date'][$key];
                $products[$key]['calender_day'] = $productPost['calender_day'][$key];
                $products[$key]['calender_type'] = $productPost['calender_type'][$key];
                $products[$key]['product_quantity'] = $productPost['product_quantity'][$key];
                $products[$key]['delivery_time'] = $productPost['delivery_time'][$key];
                $products[$key]['option_value'] = $productPost['option_value'][$key];
                $products[$key]['option_value_id'] = $productPost['option_value_id'][$key];
                $userMenuDetails->user_menu_id = $userMenu->id;
                $userMenuDetails->product_name = $productPost['product_name'][$key];
                $userMenuDetails->product_id = $productPost['product_id'][$key];
                $userMenuDetails->product_price = $productPost['product_price'][$key];
                $userMenuDetails->calender_date = $productPost['calender_date'][$key];
                $userMenuDetails->calender_day = $productPost['calender_day'][$key];
                $userMenuDetails->calender_type = $productPost['calender_type'][$key];
                $userMenuDetails->product_quantity = $productPost['product_quantity'][$key];
                $userMenuDetails->delivery_time = $productPost['delivery_time'][$key];
                $userMenuDetails->option_value = $productPost['option_value'][$key];
                $userMenuDetails->option_value_id = $productPost['option_value_id'][$key];
                if ($userMenuDetails->validate()) {
                    $getUserDetails = Customer::model()->findByPk($userMenu->user_id);
                    $userMenuDetails->save();

                }
            }

            /*Email Send*/
            $subject = "The Health Act - Meal Plan";
            $message = '<b>' . "Thank You for placing an order" . '</b><br/>';
            $message .= '<b>' . "Full Name: " . $getUserDetails['username'] . '</b><br/>';
            $message .= '<b>' . "Email Address: " . $getUserDetails['email'] . '</b><br/>';
            $message .= '<b>' . "Mobile Number: " . $getUserDetails['mobile'] . '</b><br/>';
            $message .= '<b>' . "Address: " . $getUserDetails['address'] . '</b><br/>';
            $message .= '<b>' . "Order Amount: " . $userMenu->total_amount . '</b><br/>';
            $cc = 'muhammad99abdullah@hotmail.com';
            $this->mailsend($getUserDetails['email'], '', $cc, $subject, $message);
            /*Email Send*/

            $this->redirect(Yii::app()->baseUrl . '/mymealplans');
        }

        $this->render('mealplanEdit', array(
            'category' => $category,
            'userMenu' => $userMenu,
        ));
    }

    public function actionMealPlanCalenderJSONEdit()
    {
        $startingDate = $_POST['startDate'];
        $endingDate = $_POST['endDate'];
        $mealPlanID = $_POST['mealPlanID'];
        //        $this->pre($userMenuItems, 1);
        $this->render('MealPlanCalenderJSONEdit', array(
            'startingDate' => $startingDate,
            'endingDate' => $endingDate,
            'mealPlanID' => $mealPlanID
        ));
    }

    public function actionProductPriceJSONEdit()
    {


        $product_id = explode(",", $_POST['product_id']);
        $price = 0;

        foreach ($product_id as $ids) {
            $product = Item::model()->findByPk($ids);
            if ($product) {
                $price += $product['price'];
            }
        }


        $this->render('productPriceJSONEdit', array(
            'price' => $price
        ));
    }

    public function actionLogout()
    {
        $this->layout = "staticLayout";
        Yii::app()->session->clear();
        $this->render('logout', array());
    }


    public function actionTest()
    {
        $this->pre($_POST, 1);
        // die;
        $this->render('test', array());
    }

    public function actionProductPriceJSON()
    {


        $product_id = explode(",", $_POST['product_id']);
        $product_qty = explode(",", $_POST['product_qty']);

        $price = 0;
        foreach ($product_id as $key => $ids) {

            $product = Item::model()->findByPk($ids);
            if ($product) {
                $price += ($product['price'] * $product_qty[$key]);
            }
        }

        echo $price;
    }


    public function actionProductCalaoriesJSON()
    {


        $product_id = explode(",", $_POST['product_id']);
        $product_qty = explode(",", $_POST['product_qty']);
        $option_value_id = explode(",", $_POST['option_value_id']);
        $calender_date = explode(",", $_POST['calender_date']);

        $day = array();


        foreach ($product_id as $key => $ids) {

            $fats = $protein = $carbo = $calory = $price = 0;

            $product = Item::model()->findByPk($ids);

            if ($product) {

                $fats += $product['fats'] * $product_qty[$key];
                $protein += $product['protein'] * $product_qty[$key];
                $carbo += $product['carbo'] * $product_qty[$key];
                $calory += $product['calory'] * $product_qty[$key];


                $subitem_option_id = $option_value_id[$key];

                $subitemOption = SubitemOption::model()->findByPk($subitem_option_id);
                if ($subitemOption) {

                    $fats += $subitemOption['item_fats'] * $product_qty[$key];
                    $protein += $subitemOption['item_protein'] * $product_qty[$key];
                    $carbo += $subitemOption['item_carbo'] * $product_qty[$key];
                    $calory += $subitemOption['item_calory'] * $product_qty[$key];

                }

                if (isset($day[$calender_date[$key]])) {

                    $day[$calender_date[$key]]["fats"] += $fats;
                    $day[$calender_date[$key]]["protein"] += $protein;
                    $day[$calender_date[$key]]["carbo"] += $carbo;
                    $day[$calender_date[$key]]["calory"] += $calory;

                } else {

                    $day[$calender_date[$key]]["fats"] = $fats;
                    $day[$calender_date[$key]]["protein"] = $protein;
                    $day[$calender_date[$key]]["carbo"] = $carbo;
                    $day[$calender_date[$key]]["calory"] = $calory;

                }


                //$price += ($product['price'] * $product_qty[$key]);
            }
        }

        echo json_encode($day);
    }


    public function actionTestJSON()
    {
        $findedStartedDate = $_POST['findedStartedDate'] - 6;
        $findedEndedDate = $_POST['findedEndedDate'];
        $findedStartedMonth = $_POST['findedStartedMonth'];
        $findedStartedYear = $_POST['findedStartedYear'];
        $this->render('testJSON', array(
            'findedStartedDate' => $findedStartedDate,
            'findedEndedDate' => $findedEndedDate,
            'findedStartedMonth' => $findedStartedMonth,
            'findedStartedYear' => $findedStartedYear,
        ));
    }


    public function actionMealPlans()
    {
        $this->layout = "mealPlanLayout";
        $category = Category::model()->findAll('is_active = :is_active', array(
            ':is_active' => 1,
        ));
        $tabs = Tabs::model()->findAllByAttributes(array(
            'page_type' => 'Meal Plans'
        ));
        if ($_POST) {

            $mealplan_orders = new MealplansOrders();
            if (isset($_POST['MealplansOrders'])) {
                $mealplan_orders->attributes = $_POST['MealplansOrders'];
                $mealplan_orders->date_ordered = date('c');

                if ($mealplan_orders->validate()) {

                    $mealplan_orders->save(false);
                    /*Email Send*/
                    $subject = "The Health Act - Meal Plans";
                    $message = '<b>' . "Thank you for placing ordering." . '</b><br/>';
                    $message .= '<b>' . "Meal: " . $_POST['MealplansOrders']['item_name'] . "-" . $_POST['MealplansOrders']['item_sub_title'] . '</b><br/>';
                    $message .= '<b>' . "Full Name: " . $_POST['MealplansOrders']['name'] . '</b><br/>';
                    $message .= '<b>' . "Phone Number: " . $_POST['MealplansOrders']['phone'] . '</b><br/>';
                    $message .= '<b>' . "Email Address: " . $_POST['MealplansOrders']['email'] . '</b><br/>';
                    $message .= '<b>' . "Address: " . $_POST['MealplansOrders']['address'] . '</b><br/>';
                    $message .= '<b>' . "Comment: " . $_POST['MealplansOrders']['comments'] . '</b><br/><br/>';
                    $message .= '<b>' . "Total Amount: " . $_POST['MealplansOrders']['total_price'] . '</b><br/>';

                    $cc = 'info@thehealthact.com';
                    $this->mailsend($_POST['MealplansOrders']['email'], '', $cc, $subject, $message);
                    /*Email Send*/
                    Yii::app()->user->setFlash('success', "Thank You for placing order  ");
                }
            }
        }
        $this->render('MealPlans', array(
            'category' => $category,
            'tabs' => $tabs,
        ));
    }

    public function actionMealPlansJSON()
    {
        $id = $_POST['id'];

        $mealplans = Mealplans::model()->findByPk($id);
        $items = MealplansItems::model()->findAll('mealplans_id = :mealplans_id ORDER BY sort_order ASC',array(
            'mealplans_id' => $id
        ));
        $output = array();
        foreach ($items as $key => $value) {
            $arr = array();
            $arr['mealplans_id'] = $value['mealplans_id'];
            $arr['days'] = $value['days'];
            $arr['item_title'] = $value['item_title'];
            $arr['item_detail'] = $value['item_detail'];
            $arr['day_section'] = $value['day_section'];
            $arr['sort_order'] = $value['sort_order'];
            $output[$value['days']][] = $arr;
        }
        //   $this->pre($mealplans, 1);
        $this->render('MealPlansJSON', array(
            'mealplans' => $mealplans,
            'output' => $output,
        ));
    }


    public function actionMealPlanProductsJSON()
    {
        $product_id = $_POST['product_id'];

        $items = Item::model()->findAllByAttributes(array(
            'is_active' => 1,
            'id' => $product_id,
        ));

        $this->render('MealPlanProductsJSON', array(
            'items' => $items
        ));
    }

    public function actionMealPlanProductsWithOptionsJSON()
    {
        $product_id = $_POST['product_id'];
        $item_id = $_POST['item_id'];

        $product_quantity = $_POST['product_quantity'];
        $delivery_time = $_POST['delivery_time'];

        $calender_type = $_POST['calender_type'];
        $option_value_id = $_POST['option_value_id'];


        $items = Item::model()->findAllByAttributes(array(
            'is_active' => 1,
            'id' => $product_id,
        ));

        $this->render('MealPlanProductsWithOptionsJSON', array(
            'items' => $items,
            'item_id' => $item_id,
            'product_quantity' => $product_quantity,
            'delivery_time' => $delivery_time,
            'calender_type' => $calender_type,
            'option_value_id' => $option_value_id,
        ));
    }


    public function actionCold()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'Juices'
        ));
        $this->render('cold', array(
            'images' => $images
        ));
    }

    public function actionFoodService()
    {
        $this->layout = "staticLayout";
        $images = Images::model()->findAllByAttributes(array(
            'is_active' => 1,
            'page_type' => 'Food Services'
        ));
        $tabs = Tabs::model()->findAllByAttributes(array(
            'page_type' => 'Food Services',
            'is_active' => 1,
        ));
        $this->render('FoodService', array(
            'images' => $images,
            'tabs' => $tabs,
        ));
    }

    public function actionfoodServicesSuccess()
    {
        /*Email Send*/
        $subject = "The Health Act - Food Services";
        $message = '<b>' . "Thanks! Received your query. We will get back to you shortly!" . '</b><br/>';
        $message .= '<b>' . "Full Name / Email Address: " . $_POST['FoodService']['name'] . '</b><br/>';
        $message .= '<b>' . "Phone: " . $_POST['FoodService']['phone'] . '</b><br/>';
        $message .= '<b>' . "Message: " . $_POST['FoodService']['message'] . '</b><br/>';

        $cc = 'info@thehealthact.com';
        $this->mailsend('abdullah.ideabox@gmail.com', '', '', $subject, $message);
        /*Email Send*/
        $this->layout = "staticLayout";
        $this->render('foodServicesSuccess', array());
    }

    public function actionContact()
    {
        $this->layout = "staticLayout";
        $this->render('contact', array());
    }


    public function actionClientLogin()
    {
        $this->layout = "staticLayout";


        if (isset($_POST['loginUser'])) {
            $Customers = Customer::model()->findByAttributes(array(
                'is_active' => 1,
                'is_registered' => 1,
                'email' => $_POST['loginUser']['email'],
                'password' => md5($_POST['loginUser']['password'])
            ));
            if (empty(Yii::app()->session['userID'])) {
                Yii::app()->session['userID'] = $Customers['id'];
            }
            if ($Customers) {

                $this->checkSessionFront();
                $this->redirect(Yii::app()->baseUrl . '/dashboard');
                //   Yii::app()->user->setFlash('Success', "Your Order Placed Succesfully");
            } else {
                Yii::app()->user->setFlash('Error', "Invalid Email Address Or Password!");
            }
        }

        $this->render('clientlogin', array());
    }


    public function actionForgottenpassword()
    {
        $this->layout = "staticLayout";
        //   $this->pre($_POST['forgotpassword'], 1);
        if (isset($_POST['forgotpassword'])) {
            $Customers = Customer::model()->findByAttributes(array(
                'is_active' => 1,
                'is_registered' => 1,
                'email' => $_POST['forgotpassword']['email']
            ));

            if ($Customers) {
                $rnd = rand(0, 99999999);
                $Customers->password = md5($rnd);
                $Customers->save();
                /*Email Send*/
                $subject = "The Health Act - Forgot Password";
                $message = '<b>' . "Your password has been reset, Kindly login with new credentials:" . '</b><br/>';
                $message .= '<b>' . "Email Address: " . $_POST['forgotpassword']['email'] . '</b><br/>';
                $message .= '<b>' . "New Password: " . $rnd . '</b><br/>';

                $cc = 'info@thehealthact.com';
                $this->mailsend($_POST['forgotpassword']['email'], '', '', $subject, $message);
                $this->redirect(Yii::app()->baseUrl . '/clientlogin');
                //Yii::app()->user->setFlash('Success', "Password Reset! Please check your registered email!");
                /*Email Send*/
            } else {
                Yii::app()->user->setFlash('Error', "Invalid Email Address!");
            }
        }
        $this->render('forgottenpassword', array());
    }

    public function actionContactSuccess()
    {
        /*Email Send*/
        $subject = "The Health Act - Contact Us";
        $message = '<b>' . "Thank You for Contacting us" . '</b><br/>';
        $message .= '<b>' . "Full Name: " . $_POST['Contact']['full_name'] . '</b><br/>';
        $message .= '<b>' . "Email Address: " . $_POST['Contact']['email_address'] . '</b><br/>';
        $message .= '<b>' . "Message: " . $_POST['Contact']['message'] . '</b><br/>';

        $cc = 'info@thehealthact.com';
        $this->mailsend($_POST['Contact']['email_address'], '', $cc, $subject, $message);
        /*Email Send*/


        $this->layout = "staticLayout";
        $this->render('contactSuccess');
    }


    public function actionError()
    {
        $this->layout = "viewsLayout";
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionPage404()
    {
        $this->layout = "viewsLayout";
        $message = "Page Not Found.";

        $this->render('error', array('message' => $message));

    }

    public function actionLogin()
    {
        //$this->layout = 'loginlayout';
        $this->layout = "adminLoginLayout";
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())

                $this->redirect(Yii::app()->baseUrl . '/admin/index');
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionAdmin()
    {
        $this->redirect(Yii::app()->baseUrl . '/admin/index');
    }

    public function actionLogout2()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->baseUrl . '/admin');
    }

    //Create PDF
    public function actionmealplansExport()
    {
        require_once(Yii::app()->basePath . '/../plugins/html2pdf/html2pdf.class.php');
        $id = Yii::app()->request->getQuery('id');

        $mealplans = Mealplans::model()->findByPk($id);
        $items = MealplansItems::model()->findAllByAttributes(array(
            'mealplans_id' => $id
        ));
        $output = array();
        foreach ($items as $key => $value) {
            $arr = array();
            $arr['mealplans_id'] = $value['mealplans_id'];
            $arr['days'] = $value['days'];
            $arr['item_title'] = $value['item_title'];
            $arr['item_detail'] = $value['item_detail'];
            $arr['day_section'] = $value['day_section'];
            $output[$value['days']][] = $arr;
        }

        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->WriteHTML($this->renderPartial('mealplansExport', array(
            'mealplans' => $mealplans,
            'output' => $output,
        ), true));
        $html2pdf->Output('TheHealthAct-MealPlans' . '.pdf');

    }
}