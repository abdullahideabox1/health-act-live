<?php

/**
 * This is the model class for table "subitem_option".
 *
 * The followings are the available columns in table 'subitem_option':
 * @property integer $id
 * @property string $option_name
 * @property integer $subitem_id
 * @property integer $item_fats
 * @property integer $item_protein
 * @property integer $item_carbo
 * @property integer $item_calory
 * @property integer $is_active
 */
class SubitemOption extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'subitem_option';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('option_name, subitem_id, item_fats, item_protein, item_carbo, item_calory, is_active', 'required'),
            array('subitem_id, is_active', 'numerical', 'integerOnly' => true),
            array('option_name', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, option_name, subitem_id, item_fats, item_protein, item_carbo, item_calory, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'option' => array(self::BELONGS_TO, 'Subitem', 'subitem_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'option_name' => 'Option Name',
            'subitem_id' => 'Subitem',
            'item_fats' => 'Item Fats',
            'item_protein' => 'Item Protein',
            'item_carbo' => 'Item Carbo',
            'item_calory' => 'Item Calory',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('option_name', $this->option_name, true);
        $criteria->compare('subitem_id', $this->subitem_id);
        $criteria->compare('item_fats', $this->item_fats);
        $criteria->compare('item_protein', $this->item_protein);
        $criteria->compare('item_carbo', $this->item_carbo);
        $criteria->compare('item_calory', $this->item_calory);
        $criteria->compare('is_active', $this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SubitemOption the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
