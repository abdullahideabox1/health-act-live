<?php

/**
 * This is the model class for table "customer_order".
 *
 * The followings are the available columns in table 'customer_order':
 * @property integer $id
 * @property integer $customer_id
 * @property string $shipping_address
 * @property string $total_price
 * @property string $order_date
 * @property string $order_time
 * @property integer $is_delivery
 * @property integer $delivery_charges
 * @property string $comments
 * @property integer $status
 * @property integer $is_active
 */
class CustomerOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, shipping_address, total_price, order_date, order_time, is_delivery, delivery_charges, is_active', 'required'),
			array('customer_id, is_delivery, delivery_charges, status, is_active', 'numerical', 'integerOnly'=>true),
			array('shipping_address, order_time', 'length', 'max'=>255),
			array('total_price', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, customer_id, shipping_address, total_price, order_date, order_time, is_delivery, delivery_charges, comments, status, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
				'order_detail' => array(self::HAS_MANY, 'OrderDetails', 'order_id'),
				'order_status' => array(self::HAS_MANY, 'OrderStatus', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Customer',
			'shipping_address' => 'Shipping Address',
			'total_price' => 'Total Price',
			'order_date' => 'Order Date',
			'order_time' => 'Order Time',
			'is_delivery' => 'Is Delivery',
			'delivery_charges' => 'Delivery Charges',
			'comments' => 'Comments',
			'status' => 'Status',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('shipping_address',$this->shipping_address,true);
		$criteria->compare('total_price',$this->total_price,true);
		$criteria->compare('order_date',$this->order_date,true);
		$criteria->compare('order_time',$this->order_time,true);
		$criteria->compare('is_delivery',$this->is_delivery);
		$criteria->compare('delivery_charges',$this->delivery_charges);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
