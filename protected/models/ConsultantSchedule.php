<?php

/**
 * This is the model class for table "consultant_schedule".
 *
 * The followings are the available columns in table 'consultant_schedule':
 * @property integer $id
 * @property integer $consultant_id
 * @property string $days
 * @property string $time
 * @property integer $is_active
 * @property integer $is_table
 * @property integer $sort_order
 */
class ConsultantSchedule extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'consultant_schedule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('consultant_id, days, time, is_active, sort_order', 'required'),
			array('consultant_id, is_active, is_table, sort_order', 'numerical', 'integerOnly'=>true),
			array('days, time', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, consultant_id, days, time, is_active, is_table, sort_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'consultant' => array(self::BELONGS_TO, 'Consultant', 'consultant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'consultant_id' => 'Consultant',
			'days' => 'Days',
			'time' => 'Time',
			'is_active' => 'Is Active',
			'is_table' => 'Is Table',
			'sort_order' => 'Sort Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('consultant_id',$this->consultant_id);
		$criteria->compare('days',$this->days,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_table',$this->is_table);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConsultantSchedule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
