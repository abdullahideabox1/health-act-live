<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $mobile
 * @property string $address
 * @property string $address2
 * @property string $address3
 * @property string $fb_user_id
 * @property integer $is_registered
 * @property integer $is_active
 */
class Customer extends CActiveRecord
{

    public $comments;
    public $addr_radio;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'customer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, email, mobile, address', 'required'),
            array('is_registered, mobile, is_active', 'numerical', 'integerOnly' => true),
            array('username, mobile', 'length', 'max' => 100),
            array('email, password, fb_user_id', 'length', 'max' => 255),
            array('email', 'email'),
            array('address2', 'required', 'on' => '2'),
            array('address3', 'required', 'on' => '3'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, email, password, mobile, addr_radio , address, address2, address3, fb_user_id, is_registered, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'address2' => 'Address2',
            'address3' => 'Address3',
            'fb_user_id' => 'Fb User',
            'is_registered' => 'Is Registered',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('address2', $this->address2, true);
        $criteria->compare('address3', $this->address3, true);
        $criteria->compare('fb_user_id', $this->fb_user_id, true);
        $criteria->compare('is_registered', $this->is_registered);
        $criteria->compare('is_active', $this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Customer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
