<?php

/**
 * This is the model class for table "mealplans_orders".
 *
 * The followings are the available columns in table 'mealplans_orders':
 * @property integer $id
 * @property string $item_name
 * @property string $item_sub_title
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $comments
 * @property string $total_price
 * @property string $date_ordered
 */
class MealplansOrders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mealplans_orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_name, item_sub_title, name, phone, email, address, comments, total_price, date_ordered', 'required'),
			array('item_name, item_sub_title, name, phone, email, address, comments, total_price', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_name, item_sub_title, name, phone, email, address, comments, total_price, date_ordered', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'item_sub_title' => 'Item Sub Title',
			'name' => 'Name',
			'phone' => 'Phone',
			'email' => 'Email',
			'address' => 'Address',
			'comments' => 'Comments',
			'total_price' => 'Total Price',
			'date_ordered' => 'Date Ordered',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('item_sub_title',$this->item_sub_title,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('total_price',$this->total_price,true);
		$criteria->compare('date_ordered',$this->date_ordered,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MealplansOrders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
