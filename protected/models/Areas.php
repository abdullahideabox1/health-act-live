<?php

/**
 * This is the model class for table "areas".
 *
 * The followings are the available columns in table 'areas':
 * @property integer $id
 * @property string $area
 * @property integer $min_order
 * @property integer $delivery_fee
 * @property integer $free_delivery_amount
 * @property integer $delivery_time
 * @property integer $is_active
 */
class Areas extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'areas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('area, delivery_time, is_active', 'required'),
            array('min_order, delivery_fee, free_delivery_amount, delivery_time, is_active', 'numerical', 'integerOnly' => true),
            array('area', 'length', 'max' => 255),
            array('id, area, min_order, delivery_fee, free_delivery_amount, delivery_time, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'area' => 'Area',
            'min_order' => 'Min Order',
            'delivery_fee' => 'Delivery Fee',
            'free_delivery_amount' => 'Free Delivery Amount',
            'delivery_time' => 'Delivery Time',
            'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('area', $this->area, true);
        $criteria->compare('min_order', $this->min_order);
        $criteria->compare('delivery_fee', $this->delivery_fee);
        $criteria->compare('free_delivery_amount', $this->free_delivery_amount);
        $criteria->compare('delivery_time', $this->delivery_time);
        $criteria->compare('is_active', $this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Areas the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
