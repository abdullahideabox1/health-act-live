<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_USER_BANNED = -1;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        if (Yii::app()->controller->action->id == 'proceed') {
            $users = Customer::model()->find('email = :email AND password =:password',
                array(
                    ':email' => $this->username,
                    ':password' => md5($this->password),

                ));
        } else {
            $users = User::model()->find('email = :email AND password =:password',
                array(
                    ':email' => $this->username,
                    ':password' => md5($this->password),

                ));

        };


        if (!isset($users['email']) && $users['email'] == $this->username) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($users['password'] !== md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } elseif ($users['is_active'] != 1) {
            $this->errorCode = self::ERROR_USER_BANNED;
            $this->errorMessage = 'Sorry, User has been disabled.';
            return $this->errorCode;
        } else
            $this->errorCode = self::ERROR_NONE;

        if (Yii::app()->controller->action->id != 'proceed') {
            Yii::app()->session['userId'] = $users['id'];
            Yii::app()->session['userType'] = $users['user_type'];
        }else{
            Yii::app()->session['customerId'] = $users['id'];
        }

        return !$this->errorCode;

    }
}